#include <stdlib.h>
#include <iostream>
#include <admesh/stl.h>

using namespace std;

const size_t angleJump = 5;

double calculateXYVolume(stl_file& stlIn){
  float minX, minY, maxX, maxY;
  stl_facet facet = stlIn.facet_start[0];
  minX = maxX = facet.vertex[0].x;
  minY = maxY = facet.vertex[0].y;
  for (size_t i = 0; i < stlIn.stats.number_of_facets; i++) {
    facet = stlIn.facet_start[i];
    for (size_t j = 0; j < 3; j++) {
      stl_vertex vertex = facet.vertex[j];
      if (vertex.x < minX)
        minX = vertex.x;
      else if (vertex.x > maxX)
        maxX = vertex.x;
      if (vertex.y < minY)
        minY = vertex.y;
      else if (vertex.y > maxY)
        maxY = vertex.y;
    }
  }
  double res = (maxX - minX) * (maxY - minY);
  return res;
}

int main(int argc, char** argv) {
  stl_file stl_in;

  if (argc != 3){
    cout << "Error" << endl;
    return 1;
  }

  stl_open(&stl_in, argv[1]);
  stl_exit_on_error(&stl_in);
  double minVolume;
  int bestAngle = 0;
  for (size_t i = 0; i < 90; i+=angleJump) {
    if (i == 0){
      minVolume = calculateXYVolume(stl_in);
    } else {
      stl_rotate_z(&stl_in, angleJump);
      stl_exit_on_error(&stl_in);
      double vol = calculateXYVolume(stl_in);
      if (vol < minVolume){
        minVolume = vol;
        bestAngle = i;
      }
    }
  }
  stl_rotate_z(&stl_in, -85 + bestAngle);
  stl_exit_on_error(&stl_in);
  stl_write_binary(&stl_in, argv[2], "ADMesh");
  stl_exit_on_error(&stl_in);
  cout << bestAngle << endl << minVolume << endl;
  stl_close(&stl_in);
  return 0;
}
