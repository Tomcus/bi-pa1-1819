/**
 * Chair
 * Model jednoduché židle s opěradlem složeným z příček.
 * Opěradlo je pro uznání nutné dělat forcykly!
 * @param rung_number Počet příček
 * @param seat_width Šířka čtvercového sedáku (zároveň i délka)
 * @param seat_thickness Tloušťka sedáku
 * @param feet_width Tloušťka nohou i příček
 * @param feet_length Délka nohou židle. Musí být počítáno od spodní strany sedáku!
 * @author Marek Žehra
 */

module chair(
rung_number=5,
seat_width=19,
seat_thickness=2,
feet_width=2,
feet_length=15
) {
  //insert your code here...
}