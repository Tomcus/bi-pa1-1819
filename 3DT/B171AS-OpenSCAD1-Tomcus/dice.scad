/**
 * Dice
 * Základní tvar průnik kostky a koule
 * Tečky je pro uznání nutné dělat forcykly!
 * @param cs Rozměr stěny kostky
 * @param ds Rozměr puntíku (poloměr)
 * @author Tomáš Bařtipán, Miro Hrončok
 */
 
module baseShape(cs) {
    intersection() {
        cube(cs, center=true);
        sphere(cs*0.75);
    }
}
module pointAt(pos, ds) {
    translate(pos) cylinder(r=ds, h=4);
}
module pointOnSide(numb, cs, ds) {
    if (numb%2 == 1)
        pointAt([0,0,cs/2 - 2], ds);
    if (numb >= 2){
        pointAt([cs/3.5,cs/3.5,cs/2 - 2], ds);
        pointAt([-cs/3.5,-cs/3.5,cs/2 - 2], ds);
    }
    if (numb >= 4){
        pointAt([-cs/3.5,cs/3.5,cs/2 - 2], ds);
        pointAt([cs/3.5,-cs/3.5,cs/2 - 2], ds);
    }
    if (numb == 6) {
        pointAt([0,-cs/3.5,cs/2 - 2], ds);
        pointAt([0,cs/3.5,cs/2 - 2], ds);
    }
}
module dice(cs=40, ds=3) {
    difference() {
        baseShape(cs);
        for (numb = [1:6]){
            if (numb <= 4) {
                rotate([numb * 90, 0, 0]) pointOnSide(numb,cs,ds); 
            } else {
                rotate([numb * 180, 90, 0]) pointOnSide(numb,cs,ds);
            }
        }
    }
}

dice(cs=80,ds=10);