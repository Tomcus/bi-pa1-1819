/**
 * Parametric drill stand
 * Model držáku na vrtáky
 * Jednotlivé řady je pro uznání nutné dělat forcykly!
 * @param base_height Výška držáku
 * @param holes Počet děr (předpoklad je po kroku 1mm průměr)
 * @param hole_length Výška děr
 * @param stand_step_width Šířka jedné řady děr
 * @param stand_step_num Počet děr na řadu
 * @author Marek Žehra
 */
 
 
 module drill_stand(
 base_height=10,
 holes=10,
 hole_length=5,
 stand_step_width=15,
 stand_step_num=5
 ) {
  //insert your code here...
}