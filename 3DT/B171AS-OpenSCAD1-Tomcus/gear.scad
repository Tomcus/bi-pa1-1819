/**
 * Parametric gear
 * Model jednoduchého ozubeného kola
 * Zuby je pro uznání nutné dělat forcykly!
 * @param gear_rad Poloměr ozubeného kola
 * @param gear_thickness Tloušťka kola
 * @param center_hole_width Šířka čtverce uprostřed kola
 * @param tooth_width Šířka jednoho zubu
 * @param tooth_prot Výstup zubu (jak moc je vystouplý zub)
 * @param tooth_count Počet zubů na ozubeném kolu po obvodu
 * @author Marek Žehra
 */
 
module gear(
 gear_rad=50,
 gear_thickness=10,
 center_hole_width=10,
 tooth_width=5,
 tooth_prot=5,
 tooth_count=20
)  {
  //insert your code here...
}