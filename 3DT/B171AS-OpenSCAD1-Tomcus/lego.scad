/**
 * Lego brick
 * @author Jakub Prusa
 * @documentation http://cdn.instructables.com/F65/PI3W/HDYZBK5Y/F65PI3WHDYZBK5Y.LARGE.jpg
 * @param num_x pocet pinu na ose x
 * @param num_y pocet pinu na ose y
 * @param num_z vyska na ose z ale ne v mm ale v jednotkach lega, obycejny dil ma vysky 3
 * @param smooth jestli dil ma byt hladky nebo ne (bez cudliku)
 * rozmery jednotlivych casti
 * prumer cudliku je 4.8 mm
 * vyska cudliku 1.8 mm
 * rozestup je 8 mm
 * vyska vrstvy bez cudliku je 3.2 mm
 * tlouska steny je 1.2 mm
 *
 * uvnitr kosticky nereste zadne cudliky jako v realu, staci ze bude prazdna a tlouska steny bude odpovidat parametru
 * a jak vite nektere lego dilky jsou hladke takze nemaji nahore ty cudliky. Na to je zde promena typu BOOL ktera se jmenuje smooth
 */
module lego_brick(num_x=10,num_y=2,num_z=1,smooth=false) {
  //insert your code here...
}
