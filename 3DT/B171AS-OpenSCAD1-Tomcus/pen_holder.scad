/**
 * Parametric pen holder
 * Model držáku na tužky/propisky/fixy
 * Jednotlivé otvory je pro uznání nutné dělat forcykly!
 * @param bottom_rad Spodní poloměr základny
 * @param base_height Výška základny
 * @param top_rad Horní poloměr základny
 * @param hole_count Počet otvorů na pera
 * @param hole_rad Poloměr otvoru na pero
 * @param prot_thickness Síla stěny otvorů
 * @param prot_angle Úhel naklonění otvorů
 * @author Marek Žehra
 */
 
module pen_holder(
 bottom_rad=50,
 base_height=50,
 top_rad=25,
 hole_count=5,
 hole_rad=10,
 prot_thickness=2,
 prot_angle=20
) {
  //insert your code here...
}