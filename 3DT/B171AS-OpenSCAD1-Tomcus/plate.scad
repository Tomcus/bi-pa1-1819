/**
 * Plate
 * Obdélníková podložka pod elektroniku na 4 rohové šroubky
 * @param x Šířka podložky
 * @param y Délka podložky
 * @param z Výška/tloušťka posložky
 * @param c Vzdálenost osy šroubu od rohu (všude stejná)
 * @param r Poloměr díry na šroub
 * @param b Poloměr sloupku na šroub
 * @param h Výška sloupku na šroub
 * @author Miro Hrončok, Jakub Průša
 */
module plate(x=70,y=90,z=2,c=5,r=1.5,b=2.5,h=3) {
  //insert your code here...
}