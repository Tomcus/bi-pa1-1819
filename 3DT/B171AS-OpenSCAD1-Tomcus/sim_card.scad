//%import("../sim_card.stl");

/**
 * Parametricky adapter na sim kartu
 * @author Jakub Prusa
 * @param x1,y1 jsou vnejsi rozmery adatperu
 * @param x2,y2 jsou vnejsi rozmery sim karty
 * @param z je spolecna vyska pro cely adapter
 * @param off_x,off_y je offset od praveho dolni rohu, pokud oba budou nastaveny na 0 adapter bude mit vykousnu roh
 * Pokud bude rozmer sim karty vetsi nez adapteru tak upravte hodnoty na vami vhodne zvolene
 * Sesikmeni rohu udelejte pro zjednoduseni tak aby prochazelo 3/4 strany Y. 
 */
module card_shape(x, y, z){
   difference(){
        cube([x,y,z]);
        translate([0,(3*y)/4,0])rotate([0,0,45]) cube([x,y,z]);
    }
} 

module sim_card(x1=18,y1=14,x2=10,y2=8,z=1,off_x=1,off_y=1) {
    x1 = (x1 > x2)? x1: x1+x2;
    y1 = (y1 > y2)? y1: y1+y2;
    newX1 = (x1-x2-off_x > 0) ? x1: (x1 + abs(x1-x2-off_x) + 2);
    newY1 = ((y2 + off_y) < y1) ? y1: (y1 + abs(y1-y2-off_y) + 2);
    translate([(-x1)/2,(-newY1)/2,(-z)/2]) difference(){
        card_shape(newX1,newY1,z);
        translate([newX1-x2-off_x,off_y,0]) card_shape(x2,y2,z);
    }
}