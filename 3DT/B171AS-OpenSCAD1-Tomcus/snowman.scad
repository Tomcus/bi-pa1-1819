%import("../snowman.stl");

/** 
 * Snowman
 * @param r Poloměr největší koule
 * @param factor Velikost menší koule jako zlomek velikosti koule pod ní (0.7 = 70 %)
 * @param overlap Překryv menší koule s koulí pod ní jako zlomek výšky spodní koule (0.2 = 20 %)
 * Jde zde pouze o 3 "sněhové" koule, nikoliv o ozdoby na sněhulákovi
 * @author Miro Hrončok
 */
module snowman(r=50,factor=0.7,overlap=0.2)  {
    r1 = r;
    r2 = factor * r;
    r3 = factor * factor * r;
    h2 = r1 * (1-overlap*2) + r2;
    h3 = r2 * (1-overlap*2) + h2 + r3;
    sphere(r=r1);
    translate([0,0,h2]) sphere(r=r2);
    translate([0,0,h3]) sphere(r=r3);
  
}
snowman(r=25, factor=1, overlap=0);