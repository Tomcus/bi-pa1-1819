/**
 * Parametric wall hook
 * háčku na stěnu s možností modifikace parametrů
 * Jednotlivé otvory je pro uznání nutné dělat forcykly!
 * @param hook_rad VNITŘNÍ poloměr háčku
 * @param hook_thickness Tloušťka háčku (možno představit jako tloušťka ve směru od stěny)
 * @param hook_width Šířka háčku 
 * @param top_hook_angle Naklonění horní části háčku (prodloužení na kabáty)
 * @param top_hook_length Délka horní části (prodloužení na kabáty)
 * @param top_hook_distance Vzdálenost spodní a horní části (délka rovné plochy s dírami na šrouby)
 * @param hole_rad Poloměr děr na šrouby/hřebíky
 * @param hole_count Počet děr na šrouby/hřebíky

 * @author Marek Žehra
 */

module wall_hook(
 hook_rad=10,
 hook_thickness=10,
 hook_width=10,
 top_hook_angle=30,
 top_hook_length=50,
 top_hook_distance=30,
 hole_rad=2,
 hole_count=2
) {
  //insert your code here...
}