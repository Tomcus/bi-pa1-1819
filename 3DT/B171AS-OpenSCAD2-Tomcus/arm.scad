/**
 * Arm
 * rameno napriklad na RC auticko
 * @param height vyska ramena 
 * @param offset vyosetni horni a spodni casti
 * @param thick tloustka ramena
 * @param number_holes pocet der nahore a dole
 * @param hole_radius polomer der v ramenu
 * mezera mezi diramy je jejich radius, to stejne od kraje
 * vyska je brana ze stredu der ke stredu der
 * off set je pocita opet ze stredu der 
 * @author Jakub Průša/Michael Očenášek
 */
 
module arm(
    height=60,
    offset=-25,
    thick=3,
    number_holes=2,
    hole_radius=3
) {
    // < Insert your code here >
}