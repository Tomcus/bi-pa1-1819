/** 
 * Disc
 * Modul parametrického disku k robotovi. Jedná se o kolo s gumovou pneumatikou o kruhovém průřezu.
 * @param radius Poloměr disku
 * @param height Výška disku
 * @param r_hole Poloměr díry uprostřed kola
 * @param reduce_offset Odsazení odlehčujících děr od stran kola
 * @param reduce_num Počet odlehčujících děr
 * @author Marek Žehra
 */

module wheel(radius=50,height=10,r_hole=2,reduce_offset=5,reduce_num=4) {
    difference() {
        cylinder(r=radius, h=height, center=true);
        rotate_extrude() translate([radius,0]) circle(r=height/2);
        cylinder(r=r_hole, h=height, center=true);
        max_support_size=radius-height/2;
        support_r=max_support_size/4;
        rotation_step=360/reduce_num;
        for(i = [1:reduce_num]){
            rotate([0,0,i*rotation_step]) translate([0,max_support_size-reduce_offset-support_r]) cylinder(r=support_r, h=height, center=true);
        }
    }
}

wheel(radius=10,height=10,r_hole=0,reduce_offset=5,reduce_num=3);
