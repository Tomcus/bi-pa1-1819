/**
 * Helix
 * @param d Vzdálenost středů šroubovic měřená na vodorovné rovině
 * @param o Poloměr šroubovice měřený na vodorovné rovině
 * @param h Výška šroubovice bez podstav
 * @param s Stupně rotace na milimetr výšky (znaménko určuje směr otáčení)
 * @param db Průměr podstav
 * @param hb Výška podstav
 * Model byl vyexportován s nastavením $fn=50 a výškové rozlišení je 50 µm
 * @author Miro Hrončok
 */

module helix(d=10,o=1,h=50,s=18,db=13,hb=1) {
    linear_extrude(height=h, twist=(s*h), slices=h*100) {
        translate([d/2,0]) circle(r=o);
        translate([-d/2,0]) circle(r=o);
    }
    translate([0,0,-hb]) cylinder(d=db, h=hb);
    translate([0,0,h]) cylinder(d=db, h=hb);
}

helix();
