/**
 * Mask
 * objekt podobny skrabosce
 * @param eye_offset vzdelenost stredu oci
 * @param wall tloustka steny
 * @param eye_radius dira pro oko
 * @param outer_radius vnejsi radius ze stredu oka
 * @param nose1 horni polomer nosu
 * @param nose2 spodni polomer nosu
 * @param nose_length delka nosu
 * Horni stred nosu je ve stejne vysce jako stred oci
 * @author Jakub Průša
 */

module mask(
	eye_offset=40,
	wall=2,
	eye_radius=15,
	outer_radius=30,
	nose1=15,
	nose2=20,
	nose_length=40
){
  // < Insert your code here >
}
