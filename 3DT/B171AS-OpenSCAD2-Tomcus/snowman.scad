/** 
 * Snowman
 * @param r Poloměr největší koule
 * @param factor Velikost menší koule jako zlomek velikosti koule pod ní (0.7 = 70 %)
 * @param overlap Překryv menší koule s koulí pod ní jako zlomek výšky spodní koule (0.2 = 20 %)
 * @param balls Počet koulí (pro uznání nutno řešit rekurzí)
 * Jde zde pouze o "sněhové" koule, nikoliv o ozdoby na sněhulákovi
 * @author Miro Hrončok
 */
module draw_snowball(position, r, factor, overlap, rem_balls){
    if (rem_balls>0) {
        sphere(r=r);
    }
}
module snowman(r=50,factor=0.7,overlap=0.2,balls=3) {
	draw_snowball([0,0,0], r, factor, overlap, 2);
}

snowman();
