/** 
 * Whistle
 * Modul píšťalky s poutkem o kruhovém průřezu. Průměr poutka se mění dynamicky podle parametru výšky píšťalky.
 * @param radius Poloměr dutiny (vnější hrana)
 * @param height Výška píšťalky (na výšce je zásilý průměr poutka)
 * @param r_handle Poloměr poutka (tloušťka poutka)
 * @param thickness Síla stěny píšťalky
 * @param beak_length Délka "zobáku" u píšťalky měřeno od středu dutiny
 * @param beak_thick Šířka "zobáku" u píšťalky (vnitřní díra)
 * @param hole_width Šířka díry na horní straně píšťalky
 * @author Marek Žehra
 */
 
module whistle(radius=10,height=20,r_handle=2,thickness=2,beak_length=20,beak_thick=5,hole_width=5) {
	// < Insert your code here >
}
