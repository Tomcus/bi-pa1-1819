# Secret service report

## 3D models

  - [DT](./donald_trump.stl)
  - [KJU](./kim_jong_un.stl)

## DT mapped to KJU

![DT mapped to KJU](./dt_kju_front.png)

## KJU mapped to DT

![KJU mapped to DT](./kju_dt_front.png)
