module elipse_cylinder(length, r){
    cylinder(h=length, r=r);
    sphere(r=r);
    translate([0,0,length]) sphere(r=r);
}

module arm(branch_number, circr, r, alpha){
    new_r = circr-r;
    x = sqrt((new_r*new_r)/(25 - 24*cos(180-alpha)));
    elipse_cylinder(4*x, r);
    angle = 360/branch_number;
    for (i = [1:branch_number]){
        translate([0,0,4*x]) rotate([0,0,i*angle]) rotate([alpha, 0, 0]) elipse_cylinder(3*x, r);
    }
}

function linspace_pos(min, max, number, position) = min + (position*((max-min)/(number-1)));

module flake(branches=[], circr=30, r=3, ang=45) {
    n = len(branches);
    golden_angle = 180 * (3 - sqrt(5));
    for (i = [0:n-1]){
        //propocet souradnic bodu na povrchu koule pomoci Vogelova algoritmu http://blog.marmakoide.org/?p=1
        theta = i*golden_angle;
        z = linspace_pos(1 - 1.0 / n, 1.0 / n - 1, n, i);
        radius = sqrt(1 - z * z);
        x = radius * cos(theta);
        y = radius * sin(theta);
        //translate([circr*x, circr*y, circr*z]) sphere(r=3);
        if (branches[i] > 0){
            if (branches[i] == 1){
                rotate([0,0,theta]) rotate([0,atan2(radius, z),0])elipse_cylinder(circr-r, r);
            } else {
                rotate([0,0,theta]) rotate([0,atan2(radius, z),0]) arm(branches[i], circr, r, ang);
            }
        } else {
            echo("ERROR - nekladna hodnota v branches");
        }
    }
}
/*[2, 2, 3, 2, 5, 3, 3, 2, 3, 2, 5, 3] [4, 4, 5, 1, 3, 3, 1, 3, 3, 1, 2, 3, 3] [2, 4, 2, 3, 3, 2, 1, 3, 2, 2, 3, 4, 3, 4, 3, 3, 4, 3]*/
//%sphere(r=30);
flake(branches=[2, 2, 3, 2, 5, 3, 3, 2, 3, 2, 5, 3]);