module trc(tv,rv,cr,ch){
    translate(tv)
        rotate(rv)
            cylinder(r=cr,h=ch);
}

module molecule(atom_radius = 1, atom_count=3, atom_dist=3, br=0.2){
    for(x = [0:atom_count-1], y = [0: atom_count - 1], z= [0: atom_count - 1]){
        translate([x*atom_dist, y*atom_dist, z*atom_dist])
            sphere(r=atom_radius);  
    }
    
    for(i = [0:atom_count-1], j = [0: atom_count - 1]){
        trc([i*atom_dist, j*atom_dist,0], [0,0,0], br,(atom_count-1)*atom_dist);
        trc([i*atom_dist, 0,j*atom_dist],[-90,0,0],br,(atom_count-1)*atom_dist);
        trc([0, i*atom_dist ,j*atom_dist],[0,90,0],br,(atom_count-1)*atom_dist);
    }
}

molecule();