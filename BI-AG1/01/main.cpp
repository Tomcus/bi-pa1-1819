#include <iostream>
#include <sstream>

using namespace std;

template <class T>
class CQueue{
public:
    CQueue(){
        queue = new T[DEFAULT_QUEUE_SIZE];
        size = DEFAULT_QUEUE_SIZE;
        lookAt = placeAt = 0;
    }
    ~CQueue(){
        delete [] queue;
    }
    bool canPop(){
        return placeAt > lookAt;
    }
    void push(const T& in){
        if (placeAt >= size - 1)
            resize();
        queue[placeAt++] = in;
    }
    T& pop(){
        if (canPop())
            return queue[lookAt++];
        return queue[lookAt];
    }
protected:
    const size_t DEFAULT_QUEUE_SIZE = 500;
    const int DEFAULT_QUEUE_RESIZE_VALUE = 2;
    void resize(){
        T* oldQueue = queue;
        size_t oldSize = size;
        size *= DEFAULT_QUEUE_RESIZE_VALUE;
        queue = new T[size];
        for (size_t i = lookAt; i < oldSize; ++i) {
            queue[i-lookAt] = oldQueue[i];
        }
        placeAt -= lookAt;
        lookAt = 0;
        delete [] oldQueue;
    }
    size_t lookAt;
    size_t placeAt;
    size_t size;
    T* queue;
};

template <class T>
class CArray{
public:
    CArray(size_t basicSize = 500){
        realSize = basicSize;
        if (basicSize != 0)
            array = new T[realSize];
        else
            array = NULL;
        size = 0;
    }
    CArray(size_t basicSize, T& defaultValue){
        realSize = basicSize;
        array = new T[realSize];
        size = 0;
        for (int i=0; i < realSize; ++i){
            array[i] = defaultValue;
        }
    }
    CArray(const CArray & a){
        realSize = a.realSize;
        size = a.size;
        array = new T[realSize];
        for (int i = 0; i < realSize; ++i){
            array[i] = a.array[i];
        }
    }
    ~CArray(){
        if (array != NULL)
            delete [] array;
    }
    void operator=(const CArray & a){
        realSize = a.realSize;
        size = a.size;
        if (array != NULL)
            delete [] array;
        array = new T[realSize];
        for (size_t i = 0; i < size; ++i){
            array[i] = a.array[i];
        }
    }
    void add(T& newValue){
        array[size++] = newValue;
    }
    T& get(size_t index){
        return array[index];
    }
    size_t size;
protected:
    T* array;
    size_t realSize;
};

class CNode{
public:
    CNode():neighbours(0){
        wasVisited = false;
        visitedFrom = NULL;
        id = 0;
        neighboursCount = 0;
    }
    CNode(size_t id, size_t nc):id(id), neighboursCount(nc),neighbours(nc){
        wasVisited = false;
        visitedFrom = NULL;
    }
    CNode(const CNode &a){
        wasVisited = a.wasVisited;
        visitedFrom = a.visitedFrom;
        id = a.id;
        neighboursCount = a.neighboursCount;
        neighbours = a.neighbours;
    }
    void operator= (const CNode &a){
        wasVisited = a.wasVisited;
        visitedFrom = a.visitedFrom;
        id = a.id;
        neighboursCount = a.neighboursCount;
        neighbours = a.neighbours;
    }
    void addEdge(CNode * neighbour){
        neighbours.add(neighbour);
    }
    bool getVisited(){
        return wasVisited;
    }
    void setVisited(CNode* from, size_t distance){
        wasVisited = true;
        visitedFrom = from;
        this->distance = distance;
    }
    size_t id;
    size_t neighboursCount;
    CArray<CNode*> neighbours;
    size_t distance;
    CNode * visitedFrom;
protected:
    bool wasVisited;
};

class CPather{
public:
    CPather(istream & input = cin){
        input >> nodeCount;
        input >> startingNodeId;
        input >> endingNodeId;
        nodes = new CNode*[LAYERS_COUNT];
        for (int i = 0; i < LAYERS_COUNT; ++i){
            nodes[i] = new CNode[nodeCount];
        }
        for (size_t i = 0; i < nodeCount; ++i){
            loadNode(i, input);
        }
        size_t keyCount;
        input >> keyCount;
        for (size_t i = 0; i < keyCount; ++i) {
            loadKeys(input);
        }
        CNode * finalNode = doBFS();
        if (finalNode == NULL)
            cout << "No solution" << endl;
        else{
            createPath(finalNode);
            cout << finalNode->distance << endl;
            cout << path << endl;
        }
    }
    ~CPather(){
        for (int i = 0; i < LAYERS_COUNT; ++i){
            delete [] nodes[i];//todo:maybe make this more readable :D
        }
        delete [] nodes;
    }
protected:
    const int LAYERS_COUNT = 8;
    void loadNode(int i, istream & input){
        int nc;
        input >> nc;
        for (int j = 0; j < LAYERS_COUNT; ++j) {
            nodes[j][i] = CNode((size_t)i, (size_t)nc);
        }
        for (int k = 0; k < nc; ++k) {
            loadNeighbor(i, input);
        }
    }
    void loadNeighbor(int index, istream & input){
        size_t ni;
        char key;
        input >> ni;
        input >> key;
        switch (key){
            case 'a':
                //cout << "a" << endl;
                nodes[1][index].addEdge(&nodes[1][ni]);
                nodes[3][index].addEdge(&nodes[3][ni]);
                nodes[5][index].addEdge(&nodes[5][ni]);
                nodes[7][index].addEdge(&nodes[7][ni]);
                break;
            case 'b':
                //cout << "b" << endl;
                nodes[2][index].addEdge(&nodes[2][ni]);
                nodes[3][index].addEdge(&nodes[3][ni]);
                nodes[6][index].addEdge(&nodes[6][ni]);
                nodes[7][index].addEdge(&nodes[7][ni]);
                break;
            case 'c':
                //cout << "c" << endl;
                nodes[4][index].addEdge(&nodes[4][ni]);
                nodes[5][index].addEdge(&nodes[5][ni]);
                nodes[6][index].addEdge(&nodes[6][ni]);
                nodes[7][index].addEdge(&nodes[7][ni]);
                break;
            case 'x':
            default:
                //cout << "x" << endl;
                for (int i = 0; i < LAYERS_COUNT; ++i) {
                    nodes[i][index].addEdge(&nodes[i][ni]);
                }
                break;
        }
    }
    void loadKeys(istream & input){
        size_t ni;
        char key;
        input >> ni;
        input >> key;
        switch (key){
            case 'a':
                for (int i = 0; i < LAYERS_COUNT/2; ++i) {
                    nodes[2*i][ni].neighbours = nodes[2*i + 1][ni].neighbours;
                }
                break;
            case 'b':
                nodes[0][ni].neighbours = nodes[2][ni].neighbours;
                nodes[1][ni].neighbours = nodes[3][ni].neighbours;
                nodes[4][ni].neighbours = nodes[6][ni].neighbours;
                nodes[5][ni].neighbours = nodes[7][ni].neighbours;
                break;
            case 'c':
                for (int i = 0; i < LAYERS_COUNT/2; ++i) {
                    nodes[i][ni].neighbours = nodes[i+4][ni].neighbours;
                }
                break;
            default:
                break;
        }
    }
    void createPath(CNode * endNode){
        CArray<size_t> arr(endNode->distance + 1);
        CNode * ptr = endNode;
        ostringstream o;
        while(ptr->visitedFrom != NULL){
            arr.add(ptr->id);
            ptr = ptr->visitedFrom;
        }
        arr.add(ptr->id);
        for (int i = (int)endNode->distance; i >= 0; --i){
            if (i != 0){
                o << arr.get(i) << " ";
            } else {
                o << arr.get(i);
            }
        }
        path = o.str();
    }
    CNode* doBFS(){
        CQueue<CNode *> q;
        q.push(&nodes[0][startingNodeId]);
        nodes[0][startingNodeId].setVisited(NULL, 0);
        while(q.canPop()){
            CNode * node = q.pop();
            for (size_t i = 0; i < node->neighbours.size; ++i) {
                CNode * neighbour = node->neighbours.get(i);
                if (!neighbour->getVisited()) {
                    neighbour->setVisited(node, node->distance + 1);
                    if (neighbour->id == endingNodeId)
                        return neighbour;
                    q.push(neighbour);
                }
            }
        }
        return NULL;
    }
    CNode ** nodes;
    size_t nodeCount;
    size_t startingNodeId;
    size_t endingNodeId;
    string path;
};

int main() {
    CPather path;
    return 0;
}