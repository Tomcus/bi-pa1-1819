#include <iostream>
#include <string>
#include "common.cpp" // furt je to prasarna :D

int main(void){
  string hexText = readLineFromStdin();
  if (hexText.size() <= 0)
    errExit("Vstupni text byl delky 0. NESPRAVNY VSTUP!");
  cout << hex2text(hexText) << endl;
  return 0;
}
