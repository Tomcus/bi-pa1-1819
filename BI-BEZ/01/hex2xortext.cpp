#include <string>   //string
#include <iostream> // cin, cout
#include <algorithm> // min
#include "common.cpp" //uz bych to asi nemel pouzivat :D

using namespace std;

int main(void){
  string hexText1 = readLineFromStdin();
  string hexText2 = readLineFromStdin();
  string res = "";
  if (hexText1.size() <= 0)
    errExit("Vstupni text 1 byl delky 0. NESPRAVNY VSTUP!");
  if (hexText1.size()%2 == 1)
    errExit("Vstupni text 1 neni v hexadecimalni soustave. NESPRAVNY VSTUP!");
  if (hexText2.size() <= 0)
    errExit("Vstupni text 2 byl delky 0. NESPRAVNY VSTUP!");
  if (hexText2.size()%2 == 1)
    errExit("Vstupni text 2 neni v hexadecimalni soustave. NESPRAVNY VSTUP!");
  cout << xor2texts(hexText1, hexText2) << endl;
  return 0;
}
