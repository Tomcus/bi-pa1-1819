#include <iostream>
#include <string>
#include "./common.cpp"

using namespace std;

int main(void){
  string text = readLineFromStdin();
  if (text.size() <= 0)
    errExit("Vstupni text byl delky 0. NESPRAVNY VSTUP!");
  cout << text2hex(text) << endl;
}
