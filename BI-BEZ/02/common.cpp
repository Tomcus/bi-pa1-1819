#include <iostream>
#include <string>
#include <cctype>

using namespace std;

void errExit(const char * errMessage){
  cout << errMessage << endl;
  exit(1);
}

string readLineFromStdin(){
  string text = "";
  getline(cin,text);
  return text;
}

bool isInRange(const char val, const char min, const char max){
  return (val >= min && val <= max);
}

bool isHex(const char c){
  return isInRange(tolower(c), 'a', 'f') || isInRange(c, '0', '9');
}

bool isHex(const char c1, const char c2){
  return isHex(c1) && isHex(c2);
}

char hexToChar(const char c){
  if (isInRange(c, '0', '9'))
    return (c - '0');
  else if (isInRange(c, 'a', 'f'))
    return (c + 10 - 'a');
  return 0;
}

char hexToChar(const char c1, const char c2){ //format: c1c2 => c1*16 + c2
  return hexToChar(c1) * 16 + hexToChar(c2);
}

string text2hex(const string& input){
  string res = "";
  for (size_t i = 0; i < input.size(); ++i){
    char p1, p2;
    p1 = (unsigned char)input[i]/16;
    p2 = (unsigned char)input[i]%16;
    if (p1 < 10){
      res += p1 + '0';
    } else {
      res += p1 - 10 + 'a';
    }
    if (p2 < 10){
      res += p2 + '0';
    } else {
      res += p2 - 10 + 'a';
    }
  }
  return res;
}

string hex2text(const string& input){
  string res = "";
  if (input.size()%2 == 1)
    errExit("Error");
  for (size_t i = 0; i < input.size()/2; ++i){
    if (isHex(input[2*i], input[(2*i)+1])){
      res += hexToChar(input[2*i],input[2*i + 1]);
    } else {
      errExit("Error");
    }
  }
  return res;
}

char XorChars(const char c1, const char c2){
  return c1 ^ c2;
}

string xor2texts(const string& hexText1, const string& hexText2){
  string res = "";
  for (size_t i = 0; i < min(hexText1.size(), hexText2.size()) ; ++i){
    res += XorChars(hexText1[i], hexText2[i]);
  }
  return res;
}
