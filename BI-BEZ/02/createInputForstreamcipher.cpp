#include <iostream>
#include <cstring>
#include <string>
#include "./common.cpp"
#include <openssl/evp.h>

using namespace std;

int main(void){
  unsigned char ot[1024] = "Stepan je vul :D";  // open text
  unsigned char secretText[1024] = "Sheldon Cooper je idiot ale mameho radi";
  unsigned char st[1024];  // sifrovany text
  unsigned char key[EVP_MAX_KEY_LENGTH] = "Muj klic";  // klic pro sifrovani
  unsigned char iv[EVP_MAX_IV_LENGTH] = "inicial. vektor";  // inicializacni vektor
  const char cipherName[] = "RC4";
  const EVP_CIPHER * cipher;

  OpenSSL_add_all_ciphers();
  /* sifry i hashe by se nahraly pomoci OpenSSL_add_all_algorithms() */
  cipher = EVP_get_cipherbyname(cipherName);
  if(!cipher) {
    printf("Sifra %s neexistuje.\n", cipherName);
    exit(1);
  }

  int otLength = strlen((const char*) ot);
  int stLength = 0;
  int tmpLength = 0;

  EVP_CIPHER_CTX ctx; // struktura pro kontext

  /* Sifrovani */
  EVP_EncryptInit(&ctx, cipher, key, iv);  // nastaveni kontextu pro sifrovani
  EVP_EncryptUpdate(&ctx,  st, &tmpLength, ot, otLength);  // sifrovani ot
  stLength += tmpLength;
  EVP_EncryptFinal(&ctx, st + stLength, &tmpLength);  // dokonceni (ziskani zbytku z kontextu)
  stLength += tmpLength;
  cout << string((char *)ot, otLength) << endl;
  cout << text2hex(string((char *)st, stLength)) << endl;

  otLength = strlen((const char*) secretText);
  stLength = 0;
  EVP_EncryptInit(&ctx, cipher, key, iv);
  EVP_EncryptUpdate(&ctx,  st, &tmpLength, secretText, otLength);  // sifrovani ot
  stLength += tmpLength;
  EVP_EncryptFinal(&ctx, st + stLength, &tmpLength);  // dokonceni (ziskani zbytku z kontextu)
  stLength += tmpLength;
  cout << text2hex(string((char *)st, stLength)) << endl;
  return 0;
}
