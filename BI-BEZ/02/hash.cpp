#include <iostream>
#include <string>
#include "./common.cpp"
#include <openssl/evp.h>

using namespace std;
// Maximalni delka stringu ktery se bude generovat a zkouset jako zacatek stringu
const size_t MAX_STRING_SIZE = 6;

class CHash{
public:
  CHash(const string& stype){
    OpenSSL_add_all_digests();
    type = EVP_get_digestbyname(stype.c_str());
    if (!type){
      errExit("Error");
    }
  }
  ~CHash(){
    EVP_cleanup();
  }
  string hash(const string& input) const{
    EVP_MD_CTX* context = EVP_MD_CTX_create();
    unsigned char hash[EVP_MAX_MD_SIZE];
    int length;
    EVP_DigestInit(context, type);
    EVP_DigestUpdate(context, input.c_str(), input.size());
    EVP_DigestFinal(context, hash, (unsigned int *) &length);
    EVP_MD_CTX_destroy(context);
    return string((char *)hash, length);
  }
  string hash(const char* input, const size_t size) const{
    EVP_MD_CTX* context = EVP_MD_CTX_create();
    unsigned char hash[EVP_MAX_MD_SIZE];
    int length;
    EVP_DigestInit(context, type);
    EVP_DigestUpdate(context, input, size);
    EVP_DigestFinal(context, hash, (unsigned int *) &length);
    EVP_MD_CTX_destroy(context);
    return string((char *)hash, length);
  }
  //static bool OpenSSLInit;
  size_t getLength(){
    if (size == 0){
      EVP_MD_CTX* context = EVP_MD_CTX_create();
      unsigned char hash[EVP_MAX_MD_SIZE] = "";
      int length;
      EVP_DigestInit(context, type);
      EVP_DigestUpdate(context, "size", 4);
      EVP_DigestFinal(context, hash, (unsigned int *) &length);
      EVP_MD_CTX_destroy(context);
      size = (size_t) length;
    }
    return size;
  }
private:
  size_t size = 0;
  const EVP_MD *type;
};

size_t compareBeginings(const string& a, const string& b){
  size_t sameChars = 0;
  for (size_t i = 0; i < min(a.size(),b.size()); ++i){
    if (a[i] == b[i]){
      sameChars ++;
    } else {
      break;
    }
  }
  return sameChars;
}

string * tryForKLengthStrings(const size_t k, const CHash & hash, const string& originalHash){
  if (k == 0){
    if (compareBeginings(hash.hash("",0), originalHash) == originalHash.size()){
      string * res = new string("");
      return res;
    } else {
      return NULL;
    }
  } else {
    char* str = new char[k+1];
    for (size_t i = 0; i < k; ++i){
      str[i] = 1;
    }
    str[k] = 0;
    str[0] = 0;
    while (true) {
      str[0] += 1;
      for (size_t i = 0; i < k; ++i){
        if (str[i] == 0){
          if (i == k - 1){
            delete [] str;
            return NULL;
          }
          if (str[i + 1] != 0)
            str[i] += 1;
          str[i+1] += 1;
        } else {
          break;
        }
      }
      if (compareBeginings(hash.hash(str,k), originalHash) == originalHash.size()){
        string * res = new string(str);
        delete [] str;
        return res;
      }
    }
  }
}

const char* USAGE = "Usage: hash [maxStringLength] [begginingOfHash]";

int main (int argc, const char* argv[]) {
  if (argc != 3){
    cout << USAGE << endl;
    return 1;
  }
  string hashedString = hex2text(argv[2]);
  size_t depth = atol(argv[1]);
  string* res;
  CHash hFnc("sha256");
  if (hashedString.size() > hFnc.getLength() || hashedString.size() == 0)
    errExit("Error");
  for (size_t i = 0; i <= depth; i++) {
    res = tryForKLengthStrings(i, hFnc, hashedString);
    if (res != NULL)
      break;
  }
  if (res == NULL){
    errExit("Error");
  }
  cout << text2hex(hFnc.hash(*res)) << endl;
  cout << text2hex(*res) << endl;
  delete res;
  return 0;
}
