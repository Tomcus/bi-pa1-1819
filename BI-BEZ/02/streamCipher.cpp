#include <iostream>
#include <string>
#include "./common.cpp"

using namespace std;

int main(void){
  string openText1 = readLineFromStdin();
  string cipherText1 = hex2text(readLineFromStdin());
  string cipherText2 = hex2text(readLineFromStdin());
  if (openText1.size() == 0)
    errExit("Error");
  if (cipherText1.size() == 0)
    errExit("Error");
  if (cipherText1.size() != openText1.size())
    errExit("Error");
  if (cipherText2.size() == 0)
    errExit("Error");
  if (cipherText2.size() > cipherText1.size())
    errExit("Error");
  cout << xor2texts(xor2texts(openText1, cipherText1), cipherText2) << endl;
  return 0;
}
