#include "./CCipher.h"

int CCipher::loaded = 0;

CCipher::CCipher(const string& cipherName){
  if (CCipher::loaded == 0) {
    OpenSSL_add_all_ciphers();
  }
  CCipher::loaded += 1;
  cph = EVP_get_cipherbyname(cipherName.c_str());
  if (!cph){
    throw "ERROR! Neznamy druh sifry";
  }
}

CCipher::~CCipher() {
  CCipher::loaded -= 1;
  if (CCipher::loaded == 0) {
    CRYPTO_cleanup_all_ex_data();
    EVP_cleanup();
  }
}

int CCipher::cypher(string key, string initVector, istream& sourceData, ostream& cypheredData, const size_t length) {
  EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
  if (ctx == nullptr) {
    throw "Cannot create cipher context";
  }

  key = key.substr(0, min(key.size(), size_t(EVP_MAX_KEY_LENGTH)));
  initVector = initVector.substr(0, min(key.size(), size_t(EVP_MAX_IV_LENGTH)));
  unsigned char in_buffer[COPY_SIZE];
  unsigned char out_buffer[COPY_SIZE + 16];
  int realSize = 0;
  int cypherSize = 0;

  EVP_EncryptInit(ctx, cph, (unsigned char*)key.c_str(), (unsigned char*)initVector.c_str());

  for (size_t i = sourceData.tellg(); i < length; i += min(COPY_SIZE, length - i)){
    size_t size = min(COPY_SIZE, length - i);

    sourceData.read((char *)in_buffer, size);
    EVP_EncryptUpdate(ctx, out_buffer, &cypherSize, in_buffer, size);
    cypheredData.write((char *)out_buffer, cypherSize);

    if (!sourceData.good() || !cypheredData.good()){
      throw "Can't read data (wrong size)";
      freeContext(ctx);
    }
    realSize += cypherSize;
  }

  unsigned char mini_buffer[16];
  EVP_EncryptFinal(ctx, mini_buffer, &cypherSize);
  realSize += cypherSize;
  cypheredData.write((char *)mini_buffer, cypherSize);
  if (!cypheredData.good()) {
    throw "Can't write data";
    freeContext(ctx);
  }
  freeContext(ctx);
  return realSize;
}

int CCipher::decrypt(string key, string initVector, istream& cypheredData, ostream& decryptedData, const size_t length) {
  EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
  if (ctx == nullptr) {
    throw "Cannot create cipher context";
  }

  key = key.substr(0, min(key.size(), size_t(EVP_MAX_KEY_LENGTH)));
  initVector = initVector.substr(0, min(key.size(), size_t(EVP_MAX_IV_LENGTH)));
  unsigned char in_buffer[COPY_SIZE];
  unsigned char out_buffer[COPY_SIZE + 16];
  int realSize = 0;
  int cypherSize = 0;

  EVP_DecryptInit(ctx, cph, (unsigned char*)key.c_str(), (unsigned char*)initVector.c_str());

  for (size_t i = cypheredData.tellg(); i < length; i += min(COPY_SIZE, length - i)){
    size_t size = min(COPY_SIZE, length - i);

    cypheredData.read((char *)in_buffer, size);
    EVP_DecryptUpdate(ctx, out_buffer, &cypherSize, in_buffer, size);
    decryptedData.write((char *)out_buffer, cypherSize);

    if (!cypheredData.good() || !decryptedData.good()){
      throw "Can't read data (wrong size)";
      freeContext(ctx);
    }
    realSize += cypherSize;
  }
  unsigned char mini_buffer[16];
  EVP_DecryptFinal(ctx, mini_buffer, &cypherSize);
  decryptedData.write((char *)mini_buffer, cypherSize);
  realSize += cypherSize;
  if (!decryptedData.good()) {
    throw "Can't write data";
    freeContext(ctx);
  }
  freeContext(ctx);
  return realSize;
}

void CCipher::freeContext(EVP_CIPHER_CTX* ctx) {
  EVP_CIPHER_CTX_cleanup(ctx);
}