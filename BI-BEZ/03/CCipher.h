#ifndef CCIPHER_H
#define CCIPHER_H
#include <string>
#include <openssl/evp.h>
#include <iostream>

using namespace std;

const size_t COPY_SIZE = 1024*1024;

class CCipher{
public:
  CCipher(const string& cipherName);
  ~CCipher();
  int cypher(string key, string initVector, istream& sourceData, ostream& cypheredData, const size_t length);
  int decrypt(string key, string initVector, istream& cypheredData, ostream& decryptedData, const size_t length);
private:
  void freeContext(EVP_CIPHER_CTX* ctx);
  static int loaded;
  const EVP_CIPHER * cph;
};

#endif
