#include <iostream>
#include <fstream>
#include <string>
#include "common.cpp"
#include "CCipher.h"

using namespace std;

const char * USAGE = "DesEcbImageEncrypt [key] [image]";

int main(int argc, char * argv[]){
  if (argc != 3){
    cout << USAGE << endl;
    return 1;
  }
  string fileName(argv[2]);

  string outputFileName = fileName.substr(0, fileName.size() - 4);
  outputFileName += "_des_ecb_enc.bmp";
  ifstream input(fileName);
  ofstream output(outputFileName);
  if (!input.good() || !output.good())
    errExit("Error");

  int fileSize = readAndWriteHeader(input, output, getFileSize(fileName));
  int realSize = input.tellg();

  try {
    CCipher crypt("des-ecb");
    realSize += crypt.cypher(argv[2], "inicial. vektor", input, output, fileSize);
  } catch(const string& err) {
    errExit(err);
  }

  output.seekp(2);
  output.write((char *)&realSize,4);

  return 0;
}
