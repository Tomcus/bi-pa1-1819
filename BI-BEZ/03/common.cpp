#include <iostream>
#include <fstream>
#include <openssl/evp.h>

using namespace std;


void errExit(const string& errMessage){
  cout << errMessage << endl;
  exit(1);
}

int getFileSize(string & filePath){
  ifstream file(filePath, ios::binary | ios::ate);
  return (int)file.tellg();
}

int readAndWriteHeader(ifstream & input, ofstream & output, unsigned int realFileSize){
  char* BUFFER = new char [2];
  input.read(BUFFER, 2);
  if (BUFFER[0] != 'B' || BUFFER[1] != 'M' || !input.good() || !output.good()){
    delete [] BUFFER;
    errExit("Error");
  }
  output.write(BUFFER, 2);
  delete [] BUFFER;
  unsigned int fileSize;

  input.read((char*)&fileSize, 4);
  output.write((char*)&fileSize, 4);
  if (!input.good() || !output.good() || fileSize != realFileSize){
    errExit("Error");
  }

  int someData;
  input.read((char*)&someData, 4);
  output.write((char*)&someData, 4);
  if (!input.good() || !output.good()){
    errExit("Error");
  }

  unsigned int dataStart;
  input.read((char*)&dataStart, 4);
  output.write((char*)&dataStart, 4);
  if (!input.good() || !output.good())
    errExit("Error");

  if (dataStart >= fileSize)
    errExit("Error");

  if (dataStart - 14 > 0){
    BUFFER = new char[dataStart - 14];
    input.read(BUFFER, dataStart - 14);
    output.write(BUFFER, dataStart - 14);
    delete [] BUFFER;
    if (!input.good() || !output.good()){
      errExit("Error");
    }
  }
  return fileSize;
}
