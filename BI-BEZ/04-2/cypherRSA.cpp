#include <iostream>
#include <openssl/evp.h>
#include <openssl/pem.h>

const size_t BUFFER_SIZE = 512*1024;
const char * OUTPUT_FILE_NAME = "cypher.out";

using namespace std;

EVP_PKEY * getPubKey(const char * fileName){
  FILE * fkey = fopen(fileName, "rb");
  if (!fkey){
    cout << "Unable to find " << fileName << endl;
    //fclose(fkey);
    return NULL;
  }
  EVP_PKEY * key = PEM_read_PUBKEY(fkey, NULL, NULL, NULL);
  if (!key){
    cout << "Unable to load public key" << endl;
    fclose(fkey);
    return NULL;
  }
  fclose(fkey);
  return key;
}

void doRSA(EVP_PKEY* key, FILE* input, FILE* output){
  unsigned char buffer[BUFFER_SIZE];
  unsigned char buffer_out[BUFFER_SIZE + EVP_MAX_IV_LENGTH];
  unsigned char iv[EVP_MAX_IV_LENGTH] = "test";
  unsigned char *ek = (unsigned char *)malloc (EVP_PKEY_size(key));
  EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
  int len, len_out, eklen;
  
  if (fwrite(iv, EVP_MAX_IV_LENGTH, 1, output) != 1) {
	  free (ek);
	  cout << "Error during filewriting" << endl;
      return;
  }

  if (!EVP_SealInit(ctx, EVP_des_cbc(), &ek, &eklen, iv, &key, 1)){
    free (ek);
    cout << "Error in EVP_SealInit" << endl;
    return;
  }
  if (fwrite(&eklen, sizeof eklen, 1, output) != 1){
    free (ek);
    cout << "Error during filewriting" << endl;
    return;
  }
  cout << EVP_PKEY_size(key) << endl;
  if (fwrite(ek, EVP_PKEY_size(key), 1, output) != 1){
    free (ek);
    cout << "Error during filewriting" << endl;
    return;
  }
  free(ek);
  if (fwrite(iv, EVP_MAX_IV_LENGTH, 1, output) != 1){
    cout << "Error during filewriting" << endl;
    return;
  }
  while ((len = fread(buffer, 1, sizeof buffer, input)) > 0){
    if (!EVP_SealUpdate(ctx, buffer_out, &len_out, buffer, len)){
      cout << "Error during EVP_SealUpdate" << endl;
      return;
    }
    if (fwrite(buffer_out, len_out, 1, output) != 1){
      cout << "Error during filewriting" << endl;
      return;
    }
  }
  if (ferror(input)){
    cout << "Error in input file" << endl;
    return;
  }
  if (!EVP_SealFinal(ctx, buffer_out, &len_out)){
    cout << "Error during EVP_SealUpdate" << endl;
    return;
  }
  if (fwrite(buffer_out, len_out, 1, output) != 1){
    cout << "Error during filewriting" << endl;
    return;
  }
  EVP_CIPHER_CTX_cleanup(ctx);
}

int main(int argc, char* argv[]){
  if (argc != 3){
    cout << "Usage: cypherRSA [data-in] [public-key]" << endl;
    return 1;
  }
  FILE * input, * output;
  input = fopen(argv[1], "rb");
  if (!input){
    cout << "Unable to find " << argv[1] << endl;
    //fclose(input);
    return 1;
  }
  output = fopen(OUTPUT_FILE_NAME, "wb");
  if (!output){
    cout << "Cannot create output file" << endl;
    fclose(input);
    //fclose(output);
    return 1;
  }
  EVP_PKEY * pkey = getPubKey(argv[2]);
  if (pkey == NULL){
    fclose(input);
    fclose(output);
    return 1;
  }
  doRSA(pkey, input, output);

  EVP_PKEY_free(pkey);
  CRYPTO_cleanup_all_ex_data();
  EVP_cleanup();
  fclose(input);
  fclose(output);
  return 0;
}
