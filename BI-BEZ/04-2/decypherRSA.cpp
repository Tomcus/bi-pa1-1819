#include <iostream>
#include <openssl/evp.h>
#include <openssl/pem.h>

const size_t BUFFER_SIZE = 512*1024;
const char * OUTPUT_FILE_NAME = "decypher.out";

using namespace std;

EVP_PKEY * getPrivKey(const char * fileName){
  FILE * fkey = fopen(fileName, "rb");
  if (!fkey){
    cout << "Unable to find " << fileName << endl;
    //fclose(fkey);
    return NULL;
  }
  EVP_PKEY * key = PEM_read_PrivateKey(fkey, NULL, NULL, NULL);
  if (!key){
    cout << "Unable to load private key" << endl;
    fclose(fkey);
    return NULL;
  }
  fclose(fkey);
  return key;
}

void doRSA(EVP_PKEY* key, FILE* input, FILE* output){
  unsigned char buffer[BUFFER_SIZE];
  unsigned char buffer_out[BUFFER_SIZE + EVP_MAX_IV_LENGTH];
  unsigned char iv[EVP_MAX_IV_LENGTH];
  unsigned char *ek = (unsigned char *)malloc (EVP_PKEY_size(key));
  EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
  int len, len_out, eklen;
  if (fread(iv, EVP_MAX_IV_LENGTH, 1, input) <= 0){
      free(ek);
      cout << "Error not enough data" << endl;
      return;
  }
  if (fread(&(eklen), 1, sizeof eklen, input) <= 0){
    free(ek);
    cout << "Error not enough data" << endl;
    return;
  }
  cout << EVP_PKEY_size(key) << endl;
  if (fread(ek, 1, EVP_PKEY_size(key), input) <= 0){
    free(ek);
    cout << "Error not enough data" << endl;
    return;
  }
  if (fread(iv, 1, EVP_MAX_IV_LENGTH, input) <= 0){
    free(ek);
    cout << "Error not enough data" << endl;
    return;
  }
  if (!EVP_OpenInit(ctx, EVP_des_cbc(), ek, eklen, iv, key)){
    free (ek);
    cout << "Error in EVP_OpenInit" << endl;
    return;
  }
  free(ek);
  while ((len = fread(buffer, 1, sizeof buffer, input)) > 0){
    if (!EVP_DecryptUpdate(ctx, buffer_out, &len_out, buffer, len)){
      cout << "Error during EVP_OpenUpdate" << endl;
      return;
    }
    if (fwrite(buffer_out, len_out, 1, output) != 1){
      cout << "Error during filewriting" << endl;
      return;
    }
  }
  if (ferror(input)){
    cout << "Error in input file" << endl;
    return;
  }
  if (!EVP_DecryptFinal(ctx, buffer_out, &len_out)){
    cout << "Error during EVP_OpenUpdate" << endl;
    return;
  }
  if (fwrite(buffer_out, len_out, 1, output) != 1){
    cout << "Error during filewriting" << endl;
    return;
  }
  EVP_CIPHER_CTX_cleanup(ctx);
}

int main(int argc, char* argv[]){
  if (argc != 3){
    cout << "Usage: decypherRSA [data-in] [privte-key]" << endl;
    return 1;
  }
  FILE * input, * output;
  input = fopen(argv[1], "rb");
  if (!input){
    cout << "Unable to find " << argv[1] << endl;
    //fclose(input);
    return 1;
  }
  output = fopen(OUTPUT_FILE_NAME, "wb");
  if (!output){
    cout << "Cannot create output file" << endl;
    fclose(input);
    //fclose(output);
    return 1;
  }
  EVP_PKEY * pkey = getPrivKey(argv[2]);
  if (pkey == NULL){
    fclose(input);
    fclose(output);
    return 1;
  }
  doRSA(pkey, input, output);

  EVP_PKEY_free(pkey);
  CRYPTO_cleanup_all_ex_data();
  EVP_cleanup();
  fclose(input);
  fclose(output);
  return 0;
}
