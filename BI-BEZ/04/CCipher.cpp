#include "./CCipher.h"
#include <cstring>

int CCipher::loaded = 0;

CKey::CKey():key(nullptr) { }

CKey::CKey(EVP_PKEY* key):key(key) { }

CKey::operator EVP_PKEY *() const {
  return key;
}

void CKey::loadPublicKey(const string& pathToKey) {
  FILE* fp = fopen(pathToKey.c_str(), "rb");
  if (fp == nullptr) {
    throw "Unable to open keyfile.";
  }
  key = PEM_read_PUBKEY(fp, nullptr, nullptr, nullptr);
  if (key == nullptr) {
    fclose(fp);
    throw "Unable to load key.";
  }
  fclose(fp);
}

void CKey::loadPrivateKey(const string& pathToKey) {
  FILE* fp = fopen(pathToKey.c_str(), "rb");
  if (fp == nullptr) {
    throw "Unable to open keyfile.";
  }
  key = PEM_read_PrivateKey(fp, nullptr, nullptr, nullptr);
  if (key == nullptr) {
    fclose(fp);
    throw "Unable to load key.";
  }
  fclose(fp);
}

CKey::~CKey(){
  if (key != nullptr) {
    EVP_PKEY_free(key);
  }
}

int CKey::size() const {
  return EVP_PKEY_size(key);
}

CCipher::CCipher(const string& cipherName){
  if (CCipher::loaded == 0) {
    OpenSSL_add_all_ciphers();
  }
  CCipher::loaded += 1;
  cph = EVP_get_cipherbyname(cipherName.c_str());
  if (!cph){
    throw "ERROR! Neznamy druh sifry";
  }
}

CCipher::~CCipher() {
  CCipher::loaded -= 1;
  if (CCipher::loaded == 0) {
    CRYPTO_cleanup_all_ex_data();
    EVP_cleanup();
  }
}

int CCipher::decryptRSA(const CKey& key, istream& cypheredData, ostream& decryptedData) {
  EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
  if (ctx == nullptr) {
    throw "Cannot create cipher context";
  }
  unsigned char initVector[EVP_MAX_IV_LENGTH];
  unsigned char in_buffer[COPY_SIZE];
  unsigned char out_buffer[COPY_SIZE + 16];
  unsigned char* ek = new unsigned char[key.size()];
  int realSize = 0;
  int cypherSize = 0;

  cypheredData.read((char*)initVector, EVP_MAX_IV_LENGTH);
  if (!cypheredData.good()) {
    throw "Unable to read input file";
  }

  cypheredData.read((char *)ek, key.size());
  if (!cypheredData.good()) {
    delete [] ek;
    throw "Unable to read input file";
  }

  if (!EVP_OpenInit(ctx, cph, ek, key.size(), initVector, key)) {
    delete [] ek;
    throw "EVP_SealInit failed";
  }
  delete [] ek;

  while (true) {
    cypheredData.read((char *)in_buffer, COPY_SIZE);
    EVP_OpenUpdate(ctx, out_buffer, &cypherSize, in_buffer, COPY_SIZE);
    decryptedData.write((char *)out_buffer, cypherSize);

    realSize += cypherSize;
    if (cypheredData.eof())
      break;
    
    if (!cypheredData.good() || !decryptedData.good()){
      throw "Can't read data (wrong size)";
      freeContext(ctx);
    }
  }

  unsigned char mini_buffer[16];
  EVP_OpenFinal(ctx, mini_buffer, &cypherSize);
  realSize += cypherSize;
  decryptedData.write((char *)mini_buffer, cypherSize);
  if (!decryptedData.good()) {
    throw "Can't write data";
    freeContext(ctx);
  }
  freeContext(ctx);
  return realSize;
}

int CCipher::cypherRSA(const CKey& key, string initVector, istream& sourceData, ostream& cypheredData) {
  EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
  if (ctx == nullptr) {
    throw "Cannot create cipher context";
  }
  unsigned char iv[EVP_MAX_IV_LENGTH];
  memcpy(iv, initVector.c_str(), EVP_MAX_IV_LENGTH);  
  unsigned char in_buffer[COPY_SIZE];
  unsigned char out_buffer[COPY_SIZE + 16];
  unsigned char* ek = new unsigned char[key.size()];
  int eklen;
  int realSize = 0;
  int cypherSize = 0;

  EVP_PKEY* p_key = key;
  if (!EVP_SealInit(ctx, cph, &ek, &eklen, iv, &p_key, 1)) {
    delete [] ek;
    throw "EVP_SealInit failed";
  }
  
  cypheredData.write((char*)iv, EVP_MAX_IV_LENGTH);
  if (!cypheredData.good()) {
    throw "Unable to write to output file";
  }

  cypheredData.write((char *)ek, eklen);
  if (!cypheredData.good()) {
    delete [] ek;
    throw "Unable to write to output file";
  }
  realSize += key.size();
  delete [] ek;

  while (true) {
    sourceData.read((char *)in_buffer, COPY_SIZE);
    EVP_EncryptUpdate(ctx, out_buffer, &cypherSize, in_buffer, COPY_SIZE);
    cypheredData.write((char *)out_buffer, cypherSize);

    realSize += cypherSize;
    if (sourceData.eof())
      break;
    
    if (!sourceData.good() || !cypheredData.good()){
      throw "Can't read data (file unaccesible)";
      freeContext(ctx);
    }
  }

  unsigned char mini_buffer[16];
  EVP_EncryptFinal(ctx, mini_buffer, &cypherSize);
  realSize += cypherSize;
  cypheredData.write((char *)mini_buffer, cypherSize);
  if (!cypheredData.good()) {
    throw "Can't write data";
    freeContext(ctx);
  }
  freeContext(ctx);
  return realSize;
}


int CCipher::cypher(string key, string initVector, istream& sourceData, ostream& cypheredData, const size_t length) {
  EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
  if (ctx == nullptr) {
    throw "Cannot create cipher context";
  }
  key = key.substr(0, min(key.size(), size_t(EVP_MAX_KEY_LENGTH)));
  initVector = initVector.substr(0, min(initVector.size(), size_t(EVP_MAX_IV_LENGTH)));
  unsigned char in_buffer[COPY_SIZE];
  unsigned char out_buffer[COPY_SIZE + 16];
  int realSize = 0;
  int cypherSize = 0;

  EVP_EncryptInit(ctx, cph, (unsigned char*)key.c_str(), (unsigned char*)initVector.c_str());

  for (size_t i = sourceData.tellg(); i < length; i += min(COPY_SIZE, length - i)){
    size_t size = min(COPY_SIZE, length - i);

    sourceData.read((char *)in_buffer, size);
    EVP_EncryptUpdate(ctx, out_buffer, &cypherSize, in_buffer, size);
    cypheredData.write((char *)out_buffer, cypherSize);

    if (!sourceData.good() || !cypheredData.good()){
      throw "Can't read data (wrong size)";
      freeContext(ctx);
    }
    realSize += cypherSize;
  }

  unsigned char mini_buffer[16];
  EVP_EncryptFinal(ctx, mini_buffer, &cypherSize);
  realSize += cypherSize;
  cypheredData.write((char *)mini_buffer, cypherSize);
  if (!cypheredData.good()) {
    throw "Can't write data";
    freeContext(ctx);
  }
  freeContext(ctx);
  return realSize;
}

CCipher::CCipher(const EVP_CIPHER* cph):cph(cph) { }

int CCipher::decrypt(string key, string initVector, istream& cypheredData, ostream& decryptedData, const size_t length) {
  EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
  if (ctx == nullptr) {
    throw "Cannot create cipher context";
  }

  key = key.substr(0, min(key.size(), size_t(EVP_MAX_KEY_LENGTH)));
  initVector = initVector.substr(0, min(initVector.size(), size_t(EVP_MAX_IV_LENGTH)));
  unsigned char in_buffer[COPY_SIZE];
  unsigned char out_buffer[COPY_SIZE + 16];
  int realSize = 0;
  int cypherSize = 0;

  EVP_DecryptInit(ctx, cph, (unsigned char*)key.c_str(), (unsigned char*)initVector.c_str());

  for (size_t i = cypheredData.tellg(); i < length; i += min(COPY_SIZE, length - i)){
    size_t size = min(COPY_SIZE, length - i);

    cypheredData.read((char *)in_buffer, size);
    EVP_DecryptUpdate(ctx, out_buffer, &cypherSize, in_buffer, size);
    decryptedData.write((char *)out_buffer, cypherSize);

    if (!cypheredData.good() || !decryptedData.good()){
      throw "Can't read data (wrong size)";
      freeContext(ctx);
    }
    realSize += cypherSize;
  }
  unsigned char mini_buffer[16];
  EVP_DecryptFinal(ctx, mini_buffer, &cypherSize);
  decryptedData.write((char *)mini_buffer, cypherSize);
  realSize += cypherSize;
  if (!decryptedData.good()) {
    throw "Can't write data";
    freeContext(ctx);
  }
  freeContext(ctx);
  return realSize;
}

void CCipher::freeContext(EVP_CIPHER_CTX* ctx) {
  EVP_CIPHER_CTX_cleanup(ctx);
}