#ifndef CCIPHER_H
#define CCIPHER_H
#include <string>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <iostream>

using namespace std;

const size_t COPY_SIZE = 256*1024;

class CKey{
public:
  CKey();
  CKey(EVP_PKEY* key);
  ~CKey();
  operator EVP_PKEY*() const;
  void loadPublicKey(const string& pathToKey);
  void loadPrivateKey(const string& pathToKey);
  int size() const;
protected:
  EVP_PKEY* key;
};

class CCipher{
public:
  CCipher(const string& cipherName);
  CCipher(const EVP_CIPHER* cph);
  ~CCipher();
  int cypher(string key, string initVector, istream& sourceData, ostream& cypheredData, const size_t length);
  int cypherRSA(const CKey& key, string initVector, istream& sourceData, ostream& cypheredData);
  int decrypt(string key, string initVector, istream& cypheredData, ostream& decryptedData, const size_t length);
  int decryptRSA(const CKey& key, istream& cypheredData, ostream& decryptedData);

private:
  void freeContext(EVP_CIPHER_CTX* ctx);
  static int loaded;
  const EVP_CIPHER * cph;
};

#endif
