#include <iostream>
#include <fstream>
#include <openssl/evp.h>
#include <string>
#include "CCipher.h"

using namespace std;

int main(int argc, char* argv[]){
  if (argc != 4) {
    cout << "Usage: " << argv[0] << " [public_key] [data_in] [data_out]" << endl;
    return 1;
  }
  try {
    CKey key;
    ifstream data_in(argv[2]);
    ofstream data_out(argv[3]);
    key.loadPrivateKey(argv[1]);
    CCipher cphr(EVP_des_cbc());
    cphr.decryptRSA(key, data_in, data_out);
  }
  catch (const char* err) {
    cout << err << endl;
    return 1;
  }
  return 0;
}
