#include "sec_lib.h"
#include <stdexcept>

uint8_t returnHexVal(std::istream& stream) {
    char c;
    stream >> c;
    if (!stream.good()){
        throw std::runtime_error("Unable to read hexadecimal string from input stream.");
    }
    c = tolower(c);
    if (c >= '0' && c <= '9') {
        return c - '0';
    } else if (c >= 'a' && c <= 'f') {
        return 10 + c - 'a';
    } else {
        throw std::runtime_error("Unable to convert character from input stream");
    }
}

std::vector<uint8_t> sec::hexToVector(std::istream& stream, const std::size_t count) {
    std::vector<uint8_t> res;
    for (std::size_t i = 0; i < count; ++i){
        res.push_back(16*returnHexVal(stream) + returnHexVal(stream));
    }
    return res;
}

char numberToHex(uint8_t numb) {
    if (numb <= 9) {
        return '0' + numb;
    } else if (numb <= 15) {
        return 'a' + numb - 10;
    } else {
        throw std::runtime_error("Unable to convert uint8_t to correct hexadecimal character");
    }
}

void sec::vectorToHex(std::ostream& stream, const std::vector<uint8_t>& input) {
    stream << std::hex;
    for (auto val:input) {
        stream << numberToHex(val/16) << numberToHex(val%16);
    }
}