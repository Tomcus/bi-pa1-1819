#include <vector>
#include <cstdint>
#include <sstream>
#include "sec_lib.h"
#include <cassert>
#include <cstring>

void testHexLoading() {
    std::vector<uint8_t> out;
    std::istringstream input;
    // Positive loading
    input.str("fff0e0a50001");
    out = sec::hexToVector(input, 6);
    assert(out[0] == 255);
    assert(out[1] == 240);
    assert(out[2] == 224);
    assert(out[3] == 165);
    assert(out[4] == 0);
    assert(out[5] == 1);
    //Less loading
    input.str("fff0e0a50001");
    out = sec::hexToVector(input, 2);
    assert(out.size() == 2);
    //Over loading
    input.str("fff0");
    try {
        out = sec::hexToVector(input, 10);
        assert(false);
    } catch (std::runtime_error& err) {
        std::cout << err.what() << std::endl << "All OK" << std::endl;
    }
    //Wrong characters
    input.str("123zzz");
    try {
        out = sec::hexToVector(input, 3);
        assert(false);
    } catch (std::runtime_error& err) {
        std::cout << err.what() << std::endl << "All OK" << std::endl;
    }
}

void testHexDumping() {
    std::vector<uint8_t> test;
    std::ostringstream output;
    test = {0, 1, 2, 240, 255, 165};
    sec::vectorToHex(output, test);
    std::string str = output.str();
    assert(str[0] == '0');
    assert(str[1] == '0');
    assert(str[2] == '0');
    assert(str[3] == '1');
    assert(str[4] == '0');
    assert(str[5] == '2');
    assert(str[6] == 'f');
    assert(str[7] == '0');
    assert(str[8] == 'f');
    assert(str[9] == 'f');
    assert(str[10] == 'a');
    assert(str[11] == '5');
}

int main()
{
    testHexLoading();
    testHexDumping();
    return 0;
}
