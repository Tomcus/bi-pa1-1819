#ifndef __PROGTEST__

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <climits>
#include <cmath>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <algorithm>
#include <pthread.h>
#include <semaphore.h>
#include <cstdint>
#include <array>
#include <unordered_map>
#include <unordered_set>
#include <thread>
#include <mutex>
#include <memory>
#include <condition_variable>
#include <atomic>
#include <zconf.h>

using namespace std;


class CFITCoin;

class CCVUTCoin;

class CCustomer;

typedef struct shared_ptr<CFITCoin> AFITCoin;
typedef struct shared_ptr<CCVUTCoin> ACVUTCoin;
typedef struct shared_ptr<CCustomer> ACustomer;

//=================================================================================================
class CFITCoin {
public:
    CFITCoin(const vector<uint32_t> &vectors,
             int distMax)
            : m_Vectors(vectors),
              m_DistMax(distMax),
              m_Count(0) {
    }

    virtual                  ~CFITCoin(void) = default;

    vector<uint32_t> m_Vectors;
    int m_DistMax;
    uint64_t m_Count;
};

//=================================================================================================
class CCVUTCoin {
public:
    CCVUTCoin(const vector<uint8_t> &data,
              int distMin,
              int distMax)
            : m_Data(data),
              m_DistMin(distMin),
              m_DistMax(distMax),
              m_Count(0) {
    }

    virtual                  ~CCVUTCoin(void) = default;

    vector<uint8_t> m_Data;
    int m_DistMin;
    int m_DistMax;
    uint64_t m_Count;
};

//=================================================================================================
class CCustomer {
public:
    virtual                  ~CCustomer(void) = default;

    virtual AFITCoin FITCoinGen(void) = 0;

    virtual ACVUTCoin CVUTCoinGen(void) = 0;

    virtual void FITCoinAccept(AFITCoin x) = 0;

    virtual void CVUTCoinAccept(ACVUTCoin x) = 0;
};
//=================================================================================================
#endif /* __PROGTEST__ */

class BinaryArray;

class CProblem;

class CFitProblem;

class CCvutProblem;

class CCustomerObligation;

class CBuffer;

void debugLog(const string& message){
#ifndef __PROGTEST__
    cout << message << endl;
#endif
}

const size_t BUFFER_SIZE = 16;

class CBuffer {
public:
    void insert(CProblem *prob) {
        unique_lock<mutex> ul(mtx);
        cv_isFull.wait(ul, [this]() { return cnt < BUFFER_SIZE || !isNeeded; });
        if (isNeeded) {
            buffer.push_back(prob);
            cnt++;
            ul.unlock();
            cv_isEmpty.notify_all();
        }
    }

    CProblem *recieve() {
        unique_lock<mutex> ul(mtx);
        cv_isEmpty.wait(ul, [this]() { return cnt > 0 || !isNeeded; });
        if (cnt == 0){
            return nullptr;
        }
        CProblem *prob = buffer[0];
        buffer.pop_front();
        cnt--;
        ul.unlock();
        cv_isFull.notify_all();
        return prob;
    }

    bool empty(){
        lock_guard<mutex> lck(mutex);
        return (cnt <= 0);
    }

    void freeOtherThreads() {
        isNeeded = false;
        cv_isEmpty.notify_all();
        cv_isFull.notify_all();
    }

protected:
    deque<CProblem *> buffer;
    mutex mtx;
    condition_variable cv_isFull, cv_isEmpty;
    size_t cnt = 0;
    bool isNeeded = true;
};

class CRig {
public:
    static void Solve(ACVUTCoin x);

    static void Solve(AFITCoin x);

    CRig();

    ~CRig();

    void Start(int thrCnt);

    void Stop();

    void AddCustomer(ACustomer c);

    void insertProblem(CProblem *problem);

private:
    set<CCustomerObligation *> obligations;
    mutex oblMutex;
    CBuffer workingBuffer;
    bool isRunning;

    bool isLoading();

    void workOnProblems();

    vector<thread> workerThreads;

    static bool haveSameFirstBite(const vector<uint32_t> &numbers);

    static vector<uint32_t> deleteSameBites(vector<uint32_t> numbers, uint32_t *mask, uint32_t *numbWritten);

    static uint32_t countMaxDistance(const vector<uint32_t> &vectors, uint32_t number, uint32_t maxDistance);

    static uint32_t countDistance(uint32_t vector, uint32_t number);

    static uint32_t countOnes(uint32_t x);

    static void computeCombinations(AFITCoin x, uint32_t diff, uint32_t workedOn);

    static uint64_t combinationNumber(uint32_t top, uint32_t bottom);
};

class CCustomerObligation {
public:
    CCustomerObligation(CRig *rig, ACustomer c);

    ~CCustomerObligation();

    void insertFinishedProblem(CProblem *problem);

    void loadFitCoinProblems();

    void loadCvutCoinProblems();

    void unloadProblems();

    bool isLoading();

protected:
    //static CCustomerObligation* lastObligation = nullptr;
    //static mutex lOMutex;

    CRig *rig;
    ACustomer customer;
    thread *loadFitCoinThread;
    thread *loadCvutCoinThread;
    thread *unloadProblemThread;
    CBuffer unloadBuffer;
    atomic_int cnt;
    mutex customerMutex;
    mutex flagMutex;
    bool loadingFitCoin, loadingCvutCoin;
};

class CProblem {
public:
    CProblem(CCustomerObligation *obl, ACustomer customer);

    virtual void solve() = 0;

    virtual void finish() = 0;

protected:
    ACustomer customer;
    CCustomerObligation *obl;
};

class CFitProblem : public CProblem {
public:
    CFitProblem(AFITCoin coin, CCustomerObligation *obl, ACustomer customer);

    void solve() override;

    void finish() override;

protected:
    AFITCoin coin;
};

class CCvutProblem : public CProblem {
public:
    CCvutProblem(ACVUTCoin coin, CCustomerObligation *obl, ACustomer customer);

    void solve() override;

    void finish() override;

protected:
    ACVUTCoin coin;
};

class BinaryArray {
public:
    BinaryArray(const vector<uint8_t> &values, size_t from, size_t to);

    ~BinaryArray();

    size_t size();

    void set(size_t index, bool val);

    bool get(size_t index);

protected:
    uint8_t *values;
    size_t arrSize, realSize;
};

uint64_t min3(uint64_t a, uint64_t b, uint64_t c) {
    return min(min(a, b), c);
}

void CRig::Solve(ACVUTCoin x) {
    BinaryArray ba(x->m_Data, 0, x->m_Data.size() * 8);
    uint64_t **matrix = new uint64_t *[ba.size() + 1];
    //cout << ba.size() << endl;
    for (size_t i = 0; i < ba.size() + 1; ++i) {
        matrix[i] = new uint64_t[ba.size() + 1];
        if (i != 0) {
            matrix[i][0] = i;
            matrix[0][i] = i;
        }
    }
    matrix[0][0] = 0;
    uint64_t substitutionCost = 0;
    for (uint64_t suffixSize = 1; suffixSize <= ba.size(); ++suffixSize) {
        uint64_t suffixBeginning = ba.size() - suffixSize;
        for (uint64_t j = 1; j <= suffixSize; ++j) {
            for (uint64_t i = 1; i <= ba.size(); ++i) {
                if (ba.get(i - 1) == ba.get(suffixBeginning + j - 1))
                    substitutionCost = 0;
                else
                    substitutionCost = 1;
                matrix[i][j] = min3(matrix[i][j - 1] + 1,
                                    matrix[i - 1][j] + 1,
                                    matrix[i - 1][j - 1] + substitutionCost);
                if (j == suffixSize && matrix[i][j] <= (uint64_t) x->m_DistMax &&
                    matrix[i][j] >= (uint64_t) x->m_DistMin)
                    x->m_Count++;
            }
        }
    }
    // for (size_t i = 0; i < ba.size(); ++i){
    //     uint64_t suffixSize = 1+i;
    //     uint64_t suffixBeginning = ba.size() - suffixSize;
    //     for (size_t j = 0; j < ba.size(); ++j){
    //         uint64_t prefixSize = j+1;
    //         uint64_t substitutionCost = 0;
    //         //compute distance
    //         for (size_t k = 1; k <= prefixSize; ++k){
    //             for (size_t l = 1; l <= suffixSize; ++l){
    //                 if (ba.get(k-1) == ba.get(suffixBeginning + l - 1))
    //                     substitutionCost = 0;
    //                 else
    //                     substitutionCost = 1;
    //                 matrix[k][l] = min3(matrix[k][l-1] + 1,
    //                                     matrix[k-1][l] + 1,
    //                                     matrix[k-1][l-1] + substitutionCost);
    //             }
    //         }
    //         uint64_t dist = matrix[prefixSize][suffixSize];
    //         if (dist >= (uint64_t)c->m_DistMin && dist <= (uint64_t)c->m_DistMax)
    //             c->m_Count ++;
    //     }
    // }
    for (size_t i = 0; i < ba.size(); ++i) {
        delete[] matrix[i];
    }
    delete[] matrix;
}

CRig::CRig(): isRunning(false) { }

CRig::~CRig() = default;

void CRig::Start(int thrCnt) {
    isRunning = true;
    for (int i = 0; i < thrCnt; ++i) {
        workerThreads.emplace_back(thread(&CRig::workOnProblems, this));
    }
}

void CRig::Stop() {
    isRunning = false;
    debugLog("Waiting for worker threads");
    for (thread &thr: workerThreads)
        thr.join();
    debugLog("Waiting for obligation threads");
    for (CCustomerObligation *obl: obligations)
        delete obl;
    obligations.empty();
}

void CRig::AddCustomer(ACustomer c) {
    //cout << "Adding Customer" << endl;
    oblMutex.lock();
    obligations.insert(new CCustomerObligation(this, c));
    oblMutex.unlock();
}

bool CRig::haveSameFirstBite(const vector<uint32_t> &numbers) {
    if (numbers.size() <= 1)
        return true;
    uint32_t first = numbers[0] & 1u;
    for (size_t i = 1; i < numbers.size(); ++i) {
        if ((numbers[i] & 1u) != first)
            return false;
    }
    return true;
}

vector<uint32_t> CRig::deleteSameBites(vector<uint32_t> numbers, uint32_t *mask, uint32_t *numbWritten) {
    vector<uint32_t> res(numbers.size(), 0);
    *mask = 0;
    *numbWritten = 0;
    for (size_t j = 0; j < 32; ++j) {
        if (!haveSameFirstBite(numbers)) {
            for (size_t i = 0; i < res.size(); ++i) {
                res[i] += (numbers[i] & 1u) << *numbWritten;
            }
            *numbWritten += 1;
            *mask <<= 1;
            *mask += 1;
        }
        for (size_t i = 0; i < numbers.size(); ++i) {
            numbers[i] >>= 1;
        }
    }
    return res;
}

void CRig::Solve(AFITCoin x) {
    if (x->m_Vectors.size() == 0) {
        return;
    }
    if (x->m_Vectors.size() == 1) {
        computeCombinations(x, (uint32_t) x->m_DistMax, 32);
        return;
    }
    vector<uint32_t> editedVectors;
    uint32_t maxNumber = 0;
    uint32_t numbWritten = 0;
    editedVectors = deleteSameBites(x->m_Vectors, &maxNumber, &numbWritten);
    if (numbWritten == 0) {
        computeCombinations(x, (uint32_t) x->m_DistMax, 32);
    }
    for (size_t i = 0; i <= maxNumber; ++i) {
        uint32_t dist = countMaxDistance(editedVectors, (uint32_t) i, (uint32_t) x->m_DistMax);
        if (dist > (uint32_t) x->m_DistMax) {
            continue;
        }
        computeCombinations(x, x->m_DistMax - dist, 32 - numbWritten);
    }
}

uint32_t CRig::countOnes(uint32_t x) {
    uint32_t n = 0;
    while (x > 0) {
        if (x & 1u)
            n++;
        x >>= 1;
    }
    return n;
}

uint32_t CRig::countDistance(uint32_t vector, uint32_t number) {
    return countOnes(vector ^ number);
}

uint32_t CRig::countMaxDistance(const vector <uint32_t> &vectors, uint32_t number, uint32_t maxDistance) {
    uint32_t res;
    res = countDistance(vectors[0], number);
    if (res > maxDistance) {
        return res;
    }
    for (size_t i = 1; i < vectors.size(); ++i) {
        uint32_t numb = countDistance(vectors[i], number);
        if (numb > res)
            res = numb;
        if (res > maxDistance)
            return res;
    }
    return res;
}

uint64_t CRig::combinationNumber(uint32_t top, uint32_t bottom) {
    if (bottom > top) return 0;
    if (bottom * 2 > top) bottom = top - bottom;
    if (bottom == 0) return 1;

    uint64_t result = top;
    for (size_t i = 2; i <= bottom; ++i) {
        result *= (top - i + 1);
        result /= i;
    }
    return result;
}

void CRig::computeCombinations(AFITCoin x, uint32_t diff, uint32_t workedOn) {
    uint64_t cnt = 0;
    for (size_t i = 0; i <= diff; ++i) {
        cnt += combinationNumber(workedOn, (uint32_t)i);
    }
    x->m_Count += cnt;
}

void CRig::insertProblem(CProblem *problem) {
    workingBuffer.insert(problem);
}

bool CRig::isLoading() {
    oblMutex.lock();
    for (CCustomerObligation *obl:obligations) {
        if (obl->isLoading()) {
            oblMutex.unlock();
            return true;
        }
    }
    oblMutex.unlock();
    return false;
}

void CRig::workOnProblems() {
    CProblem *prob;
    //cout << "Starting working" << endl;
    while (isRunning || isLoading() || !workingBuffer.empty()) {
        //cout << "[" << (int) isRunning << ", " << (int) isLoading() << ", " << (int)!workingBuffer.empty() << "]" << endl;
        prob = workingBuffer.recieve();
        if (prob != nullptr) {
            prob->solve();
        } else break;
    }
    workingBuffer.freeOtherThreads();
    debugLog("Stopping work");
}

BinaryArray::~BinaryArray() {
    delete[] values;
}

size_t BinaryArray::size() {
    return realSize;
}

void BinaryArray::set(size_t index, bool val) {
    uint8_t byte = values[index / 8];
    uint8_t mask = (uint8_t)1 << (index % 8);
    if ((byte & mask) == 0 && val) {
        byte += mask;
    } else if ((byte & mask) != 0 && !val) {
        byte -= mask;
    }
    values[index / 8] = byte;
}

BinaryArray::BinaryArray(const vector<uint8_t> &values, size_t from, size_t to) {
    realSize = to - from;
    if (realSize % 8 == 0) {
        arrSize = realSize / 8;
    } else {
        arrSize = realSize / 8 + 1;
    }
    this->values = new uint8_t[arrSize];
    for (size_t i = 0; i < arrSize; ++i) {
        this->values[i] = 0;
    }
    size_t arrPos, bytePos;
    for (size_t i = from; i < to; ++i) {
        arrPos = i / 8;
        bytePos = i % 8;
        set(i - from, (bool) (values[arrPos] & (1u << bytePos)));
    }
}

bool BinaryArray::get(size_t index) {
    size_t arrPos, bytePos;
    arrPos = index / 8;
    bytePos = index % 8;
    return (bool) (values[arrPos] & (1u << bytePos));
}

CCustomerObligation::CCustomerObligation(CRig *rig, ACustomer c) : rig(rig), customer(c), cnt(0) {
    loadFitCoinThread = new thread(&CCustomerObligation::loadFitCoinProblems, this);
    loadCvutCoinThread = new thread(&CCustomerObligation::loadCvutCoinProblems, this);
    unloadProblemThread = new thread(&CCustomerObligation::unloadProblems, this);
    loadingCvutCoin = loadingFitCoin = true;
}

CCustomerObligation::~CCustomerObligation() {
    loadFitCoinThread->join();
    delete loadFitCoinThread;
    loadCvutCoinThread->join();
    delete loadCvutCoinThread;
    unloadProblemThread->join();
    delete unloadProblemThread;
}

void CCustomerObligation::insertFinishedProblem(CProblem *problem) {
    unloadBuffer.insert(problem);
}

void CCustomerObligation::loadFitCoinProblems() {
    AFITCoin coin;
    debugLog("Starting loading FIT Coin Problems");
    while (true) {
        coin = customer->FITCoinGen();
        if (coin == nullptr)
            break;
        cnt++;
        rig->insertProblem(new CFitProblem(coin, this, customer));
    }
    flagMutex.lock();
    loadingFitCoin = false;
    flagMutex.unlock();
    debugLog("Stopped Loading FIT Problems");
}

void CCustomerObligation::loadCvutCoinProblems() {
    ACVUTCoin coin;
    debugLog("Starting loading CVUT Coin Problems");
    while (true) {
        coin = customer->CVUTCoinGen();
        if (coin == nullptr)
            break;

        cnt++;
        rig->insertProblem(new CCvutProblem(coin, this, customer));
    }
    flagMutex.lock();
    loadingCvutCoin = false;
    flagMutex.unlock();
    debugLog("Stopped Loading CVUT Problems");
}

void CCustomerObligation::unloadProblems() {
    while (loadingFitCoin || loadingCvutCoin || cnt > 0) {
        //cout << "Unloading Problem" << endl;
        cnt--;
        CProblem *problem = unloadBuffer.recieve();
        if (problem != nullptr) {
            problem->finish();
        } else break;
    }
    //cout << "Ending unloading" << endl;
}

bool CCustomerObligation::isLoading() {
    lock_guard<mutex> lck(flagMutex);
    //cout << loadingFitCoin << " " << loadingCvutCoin << endl;
    return loadingCvutCoin || loadingFitCoin;
}

CProblem::CProblem(CCustomerObligation *obl, ACustomer customer) : customer(customer), obl(obl) {}

CFitProblem::CFitProblem(AFITCoin coin, CCustomerObligation *obl, ACustomer customer) : CProblem(obl, customer),
                                                                                        coin(coin) {}

void CFitProblem::solve() {
    CRig::Solve(coin);
    obl->insertFinishedProblem(this);
}

void CFitProblem::finish() {
    customer->FITCoinAccept(coin);
}

CCvutProblem::CCvutProblem(ACVUTCoin coin, CCustomerObligation *obl, ACustomer customer) : CProblem(obl, customer),
                                                                                           coin(coin) {}

void CCvutProblem::solve() {
    CRig::Solve(coin);
    obl->insertFinishedProblem(this);
}

void CCvutProblem::finish() {
    customer->CVUTCoinAccept(coin);
}

#ifndef __PROGTEST__

#include "test.cpp"

#endif /* __PROGTEST__ */
