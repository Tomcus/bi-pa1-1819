#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <climits>
#include <cfloat>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <vector>
#include <set>
#include <list>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <stack>
#include <deque>
#include <memory>
#include <functional>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include "progtest_solver.h"
#include "sample_tester.h"
using namespace std;
#endif /* __PROGTEST__ */ 

void debug(const string & str) {
#ifndef __PROGTEST__
	cout << str << endl;
#endif
}

class CCache {
public:
	CCache(size_t producersCount);
	void addPriceList(APriceList prices);
	APriceList getPrices();
	bool isLoaded();
protected:
	void addCost(unsigned w, unsigned h, double c);
	APriceList getPriceList();
	mutex data;
	size_t producersCount, loadedCount;
	APriceList prices;
	condition_variable cv;
	unordered_map<unsigned, unordered_map<unsigned, double>> costs;
};

CCache::CCache(size_t producersCount):producersCount(producersCount), loadedCount(0), prices(nullptr) { }

APriceList CCache::getPriceList() {
	return prices;
}

void CCache::addPriceList(APriceList prices) {
	debug("adding prices from another customer");
	lock_guard<mutex> dataGuard(data);
	for (auto price:prices->m_List) {
		debug("prices loaded");
		addCost(price.m_W, price.m_H, price.m_Cost);
	}
	loadedCount++;
	debug("loaded increased");
	if (loadedCount == producersCount) {
		debug("creating new price list");
		this->prices = make_shared<CPriceList>(prices->m_MaterialID);
		for (auto wVal:costs) {
			unsigned w = wVal.first;
			for (auto hcVal:wVal.second) {
				this->prices->Add(CProd(w, hcVal.first, hcVal.second));
			}
		}
		costs.clear();
		cv.notify_all();
	}
	debug("Price List loaded");
}

bool CCache::isLoaded() {
	lock_guard<mutex> guard(data);
	return loadedCount == producersCount;
}

APriceList CCache::getPrices() {
	unique_lock<mutex> guard(data);
	cv.wait(guard, [this](){ return loadedCount == producersCount;});
	debug("recieved price");
	return getPriceList();
}

void CCache::addCost(unsigned w, unsigned h, double c) {
	auto secondMap = costs.find(min(w, h));
	if (secondMap == costs.end()){
		// NOT FOUND => CREATE NEW ITEM
		costs[min(w, h)][max(w, h)] = c;
		return;
	}
	auto value = secondMap->second.find(max(w, h));
	if (value == secondMap->second.end()) {
		// NOT FOUND => CREATE NEW ITEM
		costs[min(w, h)][max(w, h)] = c;
	} else {
		// FOUND => CHANGE IF LESS THEN PREVIOUS VALUE
		costs[min(w, h)][max(w, h)] = min(c, value->second);
	}
}

struct TOrderListJob{
	AOrderList orderList;
	ACustomer customer;
};

class CBuffer {
public:
	CBuffer(size_t size) {
		buffSize = size;
	}
    void insert(TOrderListJob *prob) {
        unique_lock<mutex> ul(mtx);
        cv_isFull.wait(ul, [this]() { return cnt < buffSize || !isNeeded; });
        if (isNeeded) {
            buffer.push_back(prob);
            cnt++;
            ul.unlock();
            cv_isEmpty.notify_all();
        }
    }

    TOrderListJob *recieve() {
        unique_lock<mutex> ul(mtx);
        cv_isEmpty.wait(ul, [this]() { return cnt > 0 || !isNeeded; });
        if (cnt == 0){
            return nullptr;
        }
        TOrderListJob *prob = buffer[0];
        buffer.pop_front();
        cnt--;
        ul.unlock();
        cv_isFull.notify_all();
        return prob;
    }

    bool empty(){
        lock_guard<mutex> lck(mutex);
        return (cnt <= 0);
    }

    void freeOtherThreads() {
        isNeeded = false;
        cv_isEmpty.notify_all();
        cv_isFull.notify_all();
    }

protected:
	size_t buffSize;
    deque<TOrderListJob *> buffer;
    mutex mtx;
    condition_variable cv_isFull, cv_isEmpty;
    size_t cnt = 0;
    bool isNeeded = true;
};

class CWeldingCompany {
public:
	CWeldingCompany();
	~CWeldingCompany();
    static void SeqSolve(APriceList priceList, COrder& order);
    void AddProducer(AProducer prod);
    void AddCustomer(ACustomer cust);
    void AddPriceList(AProducer prod, APriceList priceList);
    void Start(unsigned thrCount);
    void Stop(void);
	void loadForCustomer(ACustomer customer);
	void work();
protected:
	APriceList getPriceListForMaterial(unsigned materialID);
	CCache* getCache(unsigned materialID);
    vector<AProducer> producers;
    vector<thread> computationThreads;
    vector<thread> loadingThreads;
	unordered_map<unsigned, CCache*> materialCache;
	mutex cacheMutex;
	CBuffer buffer;
};

// void startLoading(CWeldingCompany& wc, ACustomer customer) {
// 	wc.loadForCustomer(customer);
// }

// void startWotking(CWeldingCompany& wc) {
// 	wc.work();
// }

CWeldingCompany::~CWeldingCompany() {
	for (auto pair:materialCache) {
		delete pair.second;
	}
}

CWeldingCompany::CWeldingCompany():buffer(16) { }

void CWeldingCompany::work() {
	debug("Started worker thread");
	while (true) {
		TOrderListJob* job = buffer.recieve();
		if (job->customer == nullptr) {
			debug("Recieved terminal job");
			delete job;
			break;
		}
		debug("recieved normal job");
		APriceList pl = getPriceListForMaterial(job->orderList->m_MaterialID);
		ProgtestSolver(job->orderList->m_List, pl);
		job->customer->Completed(job->orderList);
		delete job;
	}
}

void CWeldingCompany::loadForCustomer(ACustomer customer) {
	while (true) {
		TOrderListJob* job = new TOrderListJob();
		job->customer = customer;
		job->orderList = customer->WaitForDemand();
		if (job->orderList == nullptr) {
			delete job;
			break;
		}
		debug("Job loaded");
		buffer.insert(job);
	}
	debug("Job finished");
}

void CWeldingCompany::Start(unsigned thrCount) {
	debug("Starting working threads");
	for (size_t i = 0; i < thrCount; ++i) {
		computationThreads.emplace_back(&CWeldingCompany::work, this);
	}
}

void CWeldingCompany::Stop() {
	for (auto& thr:loadingThreads) {
		thr.join();
	}
	debug("Stopped all loading threads");
	for (size_t i = 0; i < computationThreads.size(); ++i) {
		TOrderListJob* job = new TOrderListJob();
		job->customer = nullptr;
		buffer.insert(job);
	}
	debug("Added all stopping jobs");
	for (auto& thr:computationThreads) {
		thr.join();
	}
	debug("Stopped all working threads");
}

void CWeldingCompany::AddCustomer(ACustomer customer) {
	loadingThreads.emplace_back(&CWeldingCompany::loadForCustomer, this, customer);
}

CCache* CWeldingCompany::getCache(unsigned materialID) {
	cacheMutex.lock();
	CCache* res;
	auto cache = materialCache.find(materialID);
	if (cache == materialCache.end()) {
		debug("material ID not found");
		materialCache[materialID] = res = new CCache(producers.size());
		for (AProducer prod:producers){
			cacheMutex.unlock();
			prod->SendPriceList(materialID);
			cacheMutex.lock();
		}
	} else {
		debug("material ID found");
		res = cache->second;
	}
	cacheMutex.unlock();
	return res;
}

APriceList CWeldingCompany::getPriceListForMaterial(unsigned materialID) {
	CCache * c = getCache(materialID);
	debug("cached value recieved");
	return c->getPrices();
}

void CWeldingCompany::AddPriceList(AProducer prod, APriceList priceList) {
	debug("Adding price list");
	if (prod != nullptr) {
		CCache* c = getCache(priceList->m_MaterialID);
		c->addPriceList(priceList);
	}
}

void CWeldingCompany::SeqSolve(APriceList priceList, COrder& order) {
	vector<COrder> orders;
	orders.emplace_back(order.m_W, order.m_H, order.m_WeldingStrength);
	ProgtestSolver(orders, priceList);
	order.m_Cost = orders[0].m_Cost;
}

void CWeldingCompany::AddProducer(AProducer prod) {
	producers.push_back(prod);
}

//-------------------------------------------------------------------------------------------------
#ifndef __PROGTEST__
int main(void) {
	using namespace std::placeholders;
	CWeldingCompany  test;
	debug("CWeldingCompany initialized");
  
	AProducer p1 = make_shared<CProducerSync> ( bind ( &CWeldingCompany::AddPriceList, &test, _1, _2 ) );
	AProducerAsync p2 = make_shared<CProducerAsync> ( bind ( &CWeldingCompany::AddPriceList, &test, _1, _2 ) );
	test . AddProducer ( p1 );
	test . AddProducer ( p2 );
	test . AddCustomer ( make_shared<CCustomerTest> ( 2 ) );
	p2 -> Start ();
	test . Start ( 3 );
	test . Stop ();
	p2 -> Stop ();
  return 0;  
}
#endif /* __PROGTEST__ */ 
