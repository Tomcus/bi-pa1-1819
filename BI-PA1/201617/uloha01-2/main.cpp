#include <iostream>
#include <stdlib.h>
#include <list>

using namespace std;

const double epsilon = 1e-12;

double abs(double value){
    if (value < 0)
        return -1 * value;
    return value;
}

bool doubleEquals(double a, double b){
    return abs(a-b)<(epsilon*((a>b)?a:b));
}

/*int doubleCompare(double a, double b){
    if (doubleEquals(a,b))
        return 0;
    if (a-b > 0)
        return 1;
    return -1;
}*/

void errExit(const char* err){
    cout << err << endl;
    exit(0);
}

double ceil(double value){
    int tmp = (int) value;
    double res = (double) tmp;
    //cout << value << "==" << res << endl;
    if (doubleEquals(res,value)){
        return res;
    }
    return res + 1;
}

struct TShape {
    TShape() {}
    TShape(const TShape& a){
        width = a.width;
        height = a.height;
    }
    void operator=(const TShape& a){
        width = a.width;
        height = a.height;
    }
    TShape rotate() {
        TShape newTShape;
        newTShape.width = height;
        newTShape.height = width;
        return newTShape;
    }
    friend istream& operator>> (istream& input, TShape & a){
        double w, h;
        if (!(input >> w) || w <= 0)
            errExit("Nespravny vstup.");
        if (!(input >> h) || h <= 0)
            errExit("Nespravny vstup.");
        a.width = w;
        a.height = h;
        return input;
    }
    double width, height;
};

bool isOverlayNeeded(TShape & cloth, TShape & sail){
    return ((cloth.width < sail.width || cloth.height < sail.height) &&
            (cloth.width < sail.height || cloth.height < sail.width));
}

double countDimension(double clothParametr, double sailParametr, double overlay){
    if (clothParametr >= sailParametr)
        return 1;
    return (sailParametr-overlay)/(clothParametr-overlay);
}

double countSize(TShape & cloth, TShape& sail, double overlay){
    double widthCount, heightCount;
    widthCount = countDimension(cloth.width, sail.width, overlay);
    heightCount = countDimension(cloth.height, sail.height, overlay);
    //cout << ceil(widthCount) << " * " << ceil(heightCount) << "(" << widthCount << " * " << heightCount << ")" << endl;
    return (ceil(widthCount) * ceil(heightCount));
}

int selectMinimalValue(list<double>& list){
    double min = list.front();
    for (double i : list){
        if (i < min){
            min = i;
        }
    }
    return (int)min;
}

void calculateMinimalSize(TShape& cloth, TShape& sail, double overlay){
    list<double> results;

    if (cloth.width <= overlay && cloth.height <= overlay)
        errExit("Nelze vyrobit.");
    double tmp = countSize(cloth, sail, overlay);
    if (tmp > 0)
        results.push_back(tmp);
    TShape rotatedCloth = cloth.rotate();
    tmp = countSize(rotatedCloth, sail, overlay);
    if (tmp > 0)
        results.push_back(tmp);

    if (results.size() == 0)
        errExit("Nelze vyrobit.");


    int res = selectMinimalValue(results);
    cout << "Pocet kusu latky: " << res << endl;
}

int main() {
    TShape cloth, sail;
    double overlay = 0;
    cout << "Velikost latky:" << endl;
    cin >> cloth;
    cout << "Velikost plachty:" << endl;
    cin >> sail;
    if (isOverlayNeeded(cloth, sail)){
        cout << "Prekryv:" << endl;
        if (!(cin >> overlay) || overlay < 0)
            errExit("Nespravny vstup.");
    }
    calculateMinimalSize(cloth, sail, overlay);
    return 0;
}