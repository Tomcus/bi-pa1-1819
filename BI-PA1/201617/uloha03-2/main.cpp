#ifndef __PROGTEST__

#include <stdio.h>
#include <assert.h>

#endif /* __PROGTEST__ */

/* POZNAMKY
 * 1.1.2000 - Sobota
 * 0 - nedele
 * 1 - pondeli
 * 2 - utery
 * 3 - streda
 * 4 - ctvrtek
 * 5 - patek
 * 6 - sobota
 * */

const int STARTING_YEAR = 2000;

int isLeap(int y) {
    return (y % 4 == 0 && (y % 100 != 0 || y % 400 == 0) && y % 4000 != 0);
}

int isValidDate(int y, int m, int d) {
    if (y < STARTING_YEAR || d < 1 || m < 1 || m > 12) {
        return 0;
    }
    if ((m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12) && d > 31) {
        return 0;
    } else if ((m == 4 || m == 6 || m == 9 || m == 11) && d > 30) {
        return 0;
    } else if (m == 2) {
        if (isLeap(y)) {
            if (d > 29)
                return 0;
        } else {
            if (d > 28)
                return 0;
        }
    }
    return 1;
}

const int DAY_1_1_2000 = 6;

int intFromDate(int y, int m, int d) {
    int res = DAY_1_1_2000;
    for (int i = STARTING_YEAR; i < y; ++i) {
        if (isLeap(i))
            res += 366;
        else
            res += 365;
    }
    for (int i = 1; i < m; ++i){
        switch (i){
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                res += 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                res += 30;
                break;
            case 2:
                if (isLeap(y)){
                    res += 29;
                } else {
                    res += 28;
                }
                break;
            default:
                break;
        }
    }
    res += d-1;
    return res;
}
// 1.1,  1.5,  8.5,  5.7,  6.7, 28.9,  28.10,  17.11,  24.12, 25.12 a 26.12
int isNationalHoliday(int m, int d){
    switch (m){
        case 1:
            if (d == 1)
                return 1;
            break;
        case 5:
            if (d == 1 || d == 8)
                return 1;
            break;
        case 7:
            if (d == 5 || d == 6)
                return 1;
            break;
        case 9:
            if (d == 28)
                return 1;
            break;
        case 10:
            if (d == 28)
                return 1;
            break;
        case 11:
            if (d == 17)
                return 1;
            break;
        case 12:
            if (d >= 24 && d <=26)
                return 1;
            break;
        default:
            return 0;
    }
    return 0;
}

int isWorkDay(int m, int d, int dim){
    if (dim == 0 || dim == 6 || isNationalHoliday(m, d))
        return 0;
    return 1;
}

int IsWorkDay(int y, int m, int d) {
    int dim = intFromDate(y, m, d);
    if (!isValidDate(y, m, d))
        return 0;
    return isWorkDay(m, d, dim%7);
}

int endOfMonth(int y, int m){
    if (m == 1 || m==3 || m==5 || m==7|| m==8 || m==10 || m==12)
        return 31;
    else if (m == 2){
        if (isLeap(y))
            return 29;
        else return 28;
    }
    return 30;
}

void updateDate(int* y1, int* m1, int* d1){
    if (*d1 > endOfMonth(*y1, *m1)){
        *d1 = 1;
        *m1 += 1;
    }
    if (*m1 > 12){
        *m1 = 1;
        *y1 += 1;
    }
}

int CountWorkDays(int y1, int m1, int d1, int y2, int m2, int d2, int *cnt) {
    if (!isValidDate(y1, m1, d1) || !isValidDate(y2, m2, d2))
        return 0;
    int res = 0;
    int dt1 = intFromDate(y1, m1, d1);
    int dt2 = intFromDate(y2, m2, d2);
    if (dt1 > dt2)
        return 0;
    while (dt1 != dt2){
        if (isWorkDay(m1, d1, dt1%7)){
            res++;
        }
        dt1++;
        d1++;
        updateDate(&y1, &m1, &d1);
    }
    if (isWorkDay(m1, d1, dt1%7)){
        res++;
    }
    *cnt = res;
    return 1;
}

#ifndef __PROGTEST__

int main(void) {
    int cnt;
    /*for (int i = 2000; i <= 2024; ++i){
        CountWorkDays(i, 1, 1, i, 12, 31, &cnt);
        printf("%d(%d): %d => %d\n", i, isLeap(i), dayFromDate(i,1,1), cnt);
    }
    CountWorkDays(2000, 1, 1, 2200, 12, 31, &cnt);
    printf("%d", cnt);*/
    //printf("%d\n", intFromDate(2001, 1, 1));
    //printf("%d\n", intFromDate(2001, 1, 1));
    assert ( IsWorkDay ( 2016, 11, 11 ) );
    assert ( ! IsWorkDay ( 2016, 11, 12 ) );
    assert ( ! IsWorkDay ( 2016, 11, 17 ) );
    assert ( ! IsWorkDay ( 2016, 11, 31 ) );
    assert ( IsWorkDay ( 2016,  2, 29 ) );
    assert ( ! IsWorkDay ( 2004,  2, 29 ) );
    assert ( ! IsWorkDay ( 2001,  2, 29 ) );
    assert ( ! IsWorkDay ( 1996,  1,  1 ) );
    //printf ("%d %d\n", CountWorkDays(2016, 11, 1, 2016, 11, 30, &cnt), cnt);
    assert ( CountWorkDays ( 2016, 11,  1,
                             2016, 11, 30, &cnt ) == 1
             && cnt == 21 );
    assert ( CountWorkDays ( 2016, 11,  1,
                             2016, 11, 17, &cnt ) == 1
             && cnt == 12 );
    assert ( CountWorkDays ( 2016, 11,  1,
                             2016, 11,  1, &cnt ) == 1
             && cnt == 1 );
    assert ( CountWorkDays ( 2016, 11, 17,
                             2016, 11, 17, &cnt ) == 1
             && cnt == 0 );
    assert ( CountWorkDays ( 2016,  1,  1,
                             2016, 12, 31, &cnt ) == 1
             && cnt == 254 );
    assert ( CountWorkDays ( 2015,  1,  1,
                             2015, 12, 31, &cnt ) == 1
             && cnt == 252 );
    assert ( CountWorkDays ( 2000,  1,  1,
                             2016, 12, 31, &cnt ) == 1
             && cnt == 4302 );
    /*CountWorkDays ( 2001,  2,  3, 2016,  7, 18, &cnt );
    printf("%d\n", cnt);*/
    assert ( CountWorkDays ( 2001,  2,  3,
                             2016,  7, 18, &cnt ) == 1
             && cnt == 3911 );
    assert ( CountWorkDays ( 2014,  3, 27,
                             2016, 11, 12, &cnt ) == 1
             && cnt == 666 );
    assert ( CountWorkDays ( 2001,  1,  1,
                             2000,  1,  1, &cnt ) == 0 );
    assert ( CountWorkDays ( 2001,  1,  1,
                             2015,  2, 29, &cnt ) == 0 );
    assert ( ! IsWorkDay ( 2100, 12, -5 ) );
    assert ( CountWorkDays ( 2009, 8, 22, 2009, 8, 22, &cnt ) == 1 && cnt == 0 );
    assert( CountWorkDays ( 2000, 1, 1, 2100, 12, 0, &cnt ) == 0);

    assert (   IsWorkDay ( 2016,  11,  11 ) );
    assert (   IsWorkDay ( 2008,   1,   2 ) );
    assert (   IsWorkDay ( 2016,   2,  29 ) );
    assert ( ! IsWorkDay ( 2016,  11,  12 ) );
    assert ( ! IsWorkDay ( 2016,  11,  17 ) );
    assert ( ! IsWorkDay ( 2016,  11,  31 ) );
    assert ( ! IsWorkDay ( 2004,   2,  29 ) );
    assert ( ! IsWorkDay ( 2001,   2,  29 ) );
    assert ( ! IsWorkDay ( 1996,   1,   1 ) );
    assert ( ! IsWorkDay ( 2018, 123, 124 ) );
    assert ( CountWorkDays ( 2000, -10, -10,    2200, 10,  1, &cnt ) == 0 );
    assert ( CountWorkDays ( 1999,  12,  31,    2000, 12, 31, &cnt ) == 0 );
    assert ( CountWorkDays ( 2001,   1,   1,    2000,  1,  1, &cnt ) == 0 );
    assert ( CountWorkDays ( 2001,   1,   1,    2015,  2, 29, &cnt ) == 0 );
    assert ( CountWorkDays ( 2000,  12,   2,    2000, 12,  3, &cnt ) == 1 && cnt ==          0 );
    assert ( CountWorkDays ( 2000,  12,   3,    2000, 12,  3, &cnt ) == 1 && cnt ==          0 );
    assert ( CountWorkDays ( 2016,  11,  17,    2016, 11, 17, &cnt ) == 1 && cnt ==          0 );
    assert ( CountWorkDays ( 2000,  12,   1,    2000, 12,  3, &cnt ) == 1 && cnt ==          1 );
    assert ( CountWorkDays ( 2016,  11,   1,    2016, 11,  1, &cnt ) == 1 && cnt ==          1 );
    assert ( CountWorkDays ( 2000,  11,  30,    2000, 12,  3, &cnt ) == 1 && cnt ==          2 );
    assert ( CountWorkDays ( 2000,  11,  29,    2000, 12,  3, &cnt ) == 1 && cnt ==          3 );
    assert ( CountWorkDays ( 2000,  11,  28,    2000, 12,  3, &cnt ) == 1 && cnt ==          4 );
    assert ( CountWorkDays ( 2000,  11,  27,    2000, 12,  3, &cnt ) == 1 && cnt ==          5 );
    assert ( CountWorkDays ( 2004,  12,  26,    2004, 12, 31, &cnt ) == 1 && cnt ==          5 );
    assert ( CountWorkDays ( 2004,  12,  25,    2004, 12, 31, &cnt ) == 1 && cnt ==          5 );
    assert ( CountWorkDays ( 2016,  11,   1,    2016, 11, 17, &cnt ) == 1 && cnt ==         12 );
    assert ( CountWorkDays ( 2016,  11,   1,    2016, 11, 30, &cnt ) == 1 && cnt ==         21 );
    assert ( CountWorkDays ( 2008,   9,  30,    2008, 11, 11, &cnt ) == 1 && cnt ==         30 );
    assert ( CountWorkDays ( 2000,   5,   8,    2000, 12, 31, &cnt ) == 1 && cnt ==        163 );
    assert ( CountWorkDays ( 2015,   1,   1,    2015, 12, 31, &cnt ) == 1 && cnt ==        252 );
    assert ( CountWorkDays ( 2016,   1,   1,    2016, 12, 31, &cnt ) == 1 && cnt ==        254 );
    assert ( CountWorkDays ( 2014,   3,  27,    2016, 11, 12, &cnt ) == 1 && cnt ==        666 );
    assert ( CountWorkDays ( 2001,   2,   3,    2016,  7, 18, &cnt ) == 1 && cnt ==       3911 );
    assert ( CountWorkDays ( 2000,   1,   1,    2016, 12, 31, &cnt ) == 1 && cnt ==       4302 );

    assert(CountWorkDays(2200, 6, 21, 2568, 9, 20, &cnt) == 1 && cnt == 93180);
    assert ( CountWorkDays ( 2279, 10, 14, 3256, 10, 5, &cnt ) == 1 && cnt == 247202);
    assert(CountWorkDays ( 2000, 5, 8, 2000, 12, 31, &cnt ) == 1 && cnt == 163);
    return 0;
}

#endif /* __PROGTEST__ */
