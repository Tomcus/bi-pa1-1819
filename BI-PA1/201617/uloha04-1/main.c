#include <stdio.h>
#include <stdlib.h>

void messageToStdOut(const char* text){
	printf("%s\n", text);
}

void errExit(const char* errMessage){
	messageToStdOut(errMessage);
	exit(0);
}

typedef struct{
	int* arr;
	int size;
} TArray;

#define ARR_SIZE 2000

void freeArray(TArray* arr){
	free(arr->arr);
}

int compare(const void* a, const void* b){
	int* aa, *bb;
	aa = (int *)a;
	bb = (int *)b;
	return *aa - *bb;
}

void insertIntoArray(TArray* arr, int toAdd){
	int i;
	for (i = 0; i < arr->size; ++i){
		if(arr->arr[i] == toAdd){
			freeArray(arr);
			errExit("Nespravny vstup.");
		}
	}
	arr->arr[arr->size] = toAdd;
	arr->size++;
}

void fillArray(TArray* arr){
	int toAdd=-15;
	arr->arr = (int *)malloc(ARR_SIZE * sizeof(int));
	arr->size = 0;
	while (scanf("%d",&toAdd) == 1 && toAdd > 0){
		if (arr->size == ARR_SIZE){
			freeArray(arr);
			errExit("Nespravny vstup.");
		}
		insertIntoArray(arr, toAdd);
	}
	if (toAdd != 0 || arr->size == 0){
		freeArray(arr); 
		errExit("Nespravny vstup.");
	}
	qsort(arr->arr, arr->size, sizeof(int), compare);
}

void findCombinations(TArray * arr, int searchedValue){
	int i, j, k, sum1, sum2, count;
	count = 0;
	for (i = 0; i < arr->size; ++i){
		if (arr->arr[i] > searchedValue)
			break;
		if (arr->arr[i] == searchedValue){
			printf("%d = %d\n", searchedValue, arr->arr[i]);
			count++;
			break;
		}
		for (j = 0; j < arr->size; ++j){
			sum1 = arr->arr[i] + arr->arr[j];
			if (sum1 > searchedValue || arr->arr[j] > arr->arr[i])
				break;
			if (sum1 == searchedValue){
				printf("%d = %d + %d\n", searchedValue, arr->arr[i], arr->arr[j]);
				count++;
				break;
			}
			for (k = 0; k < arr->size; ++k){
				sum2 = sum1 + arr->arr[k];
				if (sum2 > searchedValue|| arr->arr[k] > arr->arr[j] )
					break;
				if (sum2 == searchedValue){
					printf("%d = %d + %d + %d\n", searchedValue, arr->arr[i], arr->arr[j], arr->arr[k]);
					count++;
					break;
				}
			}
		}
	}
	printf("Celkem: %d\n", count);
}

int main (void){
	TArray arr;
	int length;
	messageToStdOut("Delky trubek:");
	fillArray(&arr);
	messageToStdOut("Potrubi:");
	while(scanf("%d", &length) == 1){
		if (length <= 0){
			freeArray(&arr);
			errExit("Nespravny vstup.");
		}
		findCombinations(&arr, length);
	}
	freeArray(&arr);
	if (!feof(stdin))
		errExit("Nespravny vstup.");
	return 0;
}