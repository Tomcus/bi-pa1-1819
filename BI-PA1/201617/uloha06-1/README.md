Úkolem je vytvořit program, který bude řadit text na svém vstupu.

Vstupem programu jsou řetězce. Každý řetězec je zadaný na samostatné řádce. Řetězec je tvořen znaky abecedy a-z, A-Z a mezerami (v bonusovém testu též diakritickými znaky, viz níže). Program tyto řetězce načte, seřadí je a vypíše ve vzestupném pořadí. Při řazení nebudou rozlišovaná malá a velká písmena. Navíc program musí zohlednit správné řazení dvojhlásky ch ( ... c, d, e, f, g, h, ch, i, j, ...).

Výstupem programu jsou abecedně seřazené vstupní řetězce. Každý řetězec je uveden na jedné výstupní řádce.

Pokud je vstup neplatný, program to musí detekovat a zobrazit chybové hlášení. Chybové hlášení zobrazujte na standardní výstup (ne na chybový výstup). Po detekování chyby se program ukončí. Za chybu považujte:

* na vstupu je řetězec obsahující neplatný znak (mimo a-z, A-Z, mezery a diakritických znaků),
* vstupní znak porušuje pravidla kódování UTF-8 (bonusové testy).

V základní variantě stačí, aby program pracoval správně se znaky bez diakritiky. Tedy základní varianta programu může odmítat české diakritické znaky. I základní varianta programu ale musí správně řadit ch. Takové řešení projde všemi testy kromě testu bonusového. Pokud chcete získat body i z bonusové části, musíte správně načítat a řadit české diakritické znaky.