#include <stdio.h>
#include <ctype.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
	char* c_str;
	int size;
	int realSize;
}TString;

void initString(TString * str, int defLength){
	str->c_str = (char *)malloc(defLength+1);
	str->size = 0;
	str->c_str[0] = 0;
	str->realSize = defLength;
}

void addCharToEndOfString(TString* str, char toAdd){
	if (str->size == str->realSize){
		str->c_str = (char *)realloc(str->c_str, (str->realSize * 2) + 1);
		str->realSize *= 2;
	}
	str->c_str[str->size] = toAdd;
	str->c_str[str->size + 1] = 0;
	str->size++;
}

int loadStringTextUntil(TString* str, char stopChar){
	char toLoad;
	while (scanf("%c", &toLoad) == 1 && toLoad != stopChar){
		if ((toLoad < 'a' || toLoad > 'z') && (toLoad < 'A' || toLoad > 'Z') && toLoad != ' ')
			return 0;
		addCharToEndOfString(str, toLoad);
	}
	return 1;
}

void freeString(TString* str){
	if (str->c_str != NULL)
		free(str->c_str);
}

void printTString(TString * str){
	printf("%s\n", str->c_str);
}

typedef struct{
	TString * arr;
	int size;
	int realSize;
} TArray;

void addStringToArray(TArray * arr, TString * str){
	if (arr->size == arr->realSize){
		arr->arr = (TString *)realloc(arr->arr, arr->realSize*2*sizeof(TString));
		arr->realSize *= 2;
	}
	arr->arr[arr->size] = *str;
	arr->size++;
}

void freeTArray(TArray* arr){
	int i;
	for (i = 0; i < arr->size; ++i){
		freeString(&arr->arr[i]);
	}
	free(arr->arr);
}

void initTArray(TArray* arr){
	arr->arr = (TString *)malloc(sizeof(TString) * 100);
	arr->size = 0;
	arr->realSize = 100;
}

char* toLowerCase(TString * str){
	char* res = (char *)malloc(str->size + 1);
	for (int i = 0; i <= str->size; ++i){
		res [i] = tolower(str->c_str[i]);
	}
	return res;
}

int compare(const void* p1, const void* p2){
	int res;
	TString * a = (TString *)p1;
	TString * b = (TString *)p2;
	char * al = toLowerCase(a);
	char * bl = toLowerCase(b);
	res = strcoll(al, bl);
	free(al);
	free(bl);
	return res;
}

void sortTArray(TArray* arr){
	qsort(arr->arr, arr->size, sizeof(TString), compare);
}

void printArray(TArray* arr){
	for (int i = 0; i < arr->size; ++i){
		printTString(&arr->arr[i]);
	}
}

int loadArray(TArray * arr){
	TString toLoad;
	while (1){
		initString(&toLoad, 50);
		if (!loadStringTextUntil(&toLoad, '\n') && !feof(stdin)){
			freeString(&toLoad);
			return 0;
		}
		if (feof (stdin)){
			freeString(&toLoad);
			return 1;
		}
		addStringToArray(arr, &toLoad);
	}
}

int main(void)
{
	setlocale(LC_ALL, "cs_CZ.UTF-8");
	TArray arr;
	initTArray(&arr);
	if (!loadArray(&arr)){
		freeTArray(&arr);
		printf("Nespravny vstup.\n");
		return 0;
	}
	sortTArray(&arr);
	printArray(&arr);
	freeTArray(&arr);
	return 0;
}