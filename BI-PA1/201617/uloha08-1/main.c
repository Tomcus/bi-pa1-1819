#ifndef __PROGTEST__
#include <stdio.h>
#include <stdlib.h>

typedef struct TSoldier
 {
   struct TSoldier   * m_Next;
   int                 m_PersonalID;
   char                m_SecretRecord[64];
 } TSOLDIER;

#endif /* __PROGTEST__ */

TSOLDIER* popTop( TSOLDIER ** src ){
	if (*src == NULL)
		return NULL;
	TSOLDIER * res = *src;
	*src = (*src)->m_Next;
	res->m_Next = NULL;
	return res;
}

TSOLDIER* mergePlatoons( TSOLDIER * p1, TSOLDIER * p2 ){
	TSOLDIER *start, *curr;
	if (p1 == NULL && p2 == NULL)
		return NULL;
	if (p1 == NULL)
		return p2;
	if (p2 == NULL)
		return p1;
	start = NULL;
	curr = NULL;
	while(p1 != NULL && p2!=NULL){
		TSOLDIER *a, *b;
		a = popTop(&p1);
		b = popTop(&p2);
		if (a != NULL)
			a->m_Next = b;
		if (curr != NULL){
			curr->m_Next = a;
			if (b != NULL)
				curr = b;
			else 
				curr = a;
		} else {
			start = a;
			if (b != NULL)
				curr = b;
			else 
				curr = a;
		}
	}
	if (p1 != NULL)
		curr->m_Next = p1;
	else
		curr->m_Next = p2;
	return start;
}

int listSize(TSOLDIER * ptr){
	int size = 0;
	while (ptr != NULL){
		size++;
		ptr = ptr->m_Next;
	}
	return size;
}
TSOLDIER* shiftList(TSOLDIER * list, int index){
	if (list == NULL)
		return NULL;
	for (int i = 0; i < index; ++i)
	{
		list = list->m_Next;
	}
	return list;
}
 
void splitPlatoon ( TSOLDIER* src, TSOLDIER ** p1, TSOLDIER ** p2 ){
	if (p1 == NULL || p2 == NULL)
		return;
	if (src == NULL){
		*p1 = NULL;
		*p2 = NULL;
		return;
	}
	int size = listSize(src);
	if (size == 1){
		*p1 = NULL;
		*p2 = NULL;
		free(src);
		return;
	}
	*p1 = src;
	src = shiftList(src, size/2 - 1);
	*p2 = src->m_Next;
	src->m_Next = NULL;
	src = *p2;
	src = shiftList(src, size/2 - 1);
	if (src->m_Next != NULL)
		free(src->m_Next);
	src->m_Next = NULL;
}
 
void destroyPlatoon ( TSOLDIER * src ){
	TSOLDIER * curr, * next;
	if (src == NULL)
		return;
	next = src;
	while (next != NULL){
		curr = next;
		next = curr->m_Next;
		free(curr);
	}
}
  
#ifndef __PROGTEST__

TSOLDIER* initSoldier(int ID){
	TSOLDIER* res = (TSOLDIER *)malloc(sizeof(TSOLDIER));
	res->m_Next = NULL;
	res->m_PersonalID = ID;
	return res;
}

void printPlatoon(TSOLDIER* src){
	while(src->m_Next != NULL){
		printf("%d;", src->m_PersonalID);
		src = src->m_Next;
	}
	printf("%d\n", src->m_PersonalID);
}

int main ( void ){
	TSOLDIER *pa, *pb, *un;
	pa = initSoldier(5);
	pa->m_Next = initSoldier(7);
	pa->m_Next->m_Next = initSoldier(1);
	pa->m_Next->m_Next->m_Next = initSoldier(20);
	pb = initSoldier(66);
	pb->m_Next = initSoldier(15);
	pb->m_Next->m_Next = initSoldier(0);
	printPlatoon(pa);
	printPlatoon(pb);
	un = mergePlatoons(pa, pb);
	printPlatoon(un);
	splitPlatoon(un, &pa, &pb);
	printPlatoon(pa);
	printPlatoon(pb);
	destroyPlatoon(pa);
	destroyPlatoon(pb);

	if (mergePlatoons(NULL,NULL) != NULL){
		printf("ERROR\n");
	}
	splitPlatoon(NULL, &pa, &pa);
	splitPlatoon(NULL, NULL, NULL);
	destroyPlatoon(NULL);

	//---------------------------------

	TSOLDIER *a, *b, *c;
	a = initSoldier(0);
	a->m_Next = initSoldier(1);
	a->m_Next->m_Next = initSoldier(2);
	a->m_Next->m_Next->m_Next = initSoldier(3);
	a->m_Next->m_Next->m_Next->m_Next = initSoldier(4);
	b = initSoldier(10);
	b->m_Next = initSoldier(11);
	b->m_Next->m_Next = initSoldier(12);
	b->m_Next->m_Next->m_Next = initSoldier(13);
	b->m_Next->m_Next->m_Next->m_Next = initSoldier(14);
	c = mergePlatoons(a,b);
	printPlatoon(c);
	splitPlatoon(c, &a, &b);
	printf("list a: 0 -> 10 -> 1 -> 11 -> 2\n");
	printPlatoon(a);
	printf("list b: 12 -> 3 -> 13 -> 4 -> 14\n");
	printPlatoon(b);
	destroyPlatoon(a);
	destroyPlatoon(b);
	//===========================================================
	a = initSoldier(0);
	a->m_Next = initSoldier(1);
	a->m_Next->m_Next = initSoldier(2);
	b = initSoldier(10);
	b->m_Next = initSoldier(11);
	b->m_Next->m_Next = initSoldier(12);
	b->m_Next->m_Next->m_Next = initSoldier(13);
	b->m_Next->m_Next->m_Next->m_Next = initSoldier(14);
	c = mergePlatoons(a,b);
	printf("list c: 0 -> 10 -> 1 -> 11 -> 2 -> 12 -> 13 -> 14\n");
	printPlatoon(c);
	splitPlatoon(c, &a, &b);
	printf("list a: 0 -> 10 -> 1 -> 11\n");
	printPlatoon(a);
	printf("list b: 2 -> 12 -> 13 -> 14\n");
	printPlatoon(b);
	destroyPlatoon(a);
	destroyPlatoon(b);
	//===========================================================
	a = initSoldier(0);
	a->m_Next = initSoldier(1);
	a->m_Next->m_Next = initSoldier(2);
	b = initSoldier(10);
	b->m_Next = initSoldier(11);
	b->m_Next->m_Next = initSoldier(12);
	b->m_Next->m_Next->m_Next = initSoldier(13);
	c = mergePlatoons(a,b);
	printf("list c: 0 -> 10 -> 1 -> 11 -> 2 -> 12 -> 13\n");
	printPlatoon(c);
	splitPlatoon(c, &a, &b);
	printf("list a: 0 -> 10 -> 1\n");
	printPlatoon(a);
	printf("list b: 11 -> 2 -> 12\n");
	printPlatoon(b);
	destroyPlatoon(a);
	destroyPlatoon(b);
	//===========================================================

	return 0;
}
#endif /* __PROGTEST__ */