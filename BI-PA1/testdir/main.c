#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//
//  adr: value (xByte)
//  01: a -> 7
//  02: int_point -> 0 -> 01
//  03: |x|y|z|
//  04: b -> adr
//  ...
//  adr: 0
//  adr+1: 1
//  adr+2: 2

typedef struct {
    int x, y, z;
} vector;

typedef vector array_type;

typedef struct {
    array_type* data;
    int size, realSize;
} array;

void initArray(array* a, int baseSize) {
    a->data = malloc(baseSize*(sizeof(*a)));
    a->realSize = baseSize;
    a->size = 0;
}

void insertItem(array* a, array_type val) {
    if (a->size == a->realSize) {
        a->realSize *= 10;
        a->data = realloc(a->data, a->realSize*sizeof(*a));
    }
    a->data[a->size] = val;
    a->size += 1;
}

void freeArray(array* a) {
    if (a->data != NULL) {
        free(a->data);
        a->data = NULL;
        a->realSize = 0;
        a->size = 0;
    }
}

void modifyInt(int* a, int* b, int* c) {
    if (*a>*b){
        *c += 2;
    }
    if (*c < 15) {
        *a *=2;
        *b -= 20;
    }
}
// |0|1|2|
// |0|1|2|

// |021457889|0|40|
// |021457889|0|40|
int modifyvector(array a) {
    vector* vp = malloc(sizeof(vector) * a.size);
    memcpy(vp, a.data, sizeof(vector) * a.size);

    // calc
    return 42;
}

int main(void) {
    array a;
    initArray(&a, 200); // 4 * 200 = 800Byte < 1KB
    // 2000 * 4 = 8000B = 8 KB 
    for (int i = 0; i < 40; ++i) {
        vector v;
        v.x = i;
        v.y = 0;
        v.z = -i;
        insertItem(&a, v);
    }
    for (int i = 0; i < 40; ++i) {
        printf("%d %d %d\n", a.data[i].x, a.data[i].y, a.data[i].z);
    }
    freeArray(&a);
    //freeArray(&a);
    return 0;
}