#include <cstdio>
#include <array>
#include <vector>
#include <memory>


int main() {
    std::array<int, 20> a;  
    std::vector<int> v;
    int* ip = new int(20);
    delete ip;
    ip = nullptr;
    delete ip;
    return 0;
}