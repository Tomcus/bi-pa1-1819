Úkolem je vytvořit program, který bude porovnávat dvojice trojúhelníků.

V rovině jsou zadané 2 trojúhelníky, každý trojúhelník je zadán pomocí trojice svých vrcholů (tedy je celkem zadáno 6 vrcholů). Vrchol trojúhelníku je souřadnice ve 2D rovině, je tedy zadán jako dvě desetinná čísla. Program tyto souřadnice přečte ze svého vstupu a rozhodne pro jednu z následujících variant:

* zda zadané body tvoří trojúhelník (zadaná trojice bodů netvoří trojúhelník, pokud body leží na jedné přímce),
* zda jsou zadané trojúhelníky shodné,
* zda jsou zadané trojúhelníky liší, ale mají stejný obvod, nebo
* zda jsou zadané trojúhelníky zcela odlišné.

Pokud je vstup neplatný, program to musí detekovat a zobrazit chybové hlášení. Chybové hlášení zobrazujte na standardní výstup (ne na chybový výstup). Za chybu považujte:

* nečíselné zadání souřadnic (neplatné desetinné číslo),
* chybějící souřadnice.