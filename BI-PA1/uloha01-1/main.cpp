#include <math.h>
#include <stdio.h>

typedef struct {
    double x;
    double y;
} point;

typedef struct {
    point a;
    double lA;
    point b;
    double lB;
    point c;
    double lC;
    double circuit;
} triangle;

double max(double a, double b){
    if (a > b){
        return a;
    }
    return b;
}

double min(double a, double b){
    if (a < b){
        return a;
    }
    return b;
}

int doubleCompare(double a, double b){
    double res = a - b;
    if (abs(res) >= 0 && abs(res) < 1e-12*max(a, b)){
        return 0;
    }
    if (res > 0){
        return 1;
    }
    return -1;
}

int pointsEqual(point* a, point* b){
    return doubleCompare(a->x, b->x) == 0 && doubleCompare(a->y, b->y) == 0;
}

int loadDouble(double* d){
    return (scanf(" %lf", d) == 1);
}

int loadPoint(point* p){
    return (loadDouble(&p->x)) && (loadDouble(&p->y));
}

point pointsDiff(point* a, point* b){
    point res = {a->x - b->x, a->y - b->y};
    return res;
}

point pointsAdd(point* a, point* b){
    point res = {a->x + b->x, a->y + b->y};
    return res;
}

double calcDistance(point* a, point* b){
    point res = pointsDiff(a, b);
    double distance = sqrt(res.x*res.x + res.y*res.y);
    return distance;
}

int loadTriangle(triangle* tr){
    printf("Bod A:\n");
    if (!loadPoint(&tr->a))
        return 0;
    printf("Bod B:\n");
    if (!loadPoint(&tr->b))
        return 0;
    printf("Bod C:\n");
    if (!loadPoint(&tr->c))
        return 0;
    tr->lA = calcDistance(&tr->a, &tr->b);
    tr->lB = calcDistance(&tr->b, &tr->c);
    tr->lC = calcDistance(&tr->c, &tr->a);
    tr->circuit = tr->lA + tr->lB + tr->lC;
    return 1;
}

int isTriangle(triangle* tr){
    return doubleCompare(tr->lA + tr->lB, tr->lC) > 0 &&
           doubleCompare(tr->lB + tr->lC, tr->lA) > 0 &&
           doubleCompare(tr->lA + tr->lC, tr->lB) > 0;
}

int sameTriangles(triangle* trA, triangle* trB){
    return doubleCompare(max(max(trA->lA, trA->lB), trA->lC), max(max(trB->lA, trB->lB), trB->lC)) == 0 &&
           doubleCompare(min(min(trA->lA, trA->lB), trA->lC), min(min(trB->lA, trB->lB), trB->lC)) == 0 &&
           doubleCompare(trA->circuit, trB->circuit) == 0;
}

int loadData(triangle* trA, triangle* trB){
    printf ("Trojuhelnik #1:\n");
    if (!loadTriangle(trA)){
        printf("Nespravny vstup.\n");
        return 0;
    }
    if (!isTriangle(trA)){
        printf("Body netvori trojuhelnik.\n");
        return 0;
    }
    printf ("Trojuhelnik #2:\n");
    if (!loadTriangle(trB)){
        printf("Nespravny vstup.\n");
        return 0;
    }
    if (!isTriangle(trB)){
        printf("Body netvori trojuhelnik.\n");
        return 0;
    }
    return 1;
}

int main(void){
    triangle a, b;
    if (!loadData(&a, &b)){
        return 1;
    }
    if (sameTriangles(&a, &b)){
        printf("Trojuhelniky jsou shodne.\n");
    } else if (doubleCompare(a.circuit, b.circuit) == 0){
        printf("Trojuhelniky nejsou shodne, ale maji stejny obvod.\n");
    } else if (doubleCompare(a.circuit, b.circuit) > 0){
        printf("Trojuhelnik #1 ma vetsi obvod.\n");
    } else {
        printf("Trojuhelnik #2 ma vetsi obvod.\n");
    }
    return 0;
}
