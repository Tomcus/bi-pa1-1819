#include <stdio.h>
#include <string.h>

int loadNumber(int* i){
    return scanf(" %d", i) == 1;
}

int loadChar(char* c){
    return scanf(" %c", c) == 1;
}

int expectChar(char expected){
    char c;
    return loadChar(&c) && c == expected;
}

void itoa(int i, int base, int* length, int* zeroCount, int* zeroSequence){
    int n = base;
    int pos = 0;
    int mod;
    int max = 0;
    int currentSequence = 0;
    while (i > 0) {
        mod = i % n;
        if (mod == 0) {
            if (zeroCount != NULL)
                *zeroCount += 1;
            currentSequence++;
        } else {
            max = (currentSequence > max)?currentSequence:max;
            currentSequence = 0;
        }
        pos++;
        n *= base;
        i -= mod;
    }
    if (length != NULL){
        *length = pos;
    }
    if (zeroSequence != NULL)
        *zeroSequence = max;
}

void calculateLength(int base, int low, int high){
    int length = 0;
    if (low == 0){
        length++;
    }
    int currLen = 0;
    for (int i = low; i <= high; ++i){
        currLen = 0;
        itoa(i, base, &currLen, NULL, NULL);
        length += currLen;
    }
    printf("Cifer: %d\n", length);
}

void calculateAllZerosInSequence(int base, int low, int high){
    int zeroCount = 0;
    if (low == 0)
        zeroCount++;
    int currZC = 0;
    for (int i = low; i <= high; ++i){
        currZC = 0;
        itoa(i, base, NULL, &currZC, NULL);
        zeroCount += currZC;
    }
    printf("Nul: %d\n", zeroCount);
}

void calculateMaxZeroCountOfSequence(int base, int low, int high){
    int max = 0;
    if (low == 0)
        max = 1;
    int currMax = 0;
    for (int i = low; i <= high; ++i){
        currMax = 0;
        itoa(i, base, NULL, NULL, &currMax);
        max = (currMax>max)?currMax:max;
    }
    printf("Sekvence: %d\n", max);
}

int main(void){
    int base = 10, low = 0, high = 0;
    char c;
    printf("Zadejte intervaly:\n");
    while (true) {
        if (!loadChar(&c)){
            if (feof(stdin))
                break;
            printf("Nespravny vstup.\n");
            return 1;
        }
        if (c == 'r'){
            if (!loadNumber(&base) || base < 2 || base > 36 || !expectChar(':') || !expectChar('<')){
                printf("Nespravny vstup.\n");
                return 1;
            }
        } else if (c != '<') {
            printf("Nespravny vstup.\n");
            return 1;
        } else {
            base = 10;
        }
        if (!loadNumber(&low) || low < 0 || !expectChar(';') 
            || !loadNumber(&high) || high < 0 || low > high
            || !expectChar('>') || !loadChar(&c)) {
            printf("Nespravny vstup.\n");
            return 1;
        }
        switch(c){
            case 'l':
            calculateLength(base, low, high);
            break;
            case 'z':
            calculateAllZerosInSequence(base, low, high);
            break;
            case 's':
            calculateMaxZeroCountOfSequence(base, low, high);
            break;
            default:
            printf("Nespravny vstup.\n");
            return 1;
        }
    }
    return 0;
}