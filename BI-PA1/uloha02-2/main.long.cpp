#include <stdio.h>
#include <string.h>

int loadNumber(long long int* i){
    return scanf(" %lld", i) == 1;
}

int loadChar(char* c){
    return scanf(" %c", c) == 1;
}

int expectChar(char expected){
    char c;
    return loadChar(&c) && c == expected;
}

void itoa(long long int i, long long int base, long long int* length, long long int* zeroCount, long long int* zeroSequence){
    long long int n = base;
    long long int pos = 0;
    long long int mod;
    long long int max = 0;
    long long int currentSequence = 0;
    while (i > 0) {
        mod = i % n;
        if (mod == 0) {
            if (zeroCount != NULL)
                *zeroCount += 1;
            currentSequence++;
        } else {
            max = (currentSequence > max)?currentSequence:max;
            currentSequence = 0;
        }
        pos++;
        n *= base;
        i -= mod;
    }
    if (length != NULL){
        *length = pos;
    }
    if (zeroSequence != NULL)
        *zeroSequence = max;
}

void calculateLength(long long int base, long long int low, long long int high){
    long long int length = 0;
    if (low == 0){
        length++;
    }
    long long int currLen = 0;
    for (long long int i = low; i <= high; ++i){
        currLen = 0;
        itoa(i, base, &currLen, NULL, NULL);
        length += currLen;
    }
    printf("Cifer: %lld\n", length);
}

void calculateAllZerosInSequence(long long int base, long long int low, long long int high){
    long long int zeroCount = 0;
    if (low == 0)
        zeroCount++;
    long long int currZC = 0;
    for (long long int i = low; i <= high; ++i){
        currZC = 0;
        itoa(i, base, NULL, &currZC, NULL);
        zeroCount += currZC;
    }
    printf("Nul: %lld\n", zeroCount);
}

void calculateMaxZeroCountOfSequence(long long int base, long long int low, long long int high){
    long long int max = 0;
    if (low == 0)
        max = 1;
    long long int currMax = 0;
    for (long long int i = low; i <= high; ++i){
        currMax = 0;
        itoa(i, base, NULL, NULL, &currMax);
        max = (currMax>max)?currMax:max;
    }
    printf("Sekvence: %lld\n", max);
}

int main(void){
    long long int base = 10, low = 0, high = 0;
    char c;
    printf("Zadejte intervaly:\n");
    while (true) {
        if (!loadChar(&c)){
            if (feof(stdin))
                break;
            printf("Nespravny vstup.\n");
            return 1;
        }
        if (c == 'r'){
            if (!loadNumber(&base) || base < 2 || base > 36 || !expectChar(':') || !expectChar('<')){
                printf("Nespravny vstup.\n");
                return 1;
            }
        } else if (c != '<') {
            printf("Nespravny vstup.\n");
            return 1;
        } else {
            base = 10;
        }
        if (!loadNumber(&low) || low < 0 || !expectChar(';') 
            || !loadNumber(&high) || high < 0 || low > high
            || !expectChar('>') || !loadChar(&c)) {
            printf("Nespravny vstup.\n");
            return 1;
        }
        switch(c){
            case 'l':
            calculateLength(base, low, high);
            break;
            case 'z':
            calculateAllZerosInSequence(base, low, high);
            break;
            case 's':
            calculateMaxZeroCountOfSequence(base, low, high);
            break;
            default:
            printf("Nespravny vstup.\n");
            return 1;
        }
    }
    return 0;
}