#ifndef __PROGTEST__
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#endif /* __PROGTEST__ */

unsigned long long int calculateWithOnePiece(unsigned long long int length, unsigned int s,
											 unsigned int bulkhead, unsigned int* c){
	unsigned int realLen = s + bulkhead;
	unsigned long long int n = length/realLen;
	if (length - realLen * n == bulkhead){
		*c = n;
		return 1;
	}
	return 0;
}

unsigned long long int calculateWithTwoPieces(unsigned long long int length, unsigned int s1,
								 unsigned int s2, unsigned int bulkhead, unsigned int* c1,
								 unsigned int* c2){
	unsigned int realLen = s1 + bulkhead;
	unsigned long long int maxN = length/realLen;
	unsigned long long int i = 0;
	unsigned long long int res = 0;
	for (i = 0; i <= maxN; ++i){
		if (calculateWithOnePiece(length - i * realLen, s2, bulkhead, c2)){
			res++;
			*c1 = (unsigned int)i;
			printf ("%u %u\n", *c1, *c2);
		}
	}
	return res;
}

unsigned long long int hyperloop(unsigned long long int length, unsigned int s1, unsigned int s2,
                                 unsigned int bulkhead, unsigned int* c1, unsigned int* c2){
	if (length == 0 || (s1 == 0 && s2 == 0 && length != bulkhead))
		return 0;
	if (s1 == 0 && s2 == 0 && length == bulkhead){
		*c1 = *c2 = 0;
		return 1;
	}
	if (s1 == s2 || s2 == 0) {
		if (calculateWithOnePiece(length, s1, bulkhead, c1)){
			*c2 = 0;
			return 1;
		}
		return 0;
	}
	if (s1 == 0){
		if (calculateWithOnePiece(length, s2, bulkhead, c2)){
			*c1 = 0;
			return 1;
		}
		return 0;
	}
	return calculateWithTwoPieces(length, s1, s2, bulkhead, c1, c2);
}

#ifndef __PROGTEST__
int main ( void )
{
  unsigned int c1, c2;
  assert ( calculateWithOnePiece(170, 10, 1, &c1) == 0 );
  assert ( calculateWithOnePiece(170, 10, 0, &c1) == 1 );
  assert ( c1 == 17 );
  assert ( calculateWithOnePiece(171, 9, 1, &c1) == 1 );
  assert ( c1 == 17 );
  assert ( hyperloop ( 100, 4, 7, 0, &c1, &c2 ) == 4 );
  assert ( 4 * c1 + 7 * c2 + 0 * (c1 + c2 + 1) == 100 );
  assert ( hyperloop ( 123456, 8, 3, 3, &c1, &c2 ) == 1871 );
  assert ( 8 * c1 + 3 * c2 + 3 * (c1 + c2 + 1) == 123456 );
  assert ( hyperloop ( 127, 12, 8, 0, &c1, &c2 ) == 0 );
  assert ( hyperloop ( 127, 12, 4, 3, &c1, &c2 ) == 1 );
  assert ( 12 * c1 + 4 * c2 + 3 * (c1 + c2 + 1) == 127 );
  assert ( hyperloop ( 100, 35, 0, 10, &c1, &c2 ) == 1 );
  assert ( c2 == 0 );
  assert ( 35 * c1 + 10 * (c1 + 1) == 100 );
  assert ( hyperloop ( 110, 30, 30, 5, &c1, &c2 ) == 1 );
  assert ( 30 * c1 + 30 * c2 + 5 * (c1 + c2 + 1) == 110 );
  c1 = 2;
  c2 = 7;
  assert ( hyperloop ( 110, 30, 30, 0, &c1, &c2 ) == 0 );
  assert ( c1 == 2 );
  assert ( c2 == 7 );
  c1 = 4;
  c2 = 8;
  assert ( hyperloop ( 9, 1, 2, 10, &c1, &c2 ) == 0 && c1 == 4 && c2 == 8 );
  assert ( hyperloop ( 29281, 44, 42, 7, &c1, &c2 ) == 12 );
  return 0;
}
#endif /* __PROGTEST__ */
