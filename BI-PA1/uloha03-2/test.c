#ifndef __PROGTEST__
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>


#define assert(expr) ((expr) ? printf("------------------\n"):__assert_fail(#expr, __FILE__, __LINE__, __ASSERT_FUNCTION))
#endif /* __PROGTEST__ */

unsigned long long int calculateWithOnePiece(unsigned long long int length, unsigned int s,
											 unsigned int bulkhead, unsigned int* c){
	unsigned int realLen = s + bulkhead;
	unsigned long long int n = length/realLen;
	if (length - realLen * n == bulkhead){
		*c = n;
		return 1;
	}
	return 0;
}

unsigned long long int calculateWithTwoPieces(unsigned long long int length, unsigned int s1,
								 			  unsigned int s2, unsigned int bulkhead, unsigned int* c1,
											  unsigned int* c2){
	unsigned int realLen = s1 + bulkhead;
	unsigned long long int maxN = length/realLen;
	unsigned long long int i = 0;
	unsigned long long int res = 0;
	for (i = 0; i <= maxN; ++i){
		if (calculateWithOnePiece(length - i * realLen, s2, bulkhead, c2)){
			res++;
			*c1 = (unsigned int)i;
			// printf ("%u %u\n", *c1, *c2);
		}
	}
	return res;
}

unsigned long long int calculateWithThreePieces(unsigned long long int length, unsigned int s1, unsigned int s2,
                                  			  	unsigned int s3, unsigned int bulkhead, unsigned int* c1,
                                  			  	unsigned int* c2, unsigned int* c3){
	unsigned int realLen = s1 + bulkhead;
	unsigned long long int maxN = length/realLen;
	unsigned long long int i = 0;
	unsigned long long int res = 0;
	unsigned long long int toAdd = 0;
	for (i = 0; i <= maxN; ++i){
		toAdd = calculateWithTwoPieces(length - i * realLen, s2, s3, bulkhead, c2, c3);
		res+=toAdd;
		if (toAdd) {
			*c1 = (unsigned int)i;
			printf ("%u %u %u\n", *c1, *c2, *c3);
		}
	}
	return res;
}

unsigned long long int hyperloop (unsigned long long int length, unsigned int s1, unsigned int s2,
                                  unsigned int s3, unsigned int bulkhead, unsigned int* c1,
                                  unsigned int* c2, unsigned int* c3 ) {
	unsigned long long int res = 0;
    if (length == 0)
		return 0;
	if (s1 == 0 && s2 == 0 && s3 == 0)
		return length == bulkhead;
	if (s1 == 0){
		*c1 = 0;
		if (s2 == 0){
			*c2 = 0;
			res = calculateWithOnePiece(length, s3, bulkhead, c3);
		} else if (s3 == 0 || s2 == s3){
			*c3 = 0;
			res = calculateWithOnePiece(length, s2, bulkhead, c2);
		} else {
			res = calculateWithTwoPieces(length, s2, s3, bulkhead, c2, c3);
		}
	} else {
		if (s2 == 0){
			*c2 = 0;
			if (s3 == 0 || s1 == s3){
				*c3 = 0;
				res = calculateWithOnePiece(length, s1, bulkhead, c1);
			} else {
				res = calculateWithTwoPieces(length, s1, s3, bulkhead, c1, c3);
			}
		} else {
			if (s3 == 0){
				res = calculateWithTwoPieces(length, s1, s2, bulkhead, c1, c2);
			} else {
				if (s1 == s2 && s2 == s3)
					res = calculateWithOnePiece(length, s1, bulkhead, c1);
				else if (s1 == s2 && s2 != s3)
					res = calculateWithTwoPieces(length, s1, s3, bulkhead, c1, c3);
				else if (s1 != s2 && s2 == s3)
					res = calculateWithTwoPieces(length, s2, s3, bulkhead, c2, c3);
				else if (s1 == s3 && s1 != s2)
					res = calculateWithTwoPieces(length, s1, s3, bulkhead, c1, c3);
				else
					res = calculateWithThreePieces(length, s1, s2, s3, bulkhead, c1, c2, c3);
			}
		}
	}
	return res;
}

#ifndef __PROGTEST__
int main ( void )
{
  unsigned int c1, c2, c3;
  assert ( hyperloop ( 100, 4, 7, 5, 0, &c1, &c2, &c3 ) == 42
           && 4 * c1 + 7 * c2 + 5 * c3 + 0 * ( c1 + c2 + c3 + 1 ) == 100 );
  assert ( hyperloop ( 12345, 8, 3, 11, 3, &c1, &c2, &c3 ) == 82708
           && 8 * c1 + 3 * c2 + 11 * c3 + 3 * ( c1 + c2 + c3 + 1 ) == 12345 );
  c1 = 8;
  c2 = 9;
  c3 = 10;
  assert ( hyperloop ( 127, 12, 8, 10, 0, &c1, &c2, &c3 ) == 0 );
  assert ( c1 == 8 );
  assert ( c2 == 9 );
  assert ( c3 == 10 );
  assert ( hyperloop ( 127, 12, 8, 10, 3, &c1, &c2, &c3 ) == 4 );
  assert ( 12 * c1 + 8 * c2 + 10 * c3 + 3 * ( c1 + c2 + c3 + 1 ) == 127 );
//   printf("%llu\n", hyperloop ( 100, 35, 0, 0, 10, &c1, &c2, &c3 ));
  assert ( hyperloop ( 100, 35, 0, 0, 10, &c1, &c2, &c3 ) == 1 );
  assert ( c2 == 0 );
  assert ( c3 == 0 );
  assert ( 35 * c1 + 10 * ( c1 + 1 ) == 100 );
  assert ( hyperloop ( 100, 35, 0, 35, 10, &c1, &c2, &c3 ) == 1 );
  assert ( c2 == 0 );
  assert ( 35 * c1 + 35 * c3 + 10 * ( c1 + c3 + 1 ) == 100 );
  assert ( hyperloop ( 100, 35, 35, 35, 10, &c1, &c2, &c3 ) == 1 );
  assert ( 35 * c1 + 35 * c2 + 35 * c3 + 10 * ( c1 + c2 + c3 + 1 ) == 100 );
  c1 = 42;
  c2 = 43;
  c3 = 44;
  assert ( hyperloop ( 9, 1, 2, 3, 10, &c1, &c2, &c3 ) == 0 );
  assert ( c1 == 42 );
  assert ( c2 == 43 );
  assert ( c3 == 44 );
  assert (hyperloop ( 94489, 183, 49, 49, 16, &c1, &c2, &c3 ) == 8);
  return 0;
}
#endif /* __PROGTEST__ */
