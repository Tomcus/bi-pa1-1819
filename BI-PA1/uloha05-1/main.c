#include <stdio.h>
#include <stdlib.h>

#define FEE_COUNT 26
#define BASIC_SIZE 200
#define SIZE_INCREASE 2

unsigned int min(unsigned int a, unsigned int b){
    return (a > b)?b:a;
}

unsigned int max(unsigned int a, unsigned int b){
    return (a > b)?a:b;
}

int expectCharOnInput(char expected) {
    char loaded;
    return scanf(" %c", &loaded) == 1 && loaded == expected;
}

typedef struct {
    unsigned int from, to;
    double fees[FEE_COUNT];
} area;

int loadArea(area* a, unsigned int passedDistance, double previousFees[FEE_COUNT]) {
    int distance;
    char loadedType;
    double loadedCost;
    int i;
    if (previousFees != NULL) {
        for (i = 0; i < FEE_COUNT; ++i) {
            a->fees[i] = previousFees[i];
        }
    } else {
        for (i = 0; i < FEE_COUNT; ++i){
            a->fees[i] = 0.0;
        }
    }
    if (!expectCharOnInput('[') || scanf(" %d", &distance) != 1 
        || distance <= 0 || !expectCharOnInput(':')){
        return 0;
    }
    a->from = passedDistance;
    a->to = passedDistance + distance;
    while (1) {
        if (scanf(" %c", &loadedType) != 1 || loadedType < 'A' || loadedType > 'Z' || 
            !expectCharOnInput('=') || scanf(" %lf", &loadedCost) != 1 || loadedCost < 0.0){
            return 0;
        }
        a->fees[loadedType - 'A'] = loadedCost;
        if (scanf(" %c", &loadedType) != 1)
            return 0;
        if (loadedType == ']')
            break;
        if (loadedType != ',')
            return 0;
    }
    return 1;
}

typedef struct {
    area* areas;
    unsigned int size, realSize;
} listOfAreas;

void initList(listOfAreas* loa) {
    loa->size = 0;
    loa->realSize = BASIC_SIZE;
    loa->areas = (area*)malloc(BASIC_SIZE * sizeof(area));
}

int loadAndAppendList(listOfAreas* loa) {
    if (loa->size == loa->realSize){
        loa->areas = (area *)realloc(loa->areas, loa->realSize * sizeof(area) * SIZE_INCREASE);
        loa->realSize *= SIZE_INCREASE;
    }
    if (!loadArea(&loa->areas[loa->size], (loa->size == 0)?0:loa->areas[loa->size - 1].to,
        (loa->size == 0)?NULL:loa->areas[loa->size - 1].fees)){
        return 0;
    }
    loa->size += 1;
    return 1;
}

void freeList(listOfAreas* loa) {
    free(loa->areas);
}

int loadList(listOfAreas* loa) {
    char c;
    printf("Myto:\n");
    initList(loa);
    if (!expectCharOnInput('{'))
        return 0;
    while (1){
        if (!loadAndAppendList(loa) || scanf(" %c", &c) != 1)
            return 0;
        if (c == '}')
            break;
        if (c != ',')
            return 0;
    }
    return 1;
}

int findStartIndex(listOfAreas* loa, unsigned int toFind) {
    unsigned int min = 0, max = loa->size - 1;
    unsigned int index = (max - min)/2;
    while (max - min != 0) {
        // printf ("%u %u %u\n", min, index, max);
        if (max - min != 1) {
            if (toFind < loa->areas[min + index].from) {
                max = min + index;
            } else {
                if (loa->areas[min + index].to < toFind) {
                    min = min + index;
                } else if (loa->areas[min + index].to == toFind){
                    return min + index + 1;
                } else {
                    return min + index;
                }
            }
            index = (max - min)/2;
        } else {
            if (toFind >= loa->areas[min].from && toFind < loa->areas[min].to){
                return min;
            }
            if (toFind >= loa->areas[max].from && toFind < loa->areas[max].to){
                return max;
            }
            return -100;
        }
    }
    return index;
}

void calculateFees(unsigned int low, unsigned int high, listOfAreas* loa, double fees[FEE_COUNT]){
    int i, kms, index;
    for (i = 0; i < FEE_COUNT; ++i){
        fees[i] = 0.0;
    }
    index = findStartIndex(loa, low);
    while (low < high){
        // printf ("%u %u", low ,high);
        kms = min(high, loa->areas[index].to) - low;
        for (i = 0; i < FEE_COUNT; ++i){
            fees[i] += kms * loa->areas[index].fees[i];
        }
        low = loa->areas[index].to;
        index++;
    }
}

int doWork(listOfAreas* loa) {
    double fees[FEE_COUNT];
    unsigned int p1, p2, res, maxP = loa->areas[loa->size - 1].to;
    int i, printed;
    printf("Hledani:\n");
    while (1) {
        printed = 0;
        res = scanf(" %u", &p1);
        if (res != 1) {
            if (feof(stdin))
                return 1;
            else 
                return 0;
        }
        if (p1 > maxP || scanf(" %u", &p2) != 1 || p2 > maxP || p1 == p2)
            return 0;
        calculateFees(min(p1, p2), max(p1, p2), loa, fees);
        printf("%u - %u:", p1, p2);
        for (i = 0; i < FEE_COUNT; ++i){
            if (fees[i] > 0.0){
                if (printed){
                    printf(", %c=%lf", 'A'+i, fees[i]);
                } else {
                    printed = 1;
                    printf(" %c=%lf", 'A'+i, fees[i]);
                }
            }
        }
        printf("\n");
    }
}

int main(){
    listOfAreas list;
    if (!loadList(&list) || !doWork(&list)){
        freeList(&list);
        printf("Nespravny vstup.\n");
        return 1;
    }
    freeList(&list);
    return 0;
}
