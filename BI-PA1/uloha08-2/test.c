#ifndef __PROGTEST__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

typedef struct TCar{
	struct TCar* m_Next;
	char* m_Model;
} TCAR;

typedef struct TEmployee{
	struct TEmployee* m_Next;
	struct TCar* m_Car;
	char* m_Name;
} TEMPLOYEE;

typedef struct TOffice{
	TEMPLOYEE* m_Emp;
	TCAR* m_Car;
} TOFFICE;

#endif /* __PROGTEST__ */

#define HASH_MAP_SIZE 256
#define INITIAL_SIZE 10
#define RESIZE 2

typedef struct {
	void* first;
	void* second;
} Pair;

void initPair(Pair* p, void* firstVal, void* secondVal){
	p->first = firstVal;
	p->second = secondVal;
}

typedef struct {
	Pair* data;
	int size, realSize;
} PairArray;

PairArray* createPairArray(){
	PairArray* arr = (PairArray *)malloc(sizeof(PairArray));
	arr->data = (Pair *)malloc(sizeof(Pair) * INITIAL_SIZE);
	arr->size = 0;
	arr->realSize = INITIAL_SIZE;
	return arr;
}

void freePairArray(PairArray* p){
	if (p != NULL && p->data != NULL) {
		free(p->data);
		free(p);
	}
}

void addPairToArray(PairArray* p, void* firstVal, void* secondVal) {
	if (p->size == p->realSize) {
		p->data = (Pair *)realloc(p->data, sizeof(Pair) * p->realSize * RESIZE);
		p->realSize *= RESIZE;
	}
	initPair(&p->data[p->size], firstVal, secondVal);
	p->size += 1;
}

void* findElementInPairArray(PairArray* arr, void* firstVal){
	void * res = NULL;
	int i;
	if (arr != NULL && firstVal != NULL) {
		for (i = 0; i < arr->size; ++i) {
			if (arr->data[i].first == firstVal) {
				res = arr->data[i].second;
				break;
			}
		}
	}
	return res;
}

typedef struct {
	PairArray* map[HASH_MAP_SIZE];
} HashMap;

void initHashMap(HashMap* hm){
	int i;
	for (i = 0; i < HASH_MAP_SIZE; ++i){
		hm->map[i] = NULL;
	}
}

void insertCarIntoHashMap(HashMap* hm, TCAR* oldCar, TCAR* newCar) {
	PairArray* arr = hm->map[(size_t)oldCar % HASH_MAP_SIZE];
	if (arr == NULL) {
		arr = (hm->map[(size_t)oldCar % HASH_MAP_SIZE]) = createPairArray();
	}
	addPairToArray(arr, (void *)oldCar, (void *)newCar);
}

TCAR* getCarByOldCarPointer(HashMap* hm, TCAR* oldCar) {
	PairArray* arr = hm->map[(size_t)oldCar % HASH_MAP_SIZE];
	return (TCAR *)findElementInPairArray(arr, (void *)oldCar);
}

void freeHashMap(HashMap* hm){
	for(int i = 0; i < HASH_MAP_SIZE; ++i){
		freePairArray(hm->map[i]);
	}
}

TCAR* createCar(const char* model_name, TCAR* car) {
	TCAR* newCar = (TCAR *)malloc(sizeof(TCAR));
	newCar->m_Model = (char *)malloc(sizeof(char) * (strlen(model_name) + 1));
	newCar->m_Next = car;
	strcpy(newCar->m_Model, model_name);
	return newCar;
}

void freeCar(TCAR* car) {
	if (car != NULL) {
		free(car->m_Model);
		free(car);
	}
}

TEMPLOYEE* createEmployee(const char* name, TEMPLOYEE* next) {
	TEMPLOYEE* newEmployee = (TEMPLOYEE *)malloc(sizeof(TEMPLOYEE));
	newEmployee->m_Next = next;
	newEmployee->m_Car = NULL;
	newEmployee->m_Name = (char *)malloc(sizeof(char) * (strlen(name) + 1));
	strcpy(newEmployee->m_Name, name);
	return newEmployee;
}

void freeEmployee(TEMPLOYEE* emp){
	if (emp != NULL){
		free(emp->m_Name);
		free(emp);
	}
}

TOFFICE* initOffice() {
	TOFFICE* newOffice = (TOFFICE *)malloc(sizeof(TOFFICE));
	newOffice->m_Emp = NULL;
	newOffice->m_Car = NULL;
	return newOffice;
}

void addEmployee (TOFFICE* office, const char* name) {
	if (office != NULL && name != NULL) {
		office->m_Emp = createEmployee(name, office->m_Emp);
	}
}

void addCar (TOFFICE* office, const char* model) {
	if (office != NULL && model != NULL) {
		office->m_Car = createCar(model, office->m_Car);
	}
}

TOFFICE* cloneOffice (TOFFICE* office) {
	HashMap hm;
	TCAR* car;
	TCAR* prevCar = NULL;
	TCAR* newCar = NULL;
	TEMPLOYEE* emp;
	TEMPLOYEE* prevEmp = NULL;
	TEMPLOYEE* newEmp = NULL;
	TOFFICE* newOffice = initOffice();
	if (office == NULL)
		return NULL;
	initHashMap(&hm);
	car = office->m_Car;
	while (car != NULL) {
		newCar = createCar(car->m_Model, NULL);
		// printf("Cloning car: %s\n", car->m_Model);
		insertCarIntoHashMap(&hm, car, newCar);
		if (prevCar == NULL) {
			newOffice->m_Car = newCar;
		} else {
			prevCar->m_Next = newCar;
		}
		car = car->m_Next;
		prevCar = newCar;
	}
	emp = office->m_Emp;
	while (emp != NULL) {
		newEmp = createEmployee(emp->m_Name, NULL);
		// printf("Cloning employee: %s\n", emp->m_Name);
		newEmp->m_Car = getCarByOldCarPointer(&hm, emp->m_Car);
		if (prevEmp == NULL) {
			newOffice->m_Emp = newEmp;
		} else {
			prevEmp->m_Next = newEmp;
		}
		emp = emp->m_Next;
		prevEmp = newEmp;
	}
	freeHashMap(&hm);
	return newOffice;
}

void freeOffice (TOFFICE* office) {
	TCAR* car;
	TCAR* tmpCar;
	TEMPLOYEE* emp;
	TEMPLOYEE* tmpEmp;
	if (office != NULL) {
		car = office->m_Car;
		emp = office->m_Emp;
		while (car != NULL || emp != NULL) {
			if (car != NULL) {
				tmpCar = car->m_Next;
				freeCar(car);
				car = tmpCar;
			}
			if (emp != NULL) {
				tmpEmp = emp->m_Next;
				freeEmployee(emp);
				emp = tmpEmp;
			}
		}
		free(office);
	}
}

#ifndef __PROGTEST__

void printOffice(TOFFICE* off) {
	printf("CARS\n");
	TCAR* car = off->m_Car;
	while(car != NULL) {
		printf("\t%s\n", car->m_Model);
		car = car->m_Next;
	}
	TEMPLOYEE* emp = off->m_Emp;
	printf("EMPLOYEES\n");
	while(emp != NULL){
		printf("\t%s\n", emp->m_Name);
		if (emp->m_Car != NULL) {
			printf("\t\t%s\n", emp->m_Car->m_Model);
		}
		emp = emp->m_Next;
	}
}

int main ( int argc, char * argv [] )
{
	TOFFICE * a, *b;
	char tmp[100];
	assert ( sizeof ( TOFFICE ) == 2 * sizeof ( void * ) );
	assert ( sizeof ( TEMPLOYEE ) == 3 * sizeof ( void * ) );
	assert ( sizeof ( TCAR ) == 2 * sizeof ( void * ) );
	a = initOffice ();
	addEmployee ( a, "Peter" );
	addEmployee ( a, "John" );
	addEmployee ( a, "Joe" );
	addEmployee ( a, "Maria" );
	addCar ( a, "Skoda Octavia" );
	addCar ( a, "VW Golf" );
	a -> m_Emp -> m_Car = a -> m_Car;
	a -> m_Emp -> m_Next -> m_Next -> m_Car = a -> m_Car -> m_Next;
	a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Car = a -> m_Car;
	assert ( a -> m_Emp
	         && ! strcmp ( a -> m_Emp -> m_Name, "Maria" )
	         && a -> m_Emp -> m_Car == a -> m_Car );
	assert ( a -> m_Emp -> m_Next
	         && ! strcmp ( a -> m_Emp -> m_Next -> m_Name, "Joe" )
	         && a -> m_Emp -> m_Next -> m_Car == NULL );
	assert ( a -> m_Emp -> m_Next -> m_Next
	         && ! strcmp ( a -> m_Emp -> m_Next -> m_Next -> m_Name, "John" )
	         && a -> m_Emp -> m_Next -> m_Next -> m_Car == a -> m_Car -> m_Next );
	assert ( a -> m_Emp -> m_Next -> m_Next -> m_Next
	         && ! strcmp ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Name, "Peter" )
	         && a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Car == a -> m_Car );
	assert ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next == NULL );
	assert ( a -> m_Car
	         && ! strcmp ( a -> m_Car -> m_Model, "VW Golf" ) );
	assert ( a -> m_Car -> m_Next
	         && ! strcmp ( a -> m_Car -> m_Next -> m_Model, "Skoda Octavia" ) );
	assert ( a -> m_Car -> m_Next -> m_Next == NULL );
	b = cloneOffice ( a );
	strncpy ( tmp, "Moe", sizeof ( tmp ) );
	addEmployee ( a, tmp );
	strncpy ( tmp, "Victoria", sizeof ( tmp ) );
	addEmployee ( a, tmp );
	strncpy ( tmp, "Peter", sizeof ( tmp ) );
	addEmployee ( a, tmp );
	strncpy ( tmp, "Citroen C4", sizeof ( tmp ) );
	addCar ( a, tmp );
	b -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Car = b -> m_Car -> m_Next -> m_Next;
	assert ( a -> m_Emp
	         && ! strcmp ( a -> m_Emp -> m_Name, "Peter" )
	         && a -> m_Emp -> m_Car == NULL );
	assert ( a -> m_Emp -> m_Next
	         && ! strcmp ( a -> m_Emp -> m_Next -> m_Name, "Victoria" )
	         && a -> m_Emp -> m_Next -> m_Car == NULL );
	assert ( a -> m_Emp -> m_Next -> m_Next
	         && ! strcmp ( a -> m_Emp -> m_Next -> m_Next -> m_Name, "Moe" )
	         && a -> m_Emp -> m_Next -> m_Next -> m_Car == NULL );
	assert ( a -> m_Emp -> m_Next -> m_Next -> m_Next
	         && ! strcmp ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Name, "Maria" )
	         && a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Car == a -> m_Car -> m_Next );
	assert ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next
	         && ! strcmp ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next -> m_Name, "Joe" )
	         && a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next -> m_Car == NULL );
	assert ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next
	         && ! strcmp ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Name, "John" )
	         && a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Car == a -> m_Car -> m_Next -> m_Next );
	assert ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next
	         && ! strcmp ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Name, "Peter" )
	         && a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Car == a -> m_Car -> m_Next );
	assert ( a -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next -> m_Next == NULL );
	assert ( a -> m_Car
	         && ! strcmp ( a -> m_Car -> m_Model, "Citroen C4" ) );
	assert ( a -> m_Car -> m_Next
	         && ! strcmp ( a -> m_Car -> m_Next -> m_Model, "VW Golf" ) );
	assert ( a -> m_Car -> m_Next -> m_Next
	         && ! strcmp ( a -> m_Car -> m_Next -> m_Next -> m_Model, "Skoda Octavia" ) );
	assert ( a -> m_Car -> m_Next -> m_Next -> m_Next == NULL );
	assert ( b -> m_Emp
	         && ! strcmp ( b -> m_Emp -> m_Name, "Maria" )
	         && b -> m_Emp -> m_Car == b -> m_Car );
	assert ( b -> m_Emp -> m_Next
	         && ! strcmp ( b -> m_Emp -> m_Next -> m_Name, "Joe" )
	         && b -> m_Emp -> m_Next -> m_Car == NULL );
	assert ( b -> m_Emp -> m_Next -> m_Next
	         && ! strcmp ( b -> m_Emp -> m_Next -> m_Next -> m_Name, "John" )
	         && b -> m_Emp -> m_Next -> m_Next -> m_Car == b -> m_Car -> m_Next );
	assert ( b -> m_Emp -> m_Next -> m_Next -> m_Next
	         && ! strcmp ( b -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Name, "Peter" )
	         && b -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Car == NULL );
	assert ( b -> m_Emp -> m_Next -> m_Next -> m_Next -> m_Next == NULL );
	assert ( b -> m_Car
	         && ! strcmp ( b -> m_Car -> m_Model, "VW Golf" ) );
	assert ( b -> m_Car -> m_Next
	         && ! strcmp ( b -> m_Car -> m_Next -> m_Model, "Skoda Octavia" ) );
	assert ( b -> m_Car -> m_Next -> m_Next == NULL );
	freeOffice ( a );
	freeOffice ( b );
	a = initOffice();
	for (int i = 0; i < 2560; ++i){
		addEmployee(a, "Empl");
		addCar(a, "Car");
	}
	b = cloneOffice(a);
	freeOffice(a);
	freeOffice(b);
	return 0;
}
#endif /* __PROGTEST__ */
