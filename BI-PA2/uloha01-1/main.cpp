#ifndef __PROGTEST__

#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <stdint.h>

using namespace std;
#endif /* __PROGTEST__ */
typedef int32_t Int;
/*========================================CSYMBOL=======================================*/
class CSymbol{
public:
	Int getDataLen(){ return dataLen; }
	Int getOffset(){ return offset; }
	Int getNameLen(){ return nameLen; }
	CSymbol(){
		nameLen = dataLen = offset = 0;
		data = NULL;
		name = NULL;
	}
	bool loadHead(fstream & input){
		if (!input.read((char *)&nameLen, sizeof(nameLen))) {
			return false;
		}
		if (!input.read((char *)&dataLen, sizeof(dataLen))) {
			return false;
		}
		if (!input.read((char *)&offset, sizeof(offset))) {
			return false;
		}
		if (name != NULL)
			delete[] name;
		name = new char [nameLen + 1];
		if (!input.read(name, nameLen)){
			return false;
		}
		name[nameLen] = 0;
		return true;
	}
	bool loadData(fstream & input){
		if (data != NULL)
			delete [] data;
		data = new char[dataLen];
		if (!input.read(data, dataLen)){
			return false;
		}

		return true;
	}
	bool writeHead(fstream & output, Int offsetPlus = 0){
		offset += offsetPlus;
		return !(!output.write((char *) &nameLen, sizeof(nameLen))
		         || !output.write((char *) &dataLen, sizeof(dataLen))
		         || !output.write((char *) &offset, sizeof(offset))
		         || !output.write(name, nameLen));
	}
	bool writeData(fstream & output){
		return (bool)output.write(data, dataLen);
	}
	Int headLen(){ return 12 + nameLen;	}
	char * getName(){ return name; }
	~CSymbol(){
		if (name != NULL)
			delete [] name;
		if (data != NULL)
			delete [] data;
	}
private:
	Int nameLen;
	Int dataLen;
	Int offset;
	char * name;
	char * data;
};
/*====================================COMPARE=FUNCTION==================================*/
int compare(const void * a, const void * b){
	CSymbol * aa, * bb;
	aa = (CSymbol *)a;
	bb = (CSymbol *)b;
	return aa->getOffset() - bb->getOffset();
}
/*=========================================CFILE========================================*/
class CFile{
public:
	CFile(fstream & input){
		input.read((char *)&cnt, sizeof(cnt));
		arr = new CSymbol[cnt];
		dataLen = 0;
		headLen = sizeof(cnt);
	}
	bool readHeader(fstream & input, vector<string> & names){
		for (int i = 0; i < cnt; ++i) {
			if (arr[i].loadHead(input)) {
				//cout << "Head Readed [" << i << "]" << endl;
				headLen += arr[i].headLen();
				string name(arr[i].getName(), (unsigned long)arr[i].getNameLen());
				//cout << name << endl;
				for (int j = 0; j < (Int)names.size(); j++){
					if ( name == names[j] ){
						//cout << "False Name" << endl;
						return false;
					}
				}
				names.push_back(name);
			} else {
				return false;
			}
		}
		//cout << "Delka: " << names.size() << endl;
		return true;
	}
	bool readData(fstream & input){
		for (int i = 0; i < cnt; i++){
			//cout << i << endl;
			bool readSucc = false;
			for (int j = 0; j < cnt; j++){
				//cout << headLen + dataLen << " = " << arr[j].getOffset() << endl;
				if (arr[j].getOffset() == (headLen + dataLen)){
					//cout << "Found" << endl;
					readSucc = true;
					dataLen += arr[j].getDataLen();
					if (!arr[j].loadData(input)) {
						return false;
					}
					break;
				}
			}
			if (!readSucc){
				return false;
			}
		}
		return true;
	}
	bool writeHead(fstream & output, Int offsetPlus){
		for (int i = 0; i < cnt; ++i) {
			if (!arr[i].writeHead(output, offsetPlus))
				return false;
		}
		qsort(arr, cnt, sizeof(*arr), compare);
		return true;
	}
	bool writeData(fstream & output){
		for (int i = 0; i < cnt; ++i) {
			if (!arr[i].writeData(output))
				return false;
		}
		return true;
	}
	Int getCount() { return cnt; }
	Int getHeadLen() { return headLen; }
	Int getDataLen() { return dataLen; }
	~CFile(){
		if (arr != NULL)
			delete [] arr;
	}
private:
	Int cnt, headLen, dataLen;
	CSymbol * arr;
};
/*=====================================HANDLE=READING===================================*/
bool handleReading(fstream & file, CFile & cfile, vector<string> & names){
	char tmp;
	//cout << "File 01: reading header!" << endl;
	if (!cfile.readHeader(file, names))
		return false;
	//cout << "File 01: reading data!" << endl;
	if (!cfile.readData(file))
		return false;
	if (file.read(&tmp, 1))
		return false;
	file.close();
	file.clear();
	return true;
}
/*=======================================LINK=FILES=====================================*/
bool linkFiles(const char *srcFile1, const char *srcFile2, const char *dstFile) {
	fstream file; // FSTREAM - handles opening file and reading/writing
	vector<string> names; // list of names
	/**************************************************************************************/
	file.open(srcFile1, ios::in | ios::binary);//opens first file
	if (!file.is_open() || !file.good())// checks if it is opened
		return false;
	CFile file01(file);// declaration of new CFile
	if (!handleReading(file, file01, names))
		return false;
	/**************************************************************************************/
	file.open(srcFile2, ios::in | ios::binary);
	if (!file.is_open() || !file.good())// checks if it is opened
		return false;
	CFile file02(file);// declaration of new CFile
	if (!handleReading(file, file02, names))
		return false;
	/**************************************************************************************/
	/*for (int i = 0; i < (Int)names.size(); ++i) {
		cout << names[i] << endl;
	}*/
	file.open(dstFile, ios::out | ios::binary); // opens dest file
	if (!file.is_open() || !file.good())// is it really open
		return false;
	/* Because it is needed to combine both files together, you need to make sum of both
	 * count of symbols*/
	Int newCnt = file01.getCount() + file02.getCount();
	file.write((char *)&newCnt, sizeof(newCnt));
	//cout << "Dst File: writing head!" << endl;
	file01.writeHead(file, file02.getHeadLen() - 4);
	file02.writeHead(file, file01.getHeadLen() + file01.getDataLen() - 4);
	//cout << "Dst File: writing data!" << endl;
	file01.writeData(file);
	file02.writeData(file);
	if (!file.good())
		return false;
	file.close();
	return true;
}
#ifndef __PROGTEST__
string getFileString(fstream & file){
	string toReturn;
	while (!file.eof()){
		toReturn += file.get();
	}
	return toReturn;
}

bool identicalContents(const char *fileName1, const char *fileName2) {
	fstream file01, file02;
	string str1, str2;
	//cout << "Comparing" << endl;
	file01.open(fileName1,ios::in | ios::binary);
	file02.open(fileName2,ios::in | ios::binary);
	str1 = getFileString(file01);
	str2 = getFileString(file02);
	//cout << str1 << endl << str2 << endl;
	return str1 == str2;
}

int main(void) {
	assert (linkFiles("in1_00.obj", "in2_00.obj", "out_00.obj")
	        && identicalContents("out_00.obj", "ref_00.obj"));

	assert (linkFiles("in1_01.obj", "in2_01.obj", "out_01.obj")
	        && identicalContents("out_01.obj", "ref_01.obj"));

	assert (!linkFiles("in1_02.obj", "in2_02.obj", "out_02.obj"));

	return 0;
}

#endif /* __PROGTEST__ */
