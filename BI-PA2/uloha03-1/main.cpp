#ifndef __PROGTEST__
#include <cstdio>
#include <ctime>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
using namespace std;
#endif /* __PROGTEST__ */

class InvalidDateException
{
public:
	InvalidDateException(string & err):error(err){ }
private:
	string error;
};

//=================================================================================================
// date_format manipulator - a dummy implementation. Keep this code unless you implement your
// own working manipulator.
ios_base & dummy_date_format_manipulator ( ios_base & x )
{
	return x;
}

ios_base & ( * ( date_format ( const char * fmt ) ) ) ( ios_base & x )
{
	return dummy_date_format_manipulator;
}
//=================================================================================================
class CDate
{
public:
	// constructor ( y,m,d )
	CDate(int y, int m, int d): y(y), m(m), d(d){
		if (!validateDate(y, m, d))
			throw "InvalidDateException";
	}
	CDate(CDate & a){
		y = a.y;
		m = a.m;
		d = a.d;
	}
	CDate(const CDate &a){
		y = a.y;
		m = a.m;
		d = a.d;
	}
	// operators ++, --, both prefix and postfix
	CDate & operator= (const CDate &b){
		y = b.y;
		m = b.m;
		d = b.d;
		return *this;
	}
	CDate operator++(){
		d++;
		if (d >= 28)
			updateDate();
		return *this;
	}
	CDate operator++(int a){
		CDate res(*this);
		if (a == 0)
			d++;
		if (d>=28)
			updateDate();
		return res;
	}
	CDate operator--(){
		d--;
		if (d < 1)
			updateDate();
		return *this;
	}
	CDate operator--(int a){
		CDate res(*this);
		if (a == 0)
			d--;
		if (d < 1)
			updateDate();
		return res;
	}
	// operators ==, !=, <=, <, >=, >
	bool operator== (const CDate &a) const{
		return y == a.y && m == a.m && d == a.d;
	}
	bool operator!= (const CDate &a) const{
		return y != a.y || m != a.m || d != a.d;
	}
	bool operator> (const CDate &a) const{
		if (y > a.y)
			return true;
		else if (y == a.y && m > a.m)
			return true;
		else if (y == a.y && m == a.m && d > a.d)
			return true;
		else
			return false;
	}
	friend bool operator>= (const CDate &a, const CDate &b){
		return (a > b || a == b);
	}
	friend bool operator< (const CDate &a, const CDate &b){
		return !(a >= b);
	}
	friend bool operator<= (const CDate &a, const CDate &b){
		return !(a>b);
	}
	// operator + int
	friend CDate operator+ (const CDate & a, const int b){
		int c = b;
		CDate res(a);
		while (c != 0){
			res++;
			c--;
		}
		return res;
	}
	// operator - int
	friend CDate operator- (const CDate & a, const int b){
		int c = b;
		CDate res(a);
		while (c != 0){
			res--;
			c--;
		}
		return res;
	}
	// operator - CDate
	friend int operator- (const CDate & a, const CDate &b){
		int res = 0;
		if (a > b){
			CDate ref(b);
			while (a != ref){
				ref++;
				res++;
			}
		} else {
			CDate ref(a);
			while (b != ref){
				ref++;
				res++;
			}
		}
		return res;
	}

	// operators << and >>
	friend ostream & operator<<(ostream & out, CDate &a){
		out << a.y;
		out << '-';
		if (a.m < 10){
			out << '0';
			out << a.m;
		} else {
			out << a.m;
		}
		out << '-';
		if (a.d < 10){
			out << '0';
			out << a.d;
		} else {
			out << a.d;
		}
		return out;
	}
	friend istream & operator>>(istream & in, CDate &a){
		char dash;
		int y,m,d;
		in >> y;
		in >> dash;
		in >> m;
		in >> dash;
		in >> d;
		if (a.validateDate(y, m, d)) {
			a = CDate(y,m,d);
		} else {
			in.setstate(ios::failbit);
		}
		return in;
	}
private:
	int y, m, d;
	bool isLeap(int y){
		return ((y%4 == 0 && y%100 !=0) || y%400 == 0);
	}
	bool validateDate(int y, int m, int d){
		if (y < 0 || m < 1 || m > 12 || d < 1)
			return false;
		switch (m){
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				if (d > 31)
					return false;
				break;
			case 4:
			case 6:
			case 9:
			case 11:
				if (d > 30)
					return false;
				break;
			case 2:
				if (isLeap(y)){
					if (d > 29)
						return false;
				} else {
					if (d > 28)
						return false;
				}
				break;
			default:
				return false;
		}
		return true;
	}
	void updateDate(){
		//Day-handelling
		if (d < 1){
			m--;
			switch (m){
				case 0:
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
					d = 31;
					break;
				case 4:
				case 6:
				case 9:
				case 11:
					d = 30;
					break;
				case 2:
					if (isLeap(y)){
						d = 29;
					} else {
						d = 28;
					}
					break;
				default:
					break;
			}
		} else {
			switch (m) {
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					if (d > 31) {
						d -= 31;
						m++;
					}
					break;
				case 4:
				case 6:
				case 9:
				case 11:
					if (d > 30) {
						d -= 30;
						m++;
					}
					break;
				case 2:
					if (isLeap(y)) {
						if (d > 29) {
							d -= 29;
							m++;
						}
					} else {
						if (d > 28) {
							d -= 28;
							m++;
						}
					}
					break;
				default:
					break;
			}
		}
		//Month-Handelling
		if (m < 1){
			m += 12;
			y--;
		}
		if (m > 12){
			m-=12;
			y++;
		}
	}
};


#ifndef __PROGTEST__
int main (void){
	ostringstream oss;
	istringstream iss;
	CDate a ( 2000, 1, 2 );
	CDate b ( 2010, 2, 3 );
	CDate c ( 2004, 2, 10 );
	oss . str (string(""));
	oss << a;
	assert ( oss . str () == "2000-01-02" );
	oss . str ("");
	oss << b;
	assert ( oss . str () == "2010-02-03" );
	oss . str ("");
	oss << c;
	assert ( oss . str () == "2004-02-10" );
	a = a + 1500;
	oss . str ("");
	oss << a;
	assert ( oss . str () == "2004-02-10" );
	b = b - 2000;
	oss . str ("");
	oss << b;
	assert ( oss . str () == "2004-08-13" );
	assert ( b - a == 185 );
	assert ( ( b == a ) == false );
	assert ( ( b != a ) == true );
	assert ( ( b <= a ) == false );
	assert ( ( b < a ) == false );
	assert ( ( b >= a ) == true );
	assert ( ( b > a ) == true );
	assert ( ( c == a ) == true );
	assert ( ( c != a ) == false );
	assert ( ( c <= a ) == true );
	assert ( ( c < a ) == false );
	assert ( ( c >= a ) == true );
	assert ( ( c > a ) == false );
	a = ++c;
	oss . str ( "" );
	oss << a << " " << c;
	assert ( oss . str () == "2004-02-11 2004-02-11" );
	a = --c;
	oss . str ( "" );
	oss << a << " " << c;
	assert ( oss . str () == "2004-02-10 2004-02-10" );
	a = c++;
	oss . str ( "" );
	oss << a << " " << c;
	assert ( oss . str () == "2004-02-10 2004-02-11" );
	a = c--;
	oss . str ( "" );
	oss << a << " " << c;
	assert ( oss . str () == "2004-02-11 2004-02-10" );
	iss . clear ();
	iss . str ( "2015-09-03" );
	assert ( ( iss >> a ) );
	oss . str ("");
	oss << a;
	assert ( oss . str () == "2015-09-03" );
	a = a + 70;
	oss . str ("");
	oss << a;
	assert ( oss . str () == "2015-11-12" );

	CDate d ( 2000, 1, 1 );
	try
	{
		CDate e ( 2000, 32, 1 );
		assert ( "No exception thrown!" == NULL );
	}
	catch ( ... )
	{
	}
	iss . clear ();
	iss . str ( "2000-12-33" );
	assert ( ! ( iss >> d ) );
	oss . str ("");
	oss << d;
	assert ( oss . str () == "2000-01-01" );
	iss . clear ();
	iss . str ( "2000-11-31" );
	assert ( ! ( iss >> d ) );
	oss . str ("");
	oss << d;
	assert ( oss . str () == "2000-01-01" );
	iss . clear ();
	iss . str ( "2000-02-29" );
	assert ( ( iss >> d ) );
	oss . str ("");
	oss << d;
	assert ( oss . str () == "2000-02-29" );
	iss . clear ();
	iss . str ( "2001-02-29" );
	assert ( ! ( iss >> d ) );
	oss . str ("");
	oss << d;
	assert ( oss . str () == "2000-02-29" );
/*
//=============================================================================
// bonus tests
//=============================================================================

	CDate f ( 2000, 5, 12 );
	oss . str ("");
	oss << f;
	assert ( oss . str () == "2000-05-12" );
	oss . str ("");
	oss << date_format ( "%Y/%m/%d" ) << f;
	assert ( oss . str () == "2000/05/12" );
	oss . str ("");
	oss << date_format ( "%d.%m.%Y" ) << f;
	assert ( oss . str () == "12.05.2000" );
	oss . str ("");
	oss << date_format ( "%m/%d/%Y" ) << f;
	assert ( oss . str () == "05/12/2000" );
	oss . str ("");
	oss << date_format ( "%Y%m%d" ) << f;
	assert ( oss . str () == "20000512" );
	oss . str ("");
	oss << date_format ( "hello kitty" ) << f;
	assert ( oss . str () == "hello kitty" );
	oss . str ("");
	oss << date_format ( "%d%d%d%d%d%d%m%m%m%Y%Y%Y%%%%%%%%%%" ) << f;
	assert ( oss . str () == "121212121212050505200020002000%%%%%" );
	oss . str ("");
	oss << date_format ( "%Y-%m-%d" ) << f;
	assert ( oss . str () == "2000-05-12" );
	iss . clear ();
	iss . str ( "2001-01-02" );
	assert ( ( iss >> date_format ( "%Y-%m-%d" ) >> f ) );
	oss . str ("");
	oss << f;
	assert ( oss . str () == "2001-01-02" );
	iss . clear ();
	iss . str ( "05.06.2003" );
	assert ( ( iss >> date_format ( "%d.%m.%Y" ) >> f ) );
	oss . str ("");
	oss << f;
	assert ( oss . str () == "2003-06-05" );
	iss . clear ();
	iss . str ( "07/08/2004" );
	assert ( ( iss >> date_format ( "%m/%d/%Y" ) >> f ) );
	oss . str ("");
	oss << f;
	assert ( oss . str () == "2004-07-08" );
	iss . clear ();
	iss . str ( "2002*03*04" );
	assert ( ( iss >> date_format ( "%Y*%m*%d" ) >> f ) );
	oss . str ("");
	oss << f;
	assert ( oss . str () == "2002-03-04" );
	iss . clear ();
	iss . str ( "C++09format10PA22006rulez" );
	assert ( ( iss >> date_format ( "C++%mformat%dPA2%Yrulez" ) >> f ) );
	oss . str ("");
	oss << f;
	assert ( oss . str () == "2006-09-10" );
	iss . clear ();
	iss . str ( "%12%13%2010%" );
	assert ( ( iss >> date_format ( "%%%m%%%d%%%Y%%" ) >> f ) );
	oss . str ("");
	oss << f;
	assert ( oss . str () == "2010-12-13" );

	CDate g ( 2000, 6, 8 );
	iss . clear ();
	iss . str ( "2001-11-33" );
	assert ( ! ( iss >> date_format ( "%Y-%m-%d" ) >> g ) );
	oss . str ("");
	oss << g;
	assert ( oss . str () == "2000-06-08" );
	iss . clear ();
	iss . str ( "29.02.2003" );
	assert ( ! ( iss >> date_format ( "%d.%m.%Y" ) >> g ) );
	oss . str ("");
	oss << g;
	assert ( oss . str () == "2000-06-08" );
	iss . clear ();
	iss . str ( "14/02/2004" );
	assert ( ! ( iss >> date_format ( "%m/%d/%Y" ) >> g ) );
	oss . str ("");
	oss << g;
	assert ( oss . str () == "2000-06-08" );
	iss . clear ();
	iss . str ( "2002-03" );
	assert ( ! ( iss >> date_format ( "%Y-%m" ) >> g ) );
	oss . str ("");
	oss << g;
	assert ( oss . str () == "2000-06-08" );
	iss . clear ();
	iss . str ( "hello kitty" );
	assert ( ! ( iss >> date_format ( "hello kitty" ) >> g ) );
	oss . str ("");
	oss << g;
	assert ( oss . str () == "2000-06-08" );
	iss . clear ();
	iss . str ( "2005-07-12-07" );
	assert ( ! ( iss >> date_format ( "%Y-%m-%d-%m" ) >> g ) );
	oss . str ("");
	oss << g;
	assert ( oss . str () == "2000-06-08" );*/
	return 0;
}
#endif