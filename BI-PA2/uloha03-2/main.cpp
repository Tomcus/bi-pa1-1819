#ifndef __PROGTEST__

#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cmath>
#include <cfloat>
#include <cassert>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>

#if defined ( __cplusplus ) && __cplusplus > 199711L /* C++ 11 */

#include <memory>

#endif /* C++ 11 */
using namespace std;
#endif /* __PROGTEST__ */
const double epsilon = 1e-9;
bool doubleComp(double a, double b){
	return (abs(a-b) < epsilon);
}
class CPolynomial {
public:
	// default constructor
	CPolynomial(){
		degree = 0;
		polynoms = vector<double>(101, 0.0);

	}
	CPolynomial(CPolynomial &a){
		degree = a.degree;
		polynoms = a.polynoms;
		updateDegree();
		cleanRest();
	}
	CPolynomial(const CPolynomial &a){
		degree = a.degree;
		polynoms = a.polynoms;
		updateDegree();
		cleanRest();
	}
	// operator []
	double& operator[](const size_t i){
		while (i >= polynoms.capacity())
			resizePolynoms();
		if (i > degree)
			degree = i;
		return  polynoms[i];
	}
	double operator[](const size_t i) const{
		if (i > degree)
			return 0.0;
		return  polynoms[i];
	}
	// Degree (), returns unsigned value
	size_t Degree(){
		updateDegree();
		return degree;
	}
	size_t Degree() const{
		return degree;
	}
	// operator +
	CPolynomial operator+(CPolynomial &a) {
		CPolynomial res;
		size_t n = (Degree()>a.Degree())?Degree():a.Degree();
		for (size_t i = 0; i <= n; ++i) {
			res[i] = polynoms[i] + a[i];
		}
		res.updateDegree();
		res.cleanRest();
		return res;
	}
	CPolynomial operator+(const CPolynomial &a) const {
		CPolynomial res;
		size_t n = (degree>a.degree)?degree:a.degree;
		for (size_t i = 0; i <= n; ++i) {
			res[i] = polynoms[i] + a[i];
		}
		res.updateDegree();
		res.cleanRest();
		return res;
	}
	// operator -
	CPolynomial operator-(CPolynomial &a){
		CPolynomial res;
		size_t n = (Degree()>a.Degree())?Degree():a.Degree();
		for (size_t i = 0; i <= n; ++i) {
			res[i] = polynoms[i] - a[i];
		}
		return res;
		res.updateDegree();
		res.cleanRest();
	}
	CPolynomial operator-(const CPolynomial &a) const{
		CPolynomial res;
		size_t n = (degree>a.degree)?degree:a.degree;
		for (size_t i = 0; i <= n; ++i) {
			res[i] = polynoms[i] - a[i];
		}
		res.updateDegree();
		res.cleanRest();
		return res;
	}
	// operator * (polynomial, double)
	CPolynomial operator*(const double a){
		CPolynomial res;
		//cout << '(' << res << ')' << " * " << a << endl;
		for (int i = Degree(); i >= 0; --i) {
			//cout << i << ": " << res[i] << " * " << a << " ";
			res[i] = polynoms[i]*a;
			//cout << res[i] << endl;
		}
		//cout << "operator * double " << a << endl;
		if (doubleComp(a,0.0))
			res.degree = 0;
		res.updateDegree();
		return res;
	}
	CPolynomial operator*(const double a) const{
		CPolynomial res;
		//cout << '(' << res << ')' << " * " << a << endl;
		for (int i = degree; i >= 0; --i) {
			//cout << i << ": " << res[i] << " * " << a << " ";
			res[i] = polynoms[i]*a;
			//cout << res[i] << endl;
		}
		//cout << "operator * double " << a << endl;
		if (doubleComp(a,0.0))
			res.degree = 0;
		res.updateDegree();
		return res;
	}
	friend CPolynomial operator*(CPolynomial &a, CPolynomial &b){
		CPolynomial res;
		for (size_t i = 0; i <= a.Degree(); ++i) {
			for (size_t j = 0; j <= b.Degree(); ++j) {
				res[i+j] += a[i] * b[j];
			}
		}
		res.updateDegree();
		res.cleanRest();
		return res;
	}
	friend CPolynomial operator*(const CPolynomial &a, const CPolynomial &b){
		CPolynomial res;
		for (size_t i = 0; i <= a.degree; ++i) {
			for (size_t j = 0; j <= b.degree; ++j) {
				res[i+j] += a[i] * b[j];
			}
		}
		res.updateDegree();
		res.cleanRest();
		return res;
	}
	// operator ==
	friend bool operator==(CPolynomial &a, CPolynomial &b){
		size_t n = (a.Degree()>b.Degree())?a.Degree():b.Degree();
		for (size_t i = 0; i <= n; ++i){
			if (!doubleComp(a[i], b[i])){
				return false;
			}
		}
		return true;
	}
	friend bool operator==(const CPolynomial &a, const CPolynomial &b){
		size_t n = (a.degree>b.degree)?a.degree:b.degree;
		for (size_t i = 0; i <= n; ++i){
			if (!doubleComp(a[i], b[i])){
				return false;
			}
		}
		return true;
	}
	// operator !=
	friend bool operator!=(CPolynomial &a, CPolynomial &b){
		return !(a==b);
	}
	friend bool operator!=(const CPolynomial &a, const CPolynomial &b){
		return !(a==b);
	}
	// operator ()
	double operator()(const double n){
		updateDegree();
		double res = 0.0;
		for (size_t i = degree; i > 0; --i) {
			if (doubleComp(polynoms[i],0.0))
				continue;
			res += polynoms[i] * pow(n,i);
		}
		res += polynoms[0];
		return res;
	}
	double operator()(const double n) const{
		double res = 0.0;
		for (size_t i = degree; i > 0; --i) {
			if (doubleComp(polynoms[i],0.0))
				continue;
			res += polynoms[i] * pow(n,i);
		}
		res += polynoms[0];
		return res;
	}
	//operator <<
	friend ostream & operator<<(ostream &out, CPolynomial &a){
		bool first = true;
		for (size_t i = a.Degree(); i > 0; --i) {
			if (doubleComp(a[i],0.0))
				continue;
			if (first){
				if (a[i] < 0)
					out << "- ";
				first = false;
			} else {
				if (a[i] < 0.0)
					out << " - ";
				else
					out << " + ";
			}
			if (!doubleComp(abs(a[i]), 1.0))
				out << abs(a[i]) << '*';
			out << "x^" << i;
		}
		if (!doubleComp(a[0],0.0)){
			if (a[0] < 0.0)
				out << " - " << abs(a[0]);
			else
				out << " + " << a[0];
		} else if (first) {
			out << 0.0;
		}
		return out;
	}
	friend ostream & operator<<(ostream &out, const CPolynomial &a){
		bool first = true;
		for (size_t i = a.degree; i > 0; --i) {
			if (doubleComp(a[i],0.0))
				continue;
			if (first){
				if (a[i] < 0)
					out << "- ";
				first = false;
			} else {
				if (a[i] < 0.0)
					out << " - ";
				else
					out << " + ";
			}
			if (!doubleComp(abs(a[i]), 1.0))
				out << abs(a[i]) << '*';
			out << "x^" << i;
		}
		if (!doubleComp(a[0],0.0)){
			if (a[0] < 0.0)
				out << " - " << abs(a[0]);
			else
				out << " + " << a[0];
		} else if (first) {
			out << 0.0;
		}
		return out;
	}
	// operator =
	CPolynomial & operator= (const CPolynomial & a){
		size_t n = (degree>a.degree)?degree:a.degree;
		for (size_t i = 0; i <= n; ++i) {
			if (i >= polynoms.capacity())
				resizePolynoms();
			polynoms[i] = a.polynoms[i];
		}
		degree = a.degree;
		updateDegree();
		cleanRest();
		return *this;
	}
private:
	void resizePolynoms(){
		size_t oldSize, newSize;
		oldSize = polynoms.capacity();
		newSize = 2 * oldSize;
		polynoms.reserve(newSize);
		for (size_t i = oldSize; i < newSize; ++i) {
			polynoms [i] = 0.0;
		}
	}
	void updateDegree(bool debug = false){
		for (size_t i = degree; i > 0; --i) {
			if (!doubleComp(polynoms[i], 0.0)) {
				degree = (size_t)i;
				if (debug){
					cout << "update Degree:" << endl;
					for (size_t i = 0; i <= degree; i++){
						cout << polynoms[i] << ";";
					}
					cout << endl;
				}
				return;
			}
		}
		degree = 0;
	}
	void cleanRest(){
		for (size_t i = degree + 1; i < polynoms.size(); i++){
			polynoms[i] = 0.0;
		}
	}
	size_t degree;
	vector<double> polynoms;
};

#ifndef __PROGTEST__

bool smallDiff(double a, double b) {
	return doubleComp(a,b);
}

bool dumpMatch(const CPolynomial &x, const vector<double> &ref) {
	for (size_t i = 0; i < ref.capacity(); ++i){
		if (!smallDiff(x[i], ref[i]))
			return false;
	}
	return true;
}

int main(void) {
	CPolynomial a, b, c;
	ostringstream out;

	a[0] = -10;
	a[1] = 3.5;
	a[3] = 1;
	assert (smallDiff(a(2), 5));
	out.str("");
	out << a;
	assert (out.str() == "x^3 + 3.5*x^1 - 10");
	a = a * -2;
	assert (a.Degree() == 3
	        && dumpMatch(a, vector<double>{20.0, -7.0, -0.0, -2.0}));

	out.str("");
	out << a;
	assert (out.str() == "- 2*x^3 - 7*x^1 + 20");
	out.str("");
	out << b;
	assert (out.str() == "0");
	b[5] = -1;
	out.str("");
	out << b;
	assert (out.str() == "- x^5");
	c = a + b;
	assert (c.Degree() == 5
	        && dumpMatch(c, vector<double>{20.0, -7.0, 0.0, -2.0, 0.0, -1.0}));

	out.str("");
	out << c;
	assert (out.str() == "- x^5 - 2*x^3 - 7*x^1 + 20");
	c = a - b;
	assert (c.Degree() == 5
	        && dumpMatch(c, vector<double>{20.0, -7.0, -0.0, -2.0, -0.0, 1.0}));

	out.str("");
	out << c;
	assert (out.str() == "x^5 - 2*x^3 - 7*x^1 + 20");
	c = a * b;
	assert (c.Degree() == 8
	        && dumpMatch(c, vector<double>{-0.0, -0.0, 0.0, -0.0, 0.0, -20.0, 7.0, -0.0,
	                                       2.0}));

	out.str("");
	out << c;
	assert (out.str() == "2*x^8 + 7*x^6 - 20*x^5");
	assert (a != b);
	b[5] = 0;
	assert (!(a == b));
	a = a * 0;
	assert (a.Degree() == 0
	        && dumpMatch(a, vector<double>{0.0}));
	assert (a == b);
	return 0;
}

#endif /* __PROGTEST__ */
