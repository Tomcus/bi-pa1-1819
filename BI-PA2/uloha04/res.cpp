//
// Created by vybirto1 on 19.4.16.
//
/*======================================================================================*/
//HEADERS
/*======================================================================================*/
#ifndef __PROGTEST__
#include <cassert>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <stdint.h>
#endif
using namespace std;

const int startingDataSize = 1024;
const int resizeMultiply = 2;

class CData {
public:
	CData( size_t len );
	CData ( CData & a);
	CData ( CData & a, const uint32_t offset );
	~CData( void );
	uint32_t Write( const uint8_t * src, uint32_t bytes, size_t at );
	uint32_t Read ( uint8_t * dst, uint32_t bytes, size_t at );
	void copy( void );
	void deCopy( void );
	int getRef( void );
	size_t getLen( void );
	size_t getSize( void );
	void writeData( void );
private:
	void resize( void );
	int references;
	size_t len; // actual size
	size_t size; // fake size
	uint8_t * data;
};

class CFile {
public:
	CFile ( void );
	CFile ( const CFile &, bool recurs = true );
	~CFile();
	CFile & operator=(const CFile &);
	// copy cons, dtor, op=
	bool Seek ( uint32_t offset );
	uint32_t Read ( uint8_t * dst, uint32_t bytes );
	uint32_t Write ( const uint8_t * src, uint32_t bytes );
	void Truncate ( void );
	uint32_t FileSize ( void ) const;
	void AddVersion ( void );
	bool UndoVersion ( void );
	void writeData( void );
	size_t getSize( void );
private:
	void deleteData( void );
	void makeRecursiveCopy( void );
	void recursCopy( void );
	CFile * prev;
	CData * data;
	size_t readWriteIndex;
};
/*======================================================================================*/
//IMPLEMETATION
/*======================================================================================*/
/**
 * CDATA
 **/
CData::CData( size_t len ):len(len) {
	references = 1;
	data = new uint8_t[len];
	size = 0;
}
CData::CData(CData &a) {
	references = 1;
	data = new uint8_t[a.getLen()];
	len = a.getLen();
	size = a.size;
	memcpy(data, a.data, size);
}
CData::CData( CData & a, const uint32_t offset) {
	//a.references--;
	references = 1;
	data = new uint8_t[a.getLen()];
	len = a.getLen();
	size = offset;
	memcpy(data, a.data, a.getSize() - offset);
}
CData::~CData( void ) {
	delete [] data;
}
void CData::resize( void ) {
	uint8_t *newData = new uint8_t[resizeMultiply * len];
	memcpy(newData, data, len);
	len *= resizeMultiply;
	delete [] data;
	data = newData;
}
void CData::copy( void ) {
	references++;
}
void CData::deCopy( void ) {
	references--;
}
int CData::getRef( void ) {
	return references;
}
size_t CData::getSize( void ) {
	return size;
}
size_t CData::getLen( void ) {
	return len;
}
uint32_t CData::Write( const uint8_t *src, uint32_t bytes, size_t at ) {
	uint32_t res = 0;
	for (size_t i = 0; i < bytes; ++i) {
		if (at + i > len)
			resize();
		data [at + i] = src[i];
		res ++;
	}
	if (size < (at+res))
		size = at + res;
	return res;
}
uint32_t CData::Read( uint8_t *dst, uint32_t bytes, size_t at ) {
	uint32_t res = 0;
	for (size_t i = 0; i < bytes; ++i){
		if (at + i >= size)
			break;
		dst[i] = data[i + at];
		res++;
	}
	return res;
}
void CData::writeData() {
	for (size_t i = 0; i < size; ++i) {
		cout << (int)data[i] << ", ";
	}
	cout << endl;
}
/****************************************************************************************/
/**
 * CFILE
 **/
CFile::CFile( void ) {
	data = NULL;
	prev = NULL;
	readWriteIndex = 0;
}
CFile::CFile( const CFile &a, bool recurs ) {
	prev = a.prev;
	data = a.data;
	readWriteIndex = a.readWriteIndex;
	if (data != NULL)
		data->copy();
	if (recurs){
		recursCopy();
	}
}
CFile::~CFile() {
	if (data != NULL)
		deleteData();
	if (prev != NULL)
		delete prev;
}
CFile & CFile::operator=( const CFile &a ) {
	if (data != NULL)
		deleteData();
	if (prev != NULL)
		delete prev;
	data = a.data;
	prev = a.prev;
	if (data != NULL)
		data->copy();
	if (prev != NULL)
		recursCopy();
	return *this;
}
void CFile::AddVersion( void ) {
	prev = new CFile(*this, false);
}
uint32_t CFile::FileSize() const {
	if (data != NULL)
		return data->getSize();
	return 0;
}
bool CFile::Seek( uint32_t offset ) {
	if (data == NULL && offset == 0)
		return true;
	else if (data == NULL && offset != 0)
		return false;
	else if ( offset <= data->getSize()){
		readWriteIndex = offset;
		return true;
	} else {
		return false;
	}
}
uint32_t CFile::Read( uint8_t *dst, uint32_t bytes ) {
	uint32_t plus;
	if (data == NULL)
		return 0;
	plus = data->Read(dst, bytes, readWriteIndex);
	readWriteIndex += plus;
	return plus;
}
uint32_t CFile::Write( const uint8_t *src, uint32_t bytes ) {
	uint32_t res = 0;
	if (data == NULL){
		data = new CData(startingDataSize);
	}
	if (data->getRef() > 1) {
		data->deCopy();
		data = new CData(*data);
	}
	res = data->Write(src, bytes, readWriteIndex);
	readWriteIndex += res;
	return res;
}
void CFile::Truncate( void ) {
	if (data == NULL)
		return;
	CData * newData;
	newData = new CData(*data, (uint32_t)readWriteIndex);
	deleteData();
	data = newData;
}
void CFile::deleteData() {
	if (data->getRef() > 1){
		data->deCopy();
	} else {
		delete data;
	}
}
void CFile::recursCopy() {
	if (prev == NULL)
		return;
	CFile * oldPrev;
	oldPrev = prev;
	prev = new CFile();
	prev->prev = oldPrev->prev;
	prev->data = oldPrev->data;
	prev->readWriteIndex = oldPrev->readWriteIndex;
	if (prev->data != NULL)
		prev->data->copy();
	prev->recursCopy();
}
bool CFile::UndoVersion() {
	CFile * oldPrev;
	if (prev == NULL)
		return false;
	if (data != NULL)
		deleteData();
	oldPrev = prev;
	readWriteIndex = prev->readWriteIndex;
	data = prev->data;
	prev = prev->prev;
	oldPrev->prev = NULL;
	oldPrev->data = NULL;
	delete oldPrev;
	return true;
}
void CFile::writeData() {
	data->writeData();
}
size_t CFile::getSize() {
	return data->getSize();
}
