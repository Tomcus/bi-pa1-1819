//
// Created by vybirto1 on 7.5.16.
//
class NoRouteException
{
};

template <typename _T, typename _E>
class CRoute
{
public:
	// default constructor
	CRoute(){}
	// destructor
	~CRoute(){}
	// Add
	CRoute & Add(const _T & a, const _T & b, const _E & e){
		auto wpItA = worldMap.find(a);
		if (wpItA == worldMap.end()){
			multimap<const _T, const _E> mltM;
			mltM.insert(make_pair(_T(b), _E(e)));
			worldMap.insert(typename map<const _T,multimap<const _T, const _E>>::value_type(_T(a),mltM));
		} else {
			wpItA->second.insert(make_pair(_T(b),_E(e)));
		}
		auto wpItB = worldMap.find(b);
		if (wpItB == worldMap.end()){
			multimap<const _T, const _E> mltM;
			mltM.insert(make_pair(_T(a), _E(e)));
			worldMap.insert(typename map<const _T,multimap<const _T, const _E>>::value_type(_T(b),mltM));
		} else {
			wpItB->second.insert(make_pair(_T(a),_E(e)));
		}
		return *this;
	}
	list<_T> Find(const _T & begin, const _T & end) const{
		list<_T> res;
		queue<_T> myQueue;
		map<_T, _T> wayBack;
		bool foundRoute = false;
		myQueue.push(_T(begin));
		while (!myQueue.empty()){
			_T toDo (myQueue.front());
			if (toDo == end){
				foundRoute = true;
				break;
			}
			auto wmIt = worldMap.find(toDo);
			if (wmIt != worldMap.end())
				for (auto i = wmIt->second.begin(); i != wmIt->second.end(); ++i) {
					if ( (*i).first == begin )
						continue;
					if ( wayBack.find(i->first) != wayBack.end() )
						continue;
					myQueue.push(_T(i->first));
					wayBack.insert(make_pair(_T(i->first),toDo));
				}
			myQueue.pop();
		}
		if (!foundRoute){
			//cesta nenalezena
			throw NoRouteException();
		}
		if (begin == end){
			res.push_back(_T(begin));
			return res;
		}
		_T toAdd = _T(end);
		while (toAdd != begin){
			res.push_front(toAdd);
			toAdd = wayBack.find(toAdd)->second;
		}
		res.push_front(begin);
		return res;
	}
	// Find (with optional filter)
	template <typename f>
	list<_T> Find(const _T & begin, const _T & end, const f & func) const{
		list<_T> res;
		queue<_T> myQueue;
		map<_T, _T> wayBack;
		bool foundRoute = false;
		myQueue.push(_T(begin));
		while (!myQueue.empty()){
			_T toDo(myQueue.front());
			if (toDo == end){
				foundRoute = true;
				break;
			}
			auto wmIt = worldMap.find(toDo);
			if (wmIt != worldMap.end())
				for (auto i = wmIt->second.begin(); i != wmIt->second.end(); ++i) {
					/*Pokud se prvek rovna zacatku*/
					if ( i->first == begin )
						continue;
					/*Pokud jiz byl prvek navstiven*/
					if ( wayBack.find(i->first) != wayBack.end() )
						continue;
					if (!func(i->second))
						continue;
					myQueue.push(_T(i->first));
					wayBack.insert(make_pair(_T(i->first),toDo));
				}
			myQueue.pop();
		}
		if (!foundRoute) {
			//cesta nenalezena
			throw NoRouteException();
		}
		if (begin == end){
			res.push_back(_T(begin));
			return res;
		}
		_T toAdd(end);
		while (toAdd != begin){
			res.push_front(toAdd);
			toAdd = wayBack.find(toAdd)->second;
		}
		res.push_front(begin);
		return res;
	}
protected:
	map<const _T, multimap<const _T, const _E>> worldMap;
};