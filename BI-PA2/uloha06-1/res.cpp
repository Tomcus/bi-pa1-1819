//
// Created by vybirto1 on 13.5.16.
//
#ifndef __PROGTEST__
class CSyntaxException : public exception
{
public:
	CSyntaxException ( const string & desc )
			: m_Desc ( desc ) { }
	virtual const char * what ( void ) const noexcept {
		return m_Desc . c_str ();
	}
private:
	string  m_Desc;
};
#endif
class CDataType{
public:
	CDataType(){}
	virtual ~CDataType(){}
	virtual size_t Sizeof() const{ return size; }
	virtual bool operator==(const CDataType & a) const {
		return typeName == a.typeName;
	}
	virtual bool operator!=(const CDataType & a) const {
		return typeName != a.typeName;
	}
	virtual ostream & print(ostream & os, size_t tabIndent = 0) const{
		for (size_t i = 0; i < tabIndent; ++i) {
			os << "\t";
		}
		os << typeName;
		return os;
	}
	friend ostream & operator<<(ostream & os, const CDataType & a){
		a.print(os);
		return os;
	}
	virtual string getType() const { return typeName; }
protected:
	size_t size;
	string typeName;
};
class CDataTypeInt: public CDataType
{
public:
	// default constructor
	CDataTypeInt():CDataType(){
		size = 4;
		typeName = "int";
	}
	CDataTypeInt( const CDataTypeInt & a ):CDataType(){
		size = a.size;
		typeName = a.typeName;
	}
};

class CDataTypeDouble: public CDataType
{
public:
	// default constructor
	CDataTypeDouble():CDataType(){
		size = 8;
		typeName = "double";
	}
	CDataTypeDouble( const CDataTypeDouble & a ):CDataType(){
		size = a.size;
		typeName = a.typeName;
	}
};

class CDataTypeEnum: public CDataType
{
public:
	// default constructor
	CDataTypeEnum():CDataType(){
		size = 4;
		typeName = "enum";
	}
	CDataTypeEnum(const CDataTypeEnum & a):CDataType(){
		size = a.size;
		typeName = a.typeName;
		for (size_t i = 0; i < a.types.size(); ++i)
			types.push_back(a.types[i]);
	}
	// Add ( )
	CDataTypeEnum & Add(const string & str){
		for (size_t i = 0; i < types.size(); ++i){
			if (types[i] == str)
				throw CSyntaxException("Duplicate enum value: " + str);
		}
		types.push_back(str);
		return *this;
	}
	// operator ==
	bool compareWithEnum(const CDataTypeEnum & a) const{
		if (types.size() != a.types.size())
			return false;
		for (size_t i = 0; i < types.size(); ++i) {
			if (types[i] != a.types[i])
				return false;
		}
		return true;
	}
	virtual bool operator==(const CDataType & a) const{
		if (typeName == a.getType())
			return compareWithEnum(dynamic_cast<const CDataTypeEnum &>(a));
		return false;
	}
	// operator !=
	virtual bool operator!=(const CDataType & a) const{
		if (typeName == a.getType())
			return !compareWithEnum(dynamic_cast<const CDataTypeEnum &>(a));
		return true;
	}
	virtual ostream & print(ostream & os, size_t tabIndent = 0) const{
		for (size_t i = 0; i < tabIndent; ++i) {
			os << "\t";
		}
		os << "enum" << endl;
		for (size_t i = 0; i < tabIndent; ++i) {
			os << "\t";
		}
		os << "{" << endl;
		for (size_t j = 0; j < types.size(); ++j) {
			for (size_t i = 0; i < tabIndent + 1; ++i) {
				os << "\t";
			}
			os << types[j];
			if (j != types.size() - 1)
				os << ",";
			os << endl;
		}
		for (size_t i = 0; i < tabIndent; ++i) {
			os << "\t";
		}
		os << "}";
		return os;
	}
	// operator <<
protected:
	vector<string> types;
};

class CDataTypeStruct : public CDataType
{
public:
	// default constructor
	CDataTypeStruct():CDataType(){
		typeName = "struct";
	}
	CDataTypeStruct & AddField(const string & name, const CDataType & dataType){
		for (size_t i = 0; i < fields.size(); ++i) {
			if (fields[i].first == name)
				throw CSyntaxException("Duplicate field: " + name);
		}
		CDataType * toInsert = NULL;
		if (dataType.getType() == "int")
			toInsert = new CDataTypeInt(dynamic_cast<const CDataTypeInt &>(dataType));
		else if (dataType.getType() == "double")
			toInsert = new CDataTypeDouble(dynamic_cast<const CDataTypeDouble &>(dataType));
		else if (dataType.getType() == "enum")
			toInsert = new CDataTypeEnum(dynamic_cast<const CDataTypeEnum &>(dataType));
		else if (dataType.getType() == "struct")
			toInsert = new CDataTypeStruct(dynamic_cast<const CDataTypeStruct &>(dataType));
		fields.push_back(make_pair(name, toInsert));
		return *this;
	}
	CDataTypeStruct(const CDataTypeStruct & a):CDataType(){
		typeName = a.typeName;
		for (size_t i = 0; i < a.fields.size(); ++i){
			AddField(a.fields[i].first, *(a.fields[i].second));
		}
	}
	virtual ~CDataTypeStruct(){
		for (size_t i = 0; i < fields.size(); ++i) {
			delete fields[i].second;
		}
	}
	// Field ()
	const CDataType & Field(const string & name) const{
		for (size_t i = 0; i < fields.size(); ++i) {
			if (fields[i].first == name)
				return *(fields[i].second);
		}
		throw CSyntaxException("Unknown field: " + name);
	}
	// Sizeof () -> size_t
	virtual size_t Sizeof( void ) const {
		size_t res = 0;
		for (size_t i = 0; i < fields.size(); ++i) {
			res += fields[i].second->Sizeof();
		}
		return res;
	}
	// operator ==
	bool compareWithStruct(const CDataTypeStruct & a) const{
		if (fields.size() != a.fields.size())
			return false;
		for (size_t i = 0; i < fields.size(); ++i) {
			if (*(fields[i].second) != *(a.fields[i].second))
				return false;
		}
		return true;
	}
	virtual bool operator== (const CDataType & a) const{
		if (typeName == a.getType()){
			return compareWithStruct(dynamic_cast<const CDataTypeStruct &>(a));
		}
		return false;
	}
	// operator !=
	virtual bool operator!= (const CDataType & a) const{
		if (typeName == a.getType())
			return !compareWithStruct(dynamic_cast<const CDataTypeStruct &>(a));
		return true;
	}
	// operator <<
	virtual ostream & print(ostream & os, size_t tabIndent = 0) const{
		for (size_t i = 0; i < tabIndent; ++i) {
			os << "\t";
		}
		os << "struct" << endl;
		for (size_t i = 0; i < tabIndent; ++i) {
			os << "\t";
		}
		os << "{" << endl;
		for (size_t i = 0; i < fields.size(); ++i) {
			fields[i].second->print(os, tabIndent + 1) << " " << fields[i].first << ";" << endl;
		}
		for (size_t i = 0; i < tabIndent; ++i) {
			os << "\t";
		}
		os << "}";
		return os;
	}
protected:
	vector<pair<string, const CDataType *>> fields;
};


