package org.vybirto1.pjv.termWork;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import org.vybirto1.pjv.termWork.data.AssetLoader;
import org.vybirto1.pjv.termWork.data.entity.Player;
import org.vybirto1.pjv.termWork.graphics.GameScene;

import java.util.Random;


public class Main extends Application {

	private static String[] titles = {"Adventure of Barbar Cohen in quest for new trousers"};

	private static String getTitle() {
		Random rng = new Random();
		return titles[rng.nextInt(titles.length)];
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		AssetLoader.loadAssets();
		Player player = new Player();
		GameScene gs = new GameScene(player);
		gs.reload(840, 480);
		Scene scene = new Scene(gs);
		scene.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
			gs.getKeyInput(event.getCode());
			event.consume();
		});
		addResizingToScene(gs, scene);
		primaryStage.setTitle(getTitle());
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	private void addResizingToScene(GameScene gs, Scene scene){
		scene.widthProperty().addListener(((observable, oldValue, newValue) ->
				gs.reload(newValue.intValue(), (int)scene.getHeight())));
		scene.heightProperty().addListener(((observable, oldValue, newValue) ->
				gs.reload((int)scene.getWidth(), newValue.intValue())));
	}

	public static void main(String[] args) {
		launch(args);
	}
}
