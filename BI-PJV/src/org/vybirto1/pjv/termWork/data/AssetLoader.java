package org.vybirto1.pjv.termWork.data;

import javafx.scene.image.Image;
import org.vybirto1.pjv.termWork.data.common.OnLoad;
import org.vybirto1.pjv.termWork.data.entity.EntityData;
import org.vybirto1.pjv.termWork.data.item.Item;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Dictionary;
import java.util.Hashtable;
import static org.vybirto1.pjv.termWork.graphics.LevelScene.TILE_SIZE;

public class AssetLoader {
	private static final Dictionary<String, Item> itemDictionary = new Hashtable<>();
	private static final Dictionary<String, Image> imageDictionary = new Hashtable<>();
	private static final Dictionary<String, EntityData> entityDataDictionary = new Hashtable<>();

	public static void loadAssets() {
		final String basePath = System.getProperty("user.dir") + "/data";
		// load images
		load((f) -> {
			String name = f.getName();
			name = name.substring(0, name.length() - 4);
			try {
				// todo: scale images to fit tile size
				imageDictionary.put(name, new Image(new FileInputStream(f)));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}, basePath + "/img");
		// load items
		load(f -> {
			final Item newItem = new Item(f);
			itemDictionary.put(newItem.getId(), newItem);
		}, basePath + "/items");
		// load entity definitions
		load(f -> {
			final EntityData entityData = new EntityData(f);
			entityDataDictionary.put(entityData.getId(), entityData);
		}, basePath + "/enemies");
	}

	public static Item getItem(final String id){
		return itemDictionary.get(id);
	}

	public static Image getImage(final String id) {
		return imageDictionary.get(id);
	}

	public static EntityData getEntityData(final String id) {
		return entityDataDictionary.get(id);
	}

	private static void load(final OnLoad onLoad, final String path) {
		File assetDir = new File(path);
		if (assetDir.isDirectory()) {
			File[] files = assetDir.listFiles();
			if (files != null)
				for (File folderContent : files) {
					if (folderContent != null && !folderContent.isDirectory()) {
						onLoad.onLoad(folderContent);
					}
				}
		}
	}
}
