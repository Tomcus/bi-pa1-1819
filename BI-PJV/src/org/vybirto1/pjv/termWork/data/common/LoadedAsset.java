package org.vybirto1.pjv.termWork.data.common;

import java.io.File;

public interface LoadedAsset {
	String load(final File asset);
}
