package org.vybirto1.pjv.termWork.data.common;

import java.io.File;

public interface OnLoad {
	void onLoad(final File f);
}
