package org.vybirto1.pjv.termWork.data.entity;

import org.vybirto1.pjv.termWork.data.level.Level;
import org.vybirto1.pjv.termWork.data.level.Tile;

import java.util.Random;
import java.util.Set;

public class Enemy extends MovableEntity {
	private final EntityData entityData;
	public Enemy(final Tile t, final Level l, final EntityData ed) {
		super(t, l);
		entityData = ed;
		hitPoints = ed.getMaxHP();
	}

	@Override
	protected int getDamage() {
		return rng.nextInt(entityData.getMaxDamage() - entityData.getMinDamage()) + entityData.getMinDamage();
	}

	@Override
	protected int getArmor() {
		return entityData.getArmor();
	}

	public EntityData getEntityData() {
		return entityData;
	}

	public void doAI(final Set<Tile> visibleTiles) {
		int diffX = rng.nextInt(3) - 1;
		int diffY = rng.nextInt(3) - 1;
		move(level.get(tile.getX() + diffX, tile.getY() + diffY));
	}
}
