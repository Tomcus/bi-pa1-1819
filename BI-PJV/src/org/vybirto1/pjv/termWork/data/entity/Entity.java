package org.vybirto1.pjv.termWork.data.entity;

import org.vybirto1.pjv.termWork.data.level.Tile;
import org.vybirto1.pjv.termWork.logic.IInteraction;

import java.util.Random;

public abstract class Entity {
	protected Tile tile;
	protected IInteraction onInteraction;
	protected static Random rng = new Random();

	public Tile getTile() {
		return tile;
	}

	public Entity(final Tile t) {
		this.tile = t;
	}
}
