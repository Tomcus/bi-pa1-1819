package org.vybirto1.pjv.termWork.data.entity;

import org.json.JSONObject;
import org.vybirto1.pjv.termWork.data.common.LoadedAsset;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class EntityData implements LoadedAsset {
	private final String id;
	private final String name;
	private final int minDamage;
	private final int maxDamage;
	private final int armor;
	private final int maxHP;
	private final int visionDistance;
	private final int speed;

	public EntityData(final File assetFile){
		final String fileContent = load(assetFile);
		if (fileContent != null){
			final JSONObject obj = new JSONObject(fileContent);
			id = obj.getString("id");
			name = obj.getString("name");
			minDamage = obj.getInt("minDamage");
			maxDamage = obj.getInt("maxDamage");
			armor = obj.getInt("armor");
			maxHP = obj.getInt("maxHP");
			visionDistance = obj.getInt("vision");
			speed = obj.getInt("speed");
		} else {
			throw new IllegalArgumentException("Unable to read file content for :" + assetFile.getName());
		}
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getMinDamage() {
		return minDamage;
	}

	public int getMaxDamage() {
		return maxDamage;
	}

	public int getArmor() {
		return armor;
	}

	public int getMaxHP() {
		return maxHP;
	}

	public int getVisionDistance() {
		return visionDistance;
	}

	public int getSpeed() {
		return speed;
	}

	@Override
	public String load(File asset) {
		String content = null;
		try {
			content = new Scanner(asset).useDelimiter("\\Z").next();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return content;
	}
}
