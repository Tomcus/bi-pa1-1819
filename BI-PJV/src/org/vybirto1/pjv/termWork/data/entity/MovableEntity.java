package org.vybirto1.pjv.termWork.data.entity;

import org.vybirto1.pjv.termWork.data.level.Level;
import org.vybirto1.pjv.termWork.data.level.Tile;

public abstract class MovableEntity extends Entity {

	protected Level level;
	protected int hitPoints;

	protected MovableEntity(){
		this (null, null);
	}

	public MovableEntity(final Tile t, final Level l) {
		super(t);
		this.level = l;
		onInteraction = (e) -> {
			int damage = e.getDamage() - getArmor();
			if (damage > 0){
				hitPoints -= damage;
				if (hitPoints <= 0){
					tile.setEntity(null);
				}
			}
		};
	}

	public void move(final Tile newTile) {
		if (Tile.distance(tile, newTile) < 2 && newTile.isPassable()) {
			if (newTile.getEntity() == null) {
				tile.setEntity(null);
				if (tile.getVisible())
					tile.doUpdate();
				newTile.setEntity(this);
				tile = newTile;
				if (tile.getVisible())
					tile.doUpdate();
			} else {
				if (newTile.getEntity().onInteraction != null)
					newTile.getEntity().onInteraction.interact(this);
			}
		}
	}

	protected abstract int getDamage();
	protected abstract int getArmor();

	public int getHitPoints() {
		return hitPoints;
	}
}
