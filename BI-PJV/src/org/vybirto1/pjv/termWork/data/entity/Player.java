package org.vybirto1.pjv.termWork.data.entity;

import org.vybirto1.pjv.termWork.data.AssetLoader;
import org.vybirto1.pjv.termWork.data.item.ItemInstance;
import org.vybirto1.pjv.termWork.data.level.Level;
import org.vybirto1.pjv.termWork.data.level.Tile;
import org.vybirto1.pjv.termWork.logic.OnPlayerUpdate;

import java.util.*;

public class Player extends MovableEntity {
	private static int viewRadius = 3;

	private List<ItemInstance> inventory = new ArrayList<>();
	private ItemInstance weapon;
	private ItemInstance armor;
	private int maxHitPoints = 20;
	private int speed = 30;

	private List<OnPlayerUpdate> onPlayerMove;
	private List<OnPlayerUpdate> onInventoryChange;
	private List<OnPlayerUpdate> onMainStatChange;

	public Player() {
		this(null, null);
	}

	public Player(Tile t, Level l) {
		if (t != null && l != null) {
			setTileAndLevel(t, l);
		}
		weapon = new ItemInstance(AssetLoader.getItem("basic_sword"));
		armor = new ItemInstance(AssetLoader.getItem("boxers"));
		hitPoints = maxHitPoints;
		onInteraction = e -> {
			int damage = e.getDamage() - getArmor();
			if (damage > 0){
				hitPoints -= damage;
				if (hitPoints <= 0){
					System.out.println("GAME OVER");//todo: implement game over scene
				}
				execOnMainStatChange();
			}
		};
	}

	public void addOnPlayerMove(final OnPlayerUpdate onPlayerMove) {
		if (this.onPlayerMove == null) {
			this.onPlayerMove = new LinkedList<>();
		}
		this.onPlayerMove.add(onPlayerMove);
	}

	public void addOnInventoryChanged(final OnPlayerUpdate update) {
		if (onInventoryChange == null) {
			onInventoryChange = new LinkedList<>();
		}
		onInventoryChange.add(update);
	}

	public void addOnMainStatChange(final OnPlayerUpdate update) {
		if (onMainStatChange == null) {
			onMainStatChange = new LinkedList<>();
		}
		onMainStatChange.add(update);
	}

	public List<ItemInstance> getInventory() {
		return inventory;
	}

	@Override
	public void move(Tile newTile) {
		for (Tile tile : level.getTilesWithEvaluator(tile, viewRadius, (Tile::canSeeThrough))) {
			tile.setVisible(false);
			tile.doUpdate();
		}
		super.move(newTile);
		for (Tile tile : level.getTilesWithEvaluator(tile, viewRadius, (Tile::canSeeThrough))) {
			tile.setVisible(true);
			tile.doUpdate();
		}
		if (onPlayerMove != null) {
			for (OnPlayerUpdate update : onPlayerMove) {
				update.onUpdate(this);
			}
		}
	}

	public void setTileAndLevel(Tile t, Level l) {
		this.tile = t;
		this.level = l;
		for (Tile tile : l.getTilesWithEvaluator(tile, viewRadius, (Tile::canSeeThrough))) {
			tile.setVisible(true);
		}
	}

	public double getHealthPercent() {
		return (double)hitPoints/(double)maxHitPoints;
	}

	public double getXPPercent() {
		return 0;
	}

	@Override
	protected int getDamage() {
		if (weapon == null)
			return 1;
		int minDamage = weapon.getItem().getStats().getStatIntStat("minDamage");
		int maxDamage = weapon.getItem().getStats().getStatIntStat("maxDamage");
		return rng.nextInt(maxDamage - minDamage) + minDamage;
	}

	@Override
	protected int getArmor() {
		if (armor == null)
			return 0;
		return armor.getItem().getStats().getStatIntStat("armor");
	}

	private boolean canInsertItemIntoInventory() {
		return inventory.size() < 4;
	}

	private boolean validInventoryAndIndex(final int index){
		return inventory.size() > 0 && index >= 0 && index < inventory.size();
	}

	private void addItemIntoInventory(final ItemInstance item) {
		inventory.add(item);
	}

	private void execOnInventoryChange() {
		if (onInventoryChange != null)
			for (OnPlayerUpdate update : onInventoryChange)
				update.onUpdate(this);
	}

	private void execOnMainStatChange(){
		if (onMainStatChange != null)
			for (OnPlayerUpdate update : onMainStatChange)
				update.onUpdate(this);
	}

	public void tryPickupItemFromGround(final int index) {
		List<ItemInstance> groundItems = tile.getItems();
		if (groundItems != null && groundItems.size() > 0
				&& canInsertItemIntoInventory() && index >= 0 && index < groundItems.size()) {
			addItemIntoInventory(groundItems.remove(index));
			execOnInventoryChange();
			tile.doUpdate();
		}
	}

	public void tryDropItemFromInventory(final int index) {
		if (validInventoryAndIndex(index)) {
			tile.addItem(inventory.remove(index));
			execOnInventoryChange();
			tile.doUpdate();
		}
	}

	public void tryEquipItemFromInventory(final int index){
		if (validInventoryAndIndex(index)) {
			ItemInstance candidate = inventory.remove(index);
			switch (candidate.getItem().getType()){
				case WEAPON:
					if (weapon != null)
						inventory.add(weapon);
					weapon = candidate;
					break;
				case EQUIPABLE:
					if (armor != null)
						inventory.add(armor);
					armor = candidate;
					break;
				case MISC:
					break;
			}
			execOnInventoryChange();
		}
	}

	public ItemInstance getWeapon() {
		return weapon;
	}

	public ItemInstance getArmorInstance() {
		return armor;
	}

	public void tryUnequipWeapon(){
		if (weapon != null){
			if (canInsertItemIntoInventory())
				inventory.add(weapon);
			else
				tile.addItem(weapon);
			weapon = null;
			execOnInventoryChange();
			tile.doUpdate();
		}
	}

	public void tryUnequipArmor(){
		if (armor != null){
			if (canInsertItemIntoInventory())
				inventory.add(armor);
			else
				tile.addItem(armor);
			armor = null;
			execOnInventoryChange();
			tile.doUpdate();
		}
	}

	public int getSpeed() {
		return speed;
	}
}
