package org.vybirto1.pjv.termWork.data.item;

import org.json.JSONObject;

public class ArmorStats extends ItemStats {
	public ArmorStats(final JSONObject obj){
		addStat(obj, "armor");
	}
}
