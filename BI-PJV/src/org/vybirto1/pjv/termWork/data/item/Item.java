package org.vybirto1.pjv.termWork.data.item;

import org.json.JSONObject;
import org.vybirto1.pjv.termWork.data.common.LoadedAsset;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Item implements LoadedAsset {
	private final String id;
	private final String name;
	private final double value;
	private final double weight;
	private final ItemType type;
	private final ItemStats stats;

	public Item(File asset){
		String fileContent = load(asset);
		if (fileContent != null) {
			JSONObject obj = new JSONObject(fileContent);
			id = obj.getString("id");
			name = obj.getString("name");
			value = obj.getDouble("value");
			weight = obj.getDouble("weight");
			type = ItemType.valueOf(obj.getString("item_type"));
			switch (type){
				case WEAPON:
					stats = new WeaponStats(obj.getJSONObject("item_stats"));
					break;
				case EQUIPABLE:
					stats = new ArmorStats(obj.getJSONObject("item_stats"));
					break;
				case MISC:
					stats = null;
					break;
				default:
					//stats = null;
					throw new IllegalArgumentException("Unknown type of itemType");
			}
		} else {
			throw new IllegalArgumentException("Can't access " + asset.getName());
		}
	}

	@Override
	public String load(final File asset) {
		String content = null;
		try {
			content = new Scanner(asset).useDelimiter("\\Z").next();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return content;
	}

	public String getId() {
		return id;
	}

	public double getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public double getWeight() {
		return weight;
	}

	public ItemType getType() {
		return type;
	}

	public ItemStats getStats() {
		return stats;
	}
}
