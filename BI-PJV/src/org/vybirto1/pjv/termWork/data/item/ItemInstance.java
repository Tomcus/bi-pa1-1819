package org.vybirto1.pjv.termWork.data.item;

public class ItemInstance {
	private final Item item;
	public ItemInstance(Item item){
		this.item = item;
	}

	public Item getItem() {
		return item;
	}
}
