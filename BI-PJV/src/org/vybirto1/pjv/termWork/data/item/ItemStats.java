package org.vybirto1.pjv.termWork.data.item;

import org.json.JSONObject;

import java.util.Dictionary;
import java.util.Hashtable;

public abstract class ItemStats {
	private final Dictionary<String, String> stats = new Hashtable<>();
	public int getStatIntStat(final String statName){
		final String stat = stats.get(statName);
		if (stat != null)
			return Integer.parseInt(stat);
		return 0;
	}
	protected void addStat(JSONObject obj, String name){
		stats.put(name, obj.getString(name));
	}
}
