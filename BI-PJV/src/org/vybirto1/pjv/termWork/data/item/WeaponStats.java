package org.vybirto1.pjv.termWork.data.item;

import org.json.JSONObject;

public class WeaponStats extends ItemStats {
	public WeaponStats(JSONObject item_stats) {
		addStat(item_stats, "minDamage");
		addStat(item_stats, "maxDamage");
	}
}
