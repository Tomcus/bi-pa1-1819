package org.vybirto1.pjv.termWork.data.level;

import javafx.scene.input.KeyCode;
import org.vybirto1.pjv.termWork.data.entity.Enemy;
import org.vybirto1.pjv.termWork.data.entity.Player;
import org.vybirto1.pjv.termWork.logic.AI.Action;
import org.vybirto1.pjv.termWork.logic.AI.EntityAction;
import org.vybirto1.pjv.termWork.logic.IEvaluator;
import org.vybirto1.pjv.termWork.logic.AI.PlayerAction;

import java.util.*;

public abstract class Level {
	protected Tile[][] tiles;
	protected int width, height;
	protected int startX, startY;
	protected Player player;
	protected PriorityQueue<Action> queue = new PriorityQueue<>(Comparator.comparingInt(Action::getTime));

	public void movePlayer(final KeyCode kc) {
		Tile playerTile = player.getTile();
		if (kc == KeyCode.LEFT) {
			player.move(tiles[playerTile.getX() - 1][playerTile.getY()]);
		}
		if (kc == KeyCode.RIGHT) {
			player.move(tiles[playerTile.getX() + 1][playerTile.getY()]);
		}
		if (kc == KeyCode.UP) {
			player.move(tiles[playerTile.getX()][playerTile.getY() - 1]);
		}
		if (kc == KeyCode.DOWN) {
			player.move(tiles[playerTile.getX()][playerTile.getY() + 1]);
		}
		doAIWork();
	}

	private void doAIWork() {
		while (true){
			if (queue.size() <= 0 || queue.peek() instanceof PlayerAction)
				break;
			Action a = queue.remove();
			if (a instanceof EntityAction){
				EntityAction ea = (EntityAction)a;
				Enemy e = ea.getEntity();
				if (e.getHitPoints() > 0) {
					e.doAI(getTilesWithEvaluator(e.getTile(), e.getEntityData().getVisionDistance(), Tile::canSeeThrough));
					queue.add(new EntityAction(ea, ea.getTime() + e.getEntityData().getSpeed()));
				}
			}
		}
		final Action a = queue.remove();
		queue.add(new PlayerAction(a.getTime() + player.getSpeed()));
	}

	public Level() {
	}

	public void initLevel(final int width, final int height, final int startX, final int startY) {
		this.width = width;
		this.height = height;
		this.startX = startX;
		this.startY = startY;
		tiles = new Tile[width][height];
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				tiles[x][y] = new Tile(x, y);
			}
		}
	}

	public Tile get(final int x, final int y) {
		if (isInBounds(x, y))
			return tiles[x][y];
		return null;
	}

	public Set<Tile> getTilesWithEvaluator(final Tile center, final int radius, final IEvaluator<Tile> eval) {
		Set<Tile> res = new HashSet<>();
		Queue<Tile> queue = new LinkedList<>();
		res.add(center);
		queue.add(center);
		while (queue.size() > 0) {
			Tile t = queue.peek();
			double distance = Tile.distance(center, t);
			if (distance >= radius) {
				queue.remove();
				continue;
			}
			for (Tile tile : getNeighbours(t)) {
				if (!res.contains(tile)) {
					res.add(tile);
					if (eval.evaluate(tile))
						queue.add(tile);
				}
			}
			queue.remove();
		}
		return res;
	}

	protected List<Tile> getNeighbours(final Tile t) {
		List<Tile> res = new LinkedList<>();
		if (isInBounds(t.getX() + 1, t.getY()))
			res.add(tiles[t.getX() + 1][t.getY()]);
		if (isInBounds(t.getX() - 1, t.getY()))
			res.add(tiles[t.getX() - 1][t.getY()]);
		if (isInBounds(t.getX(), t.getY() + 1))
			res.add(tiles[t.getX()][t.getY() + 1]);
		if (isInBounds(t.getX(), t.getY() - 1))
			res.add(tiles[t.getX()][t.getY() - 1]);
		return res;
	}

	protected boolean isInBounds(final int x, final int y) {
		return x >= 0 && y >= 0 && x < width && y < height;
	}
}
