package org.vybirto1.pjv.termWork.data.level;

import org.vybirto1.pjv.termWork.data.AssetLoader;
import org.vybirto1.pjv.termWork.data.common.LoadedAsset;
import org.vybirto1.pjv.termWork.data.entity.Enemy;
import org.vybirto1.pjv.termWork.data.entity.EntityData;
import org.vybirto1.pjv.termWork.data.item.Item;
import org.vybirto1.pjv.termWork.data.entity.Player;
import org.vybirto1.pjv.termWork.logic.AI.EntityAction;
import org.vybirto1.pjv.termWork.logic.MapAction;
import org.vybirto1.pjv.termWork.logic.AI.PlayerAction;

import java.io.*;
import java.util.*;

public class LoadedLevel extends Level implements LoadedAsset {

	public LoadedLevel(final File levelFile, final Player player) {
		//fixme: split to multiple functions
		String[] lines = load(levelFile).split("\n");
		List<String> map = new ArrayList<>();
		int index = 0;
		while (true) {
			if (index >= lines.length)
				break;
			String line = lines[index];
			if (line.length() == 0 || line.toCharArray()[0] != '#') {
				break;
			}
			map.add(line);
			index++;
		}
		if (map.size() < 1)
			throw new IllegalArgumentException("level of size 0");
		int width = map.get(0).length();
		int height = map.size();
		initLevel(width, height, 0, 0);
		Dictionary<Character, MapAction> dict = new Hashtable<>();
		loadDictionary(dict, lines, index);
		populateMap(map, dict);
		player.setTileAndLevel(tiles[startX][startY], this);
		tiles[startX][startY].setEntity(player);
		this.player = player;
		queue.add(new PlayerAction(player.getSpeed()));
	}

	private void populateMap(final List<String> map, final Dictionary<Character, MapAction> dict) {
		for (int y = 0; y < height; y++) {
			char[] chLine = map.get(y).toCharArray();
			for (int x = 0; x < width; x++) {
				switch (chLine[x]) {
					case '#':
						tiles[x][y].setType(TileType.WALL);
						break;
					case '*':
						tiles[x][y].setType(TileType.DOOR);
						break;
					case '+':
						tiles[x][y].setType(TileType.HIDDEN_DOOR);
						break;
					case 'S':
						tiles[x][y].setType(TileType.AIR);
						startY = y;
						startX = x;
						break;
					default:
						doMapAction(chLine[x], tiles[x][y], dict);
					case ' ':
						tiles[x][y].setType(TileType.AIR);
						break;
				}
			}
		}
	}

	private void doMapAction(final char c, final Tile tile, final Dictionary<Character, MapAction> dict) {
		MapAction ma = dict.get(c);
		if (ma != null)
			ma.update(tile);
	}

	private void loadDictionary(final Dictionary<Character, MapAction> dict, final String[] lines, int index) {
		for (int i = index; i < lines.length; i++) {
			String[] line = lines[i].split(":");
			if (line[1].equals("i")){
				loadItemReference(dict, line);
			} else if (line[1].equals("e")){
				loadEntity(dict, line);
			}
		}
	}

	private void loadEntity(final Dictionary<Character, MapAction> dict, final String[] line) {
		EntityData data = AssetLoader.getEntityData(line[2]);
		if (data != null){
			dict.put(line[0].toCharArray()[0], t-> {
				Enemy e = new Enemy(t, this, data);
				t.setEntity(e);
				queue.add(new EntityAction(e,0));
			});
		}
	}

	private void loadItemReference(final Dictionary<Character, MapAction> dict, final String[] line) {
		String[] items = line[2].split(",");
		List<Item> itemList = new ArrayList<>();
		for (String item: items) {
			Item i = AssetLoader.getItem(item);
			if (i != null){
				itemList.add(i);
			}
		}
		if (itemList.size() > 0){
			dict.put(line[0].toCharArray()[0], (t) -> {
				for (Item i: itemList) {
					t.addItem(i);
				}
			});
		}
	}

	@Override
	public String load(final File asset) {
		String content = null;
		try {
			content = new Scanner(asset).useDelimiter("\\Z").next();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return content;
	}
}
