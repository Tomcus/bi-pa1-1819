package org.vybirto1.pjv.termWork.data.level;

import org.vybirto1.pjv.termWork.data.item.Item;
import org.vybirto1.pjv.termWork.data.item.ItemInstance;
import org.vybirto1.pjv.termWork.data.entity.Entity;
import org.vybirto1.pjv.termWork.data.entity.Player;
import org.vybirto1.pjv.termWork.logic.IUpdate;

import java.util.ArrayList;
import java.util.List;

public class Tile {

	private final int X, Y;
	private boolean wasVisited = false;
	private boolean isVisible = false;
	private Entity entity;
	private List<ItemInstance> items = null;
	private TileType type = TileType.WALL;
	private IUpdate onUpdate;

	public static double distance(final Tile a, final Tile b) {
		double a2 = (a.getX() - b.getX()) * (a.getX() - b.getX());
		double b2 = (a.getY() - b.getY()) * (a.getY() - b.getY());
		return Math.sqrt(a2 + b2);
	}

	public void setEntity(final Entity entity) {
		this.entity = entity;
		if (type == TileType.HIDDEN_DOOR && entity != null && entity instanceof Player) {
			type = TileType.DOOR;
		}
	}

	public Entity getEntity() {
		return entity;
	}

	public Tile(final int x, final int y) {
		X = x;
		Y = y;
	}

	public int getX() {
		return X;
	}

	public int getY() {
		return Y;
	}

	public boolean getWasVisited() {
		return wasVisited;
	}

	public boolean getVisible() {
		return isVisible;
	}

	public void setWasVisited(final boolean wasVisited) {
		this.wasVisited = wasVisited;
	}

	public void setVisible(final boolean visible) {
		isVisible = visible;
		if (visible && !wasVisited) {
			wasVisited = true;
		}
	}

	public boolean canSeeThrough() {
		return type == TileType.AIR;
	}

	public void setOnUpdate(final IUpdate onUpdate) {
		this.onUpdate = onUpdate;
	}

	public void doUpdate() {
		if (onUpdate != null) {
			onUpdate.onUpdate();
		}
	}

	public boolean isPassable() {
		switch (type) {
			case DOOR:
			case HIDDEN_DOOR:
			case AIR:
				return true;
		}
		return false;
	}

	public TileType getType() {
		return type;
	}

	public void setType(final TileType type) {
		this.type = type;
	}

	public void addItem(final Item i) {
		if (items == null)
			items = new ArrayList<>();
		items.add(new ItemInstance(i));
	}

	public void addItem(final ItemInstance itemInstance) {
		if (items == null)
			items = new ArrayList<>();
		items.add(itemInstance);
	}

	public List<ItemInstance> getItems() {
		return items;
	}

	@Override
	public String toString() {
		return "[" + X + "," + Y + "]";
	}
}
