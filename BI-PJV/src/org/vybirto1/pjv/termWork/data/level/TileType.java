package org.vybirto1.pjv.termWork.data.level;

public enum TileType {
	WALL, DOOR, AIR, HIDDEN_DOOR
}
