package org.vybirto1.pjv.termWork.graphics;

import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import org.vybirto1.pjv.termWork.data.entity.Player;

import static org.vybirto1.pjv.termWork.graphics.LevelScene.TILE_SIZE;

public class GameScene extends Pane {
	private static final Font font = new Font("Monospaced", 9);
	private LevelScene lvl;
	private PlayerStatsScene pss;
	public GameScene(final Player player) throws Exception{
		lvl = new LevelScene(player, 40, 30);
		getChildren().add(lvl);
		pss = new PlayerStatsScene(player, font);
		getChildren().add(pss);
		pss.relocate(lvl.getPrefWidth(), 0);
	}

	public void reload(int width, int height){
		if (width-200 <= 0 || height <= 0)
			return;
		width -= 200;
		width /= TILE_SIZE;
		height /= TILE_SIZE;
		lvl.reshape(width, height);
		pss.relocate(lvl.getPrefWidth(), 0);
	}

	public void getKeyInput(final KeyCode code) {
		lvl.getKeyInput(code);
	}
}
