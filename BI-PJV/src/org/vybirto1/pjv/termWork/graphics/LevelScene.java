package org.vybirto1.pjv.termWork.graphics;

import javafx.scene.Group;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import org.vybirto1.pjv.termWork.data.entity.Player;
import org.vybirto1.pjv.termWork.graphics.level.TileDrawer;
import org.vybirto1.pjv.termWork.data.level.Level;
import org.vybirto1.pjv.termWork.data.level.LoadedLevel;
import org.vybirto1.pjv.termWork.data.level.Tile;

import java.io.File;
import java.io.IOException;

public class LevelScene extends Pane {
	private Level actualLevel;
	private Group tilesGroup = new Group();
	public static final int TILE_SIZE = 16;
	public static final int BORDER_OFFSET = 2;
	private int TDWidth, TDHeight;
	private int xPivot, yPivot;
	private TileDrawer[][] tileDrawers;

	public LevelScene(Player player, int tileHorizontalCount, int tileVerticalCount) throws IOException {
		//todo: fix test level loading
		xPivot = yPivot = 0;
		TDWidth = tileHorizontalCount;
		TDHeight = tileVerticalCount;
		setPrefSize(TDWidth * TILE_SIZE, TDHeight * TILE_SIZE);
		tileDrawers = new TileDrawer[TDWidth][TDHeight];
		loadLevel(new LoadedLevel(new File(System.getProperty("user.dir") +
				"/data/adventures/test/level01.txt"), player));
		player.addOnPlayerMove((p) -> {
			Tile playerTile = p.getTile();
			if (playerTile.getX() < getXPivot() + BORDER_OFFSET ||
					playerTile.getX() >= getXPivot() + TDWidth - BORDER_OFFSET ||
					playerTile.getY() < getYPivot() + BORDER_OFFSET ||
					playerTile.getY() >= getYPivot() + TDHeight - BORDER_OFFSET) {
				adjustLevel(playerTile.getX(), playerTile.getY());
			}
		});
		adjustLevel(player.getTile().getX(), player.getTile().getY());
	}

	public int getXPivot() {
		return xPivot;
	}

	public int getYPivot() {
		return yPivot;
	}

	public void getKeyInput(KeyCode kc) {
		if (kc.isArrowKey())
			actualLevel.movePlayer(kc);
	}

	private void loadLevel(Level newLevel) {
		actualLevel = newLevel;
		for (int x = 0; x < TDWidth; x++) {
			for (int y = 0; y < TDHeight; y++) {
				Tile t = actualLevel.get(x, y);
				tileDrawers[x][y] = new TileDrawer(t, x, y);
				tilesGroup.getChildren().add(tileDrawers[x][y]);
			}
		}
		getChildren().addAll(tilesGroup);
	}

	private void adjustLevel(int newX, int newY) {
		if (tilesGroup.getChildren().size() > 0){
			tilesGroup.getChildren().clear();
		}
		xPivot = newX - TDWidth / 2;
		yPivot = newY - TDHeight / 2;
		tileDrawers = new TileDrawer[TDWidth][TDHeight];
		for (int y = 0; y < TDHeight; y++) {
			for (int x = 0; x < TDWidth; x++) {
				Tile t = actualLevel.get(x + xPivot, y + yPivot);
				tileDrawers[x][y] = new TileDrawer(t, x, y);
				tilesGroup.getChildren().add(tileDrawers[x][y]);
			}
		}
	}

	public void reshape(int width, int height) {
		setPrefSize(width * TILE_SIZE, height * TILE_SIZE);
		int centerX = TDWidth/2 + xPivot;
		int centerY = TDHeight/2 + yPivot;
		TDWidth = width;
		TDHeight = height;
		adjustLevel(centerX, centerY);
	}
}
