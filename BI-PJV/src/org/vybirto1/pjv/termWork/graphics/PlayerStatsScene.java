package org.vybirto1.pjv.termWork.graphics;

import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import org.vybirto1.pjv.termWork.data.entity.Player;
import org.vybirto1.pjv.termWork.graphics.stats.InventoryPanel;
import org.vybirto1.pjv.termWork.graphics.stats.NamePanel;
import org.vybirto1.pjv.termWork.graphics.stats.OnGroundPanel;
import org.vybirto1.pjv.termWork.graphics.stats.PlayerPointList;

public class PlayerStatsScene extends Pane {
	public PlayerStatsScene(final Player player, final Font font){
		NamePanel np = new NamePanel(font);
		getChildren().add(np);
		PlayerPointList ppl = new PlayerPointList(player, font);
		ppl.relocate(np.getLayoutX(), np.getLayoutY() + np.getPrefHeight());
		getChildren().add(ppl);
		InventoryPanel ip = new InventoryPanel(player, font);
		getChildren().add(ip);
		ip.relocate(ppl.getLayoutX(), ppl.getLayoutY() + ppl.getPrefHeight());
		OnGroundPanel ogp = new OnGroundPanel(ip, player, font);
		getChildren().add(ogp);
	}
}
