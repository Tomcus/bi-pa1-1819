package org.vybirto1.pjv.termWork.graphics.level;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import org.vybirto1.pjv.termWork.data.AssetLoader;
import org.vybirto1.pjv.termWork.data.entity.Enemy;
import org.vybirto1.pjv.termWork.data.item.ItemInstance;
import org.vybirto1.pjv.termWork.data.entity.Player;
import org.vybirto1.pjv.termWork.data.level.Tile;
import org.vybirto1.pjv.termWork.data.level.TileType;

import java.util.List;

import static org.vybirto1.pjv.termWork.graphics.LevelScene.TILE_SIZE;

public class TileDrawer extends StackPane {
	private Tile tile;

	private static final Color FOG_COLOR = new Color(0.1, 0.1, 0.1, 0.5);

	public TileDrawer(final Tile tile, final int x, final int y) {
		relocate(x * TILE_SIZE, y * TILE_SIZE);
		setTile(tile);
	}

	public void setTile(final Tile tile) {
		if (this.tile != null)
			this.tile.setOnUpdate(null);
		this.tile = tile;
		if (this.tile != null)
			tile.setOnUpdate(this::drawTile);
		drawTile();
	}

	private void drawTile() {
		if (getChildren().size() > 0){
			getChildren().clear();
		}
		if (tile == null || !tile.getWasVisited()){
			getChildren().add(new Rectangle(TILE_SIZE, TILE_SIZE, Color.BLACK));
		} else if (tile.getWasVisited()) {
			drawTileType(tile.getType());
			drawItems();
			if (tile.getVisible()) {
				drawVisible();
			} else {
				getChildren().add(new Rectangle(TILE_SIZE, TILE_SIZE, FOG_COLOR));
			}
		}
	}

	private void drawItems() {
		List<ItemInstance> items = tile.getItems();
		if (items != null && items.size() > 0){
			if (items.size() == 1){
				Image img = AssetLoader.getImage(items.get(0).getItem().getId());
				if (img != null){
					getChildren().add(new ImageView(img));
				} else {
					getChildren().add(new ImageView(AssetLoader.getImage("unloadedImage")));
				}
			} else {
				getChildren().add(new ImageView(AssetLoader.getImage("itemStash")));
			}
		}
	}

	private void drawTileType(final TileType t){
		switch (t) {
			case WALL:
			case HIDDEN_DOOR:
				getChildren().add(new ImageView(AssetLoader.getImage("wall")));
				break;
			case DOOR:
				getChildren().add(new ImageView(AssetLoader.getImage("door")));
				break;
			default:
			case AIR:
				getChildren().add(new ImageView(AssetLoader.getImage("ground")));
				break;
		}
	}

	private void drawVisible(){
		if (tile.getEntity() != null) {
			if (tile.getEntity() instanceof Player)
				getChildren().add(new ImageView(AssetLoader.getImage("player")));
			else if (tile.getEntity() instanceof Enemy) {
				getChildren().add(new ImageView(AssetLoader.getImage(((Enemy)tile.getEntity()).getEntityData().getId())));
			}
		}
	}

	public Tile getTile() {
		return tile;
	}
}
