package org.vybirto1.pjv.termWork.graphics.stats;

import javafx.scene.Group;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import org.vybirto1.pjv.termWork.data.item.ItemInstance;
import org.vybirto1.pjv.termWork.data.entity.Player;

import java.util.List;

public class InventoryPanel extends StackPane {
	private final Font font;

	public InventoryPanel(final Player player, final Font font) {
		setPrefWidth(200);
		this.font = font;
		player.addOnInventoryChanged(this::updateList);
		updateList(player);
	}

	private void updateList(final Player player) {
		if (getChildren().size() > 0) {
			getChildren().clear();
		}
		double customHeight = font.getSize() * 2;
		Group itemList = new Group();
		List<ItemInstance> inventory = player.getInventory();
		itemList.getChildren().add(new ListItem("Inventory: ", font, null, null));
		itemList.getChildren().add(getPlayerWeaponItemList(player.getWeapon(), customHeight, player));
		itemList.getChildren().add(getPlayerArmorItemList(player.getArmorInstance(), customHeight, player));
		for (int i = 0; i < inventory.size(); i++) {
			ItemInstance item = inventory.get(i);
			ListItem li = createItemInventoryListItem(item.getItem().getName(), player, i);
			li.relocate(0, (i + 3) * customHeight);
			itemList.getChildren().add(li);
		}
		setPrefHeight((inventory.size() + 3) * customHeight);
		getChildren().add(itemList);
	}

	private ListItem getPlayerArmorItemList(final ItemInstance armorInstance, final double customHeight,
	                                        final Player player) {
		String text;
		if (armorInstance == null){
			text = "A: ";
		} else {
			text = "A: " + armorInstance.getItem().getName();
		}
		ListItem li = new ListItem(text, font, null, player::tryUnequipArmor);
		li.relocate(0, 2*customHeight);
		return li;
	}

	private ListItem createItemInventoryListItem(final String text, final Player player, final int index) {
		ListItem li = new ListItem(index + ": " + text, font, ()-> player.tryEquipItemFromInventory(index),
				()-> player.tryDropItemFromInventory(index));
		return li;
	}

	public ListItem getPlayerWeaponItemList(final ItemInstance weapon, final double customHeight, final Player player) {
		String text;
		if (weapon == null){
			text = "W: ";
		} else {
			text = "W: " + weapon.getItem().getName();
		}
		ListItem li = new ListItem(text, font, null, player::tryUnequipWeapon);
		li.relocate(0, customHeight);
		return li;
	}
}
