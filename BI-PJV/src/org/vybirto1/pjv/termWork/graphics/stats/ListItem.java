package org.vybirto1.pjv.termWork.graphics.stats;

import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import org.vybirto1.pjv.termWork.logic.IUpdate;


public class ListItem extends Pane {
	private final Text listItemText;
	public ListItem(final String text, final Font font, final IUpdate onLeftClick, final IUpdate onRightClick){
		setPrefSize(196, font.getSize()*2);
		listItemText = new Text(text);
		listItemText.setFont(font);
		getChildren().add(listItemText);
		setOnMouseClicked(event -> {
			if (event.getButton() == MouseButton.PRIMARY && onLeftClick != null){
				onLeftClick.onUpdate();
			}
			if (event.getButton() == MouseButton.SECONDARY && onRightClick != null){
				onRightClick.onUpdate();
			}
		});
	}
}
