package org.vybirto1.pjv.termWork.graphics.stats;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class NamePanel extends StackPane {
	private final Text nameText;
	public NamePanel(final Font font){
		setPrefSize(200, font.getSize() * 2);
		Rectangle background = new Rectangle(1, 1, getPrefWidth() - 2, getPrefHeight() - 2);
		background.setFill(Color.BLACK);
		getChildren().add(background);
		nameText = new Text(0, 0, "Name: Barbar Cohen");
		nameText.setFont(font);
		nameText.setFill(Color.WHITE);
		getChildren().add(nameText);
	}
}
