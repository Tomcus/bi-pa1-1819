package org.vybirto1.pjv.termWork.graphics.stats;

import javafx.scene.Group;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import org.vybirto1.pjv.termWork.data.entity.Player;
import org.vybirto1.pjv.termWork.data.item.ItemInstance;

import java.util.List;

public class OnGroundPanel extends StackPane {
	private final InventoryPanel ip;
	private final Font font;

	public OnGroundPanel(final InventoryPanel ip, final Player player, final Font font){
		setPrefWidth(200);
		this.ip = ip;
		this.font = font;
		player.addOnPlayerMove(this::updateList);
		player.addOnInventoryChanged(this::updateList);
		updateList(player);
	}

	private void updateList(final Player p){
		if (getChildren().size() > 0)
			getChildren().clear();
		relocate(0, ip.getLayoutY() + ip.getPrefHeight());
		double customHeight = font.getSize() * 2;
		Group list = new Group();
		List<ItemInstance> itemList = p.getTile().getItems();
		if (itemList != null && itemList.size() > 0){
			list.getChildren().add(new ListItem("On Ground: ", font, null, null));
			for (int i = 0; i < itemList.size(); i++){
				ItemInstance item = itemList.get(i);
				ListItem li = createOnGroundItemListItem(item.getItem().getName(), p, i);
				li.relocate(0, (i + 1) * customHeight);
				list.getChildren().add(li);
			}
			setPrefHeight((itemList.size() + 1) * customHeight);
			getChildren().add(list);
		}
	}
	private ListItem createOnGroundItemListItem(final String text, final Player player, final int index){
		ListItem li = new ListItem(index + ": " + text, font,
				()-> player.tryPickupItemFromGround(index),
				null);
		return li;
	}
}
