package org.vybirto1.pjv.termWork.graphics.stats;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class PercentageItem extends Pane {
	public PercentageItem(final String prefix, final double value, final Color fullColor, final Color emptyColor,
	                      final Font font){
		setPrefSize(198, font.getSize()*2);
		Text prefixText = new Text(0, font.getSize(), prefix);
		prefixText.setFont(font);
		getChildren().add(prefixText);
		Rectangle empty = new Rectangle(font.getSize()*2,1,197 - font.getSize()*2, font.getSize());
		empty.setFill(emptyColor);
		getChildren().add(empty);
		Rectangle full = new Rectangle(font.getSize()*2,1,(197 - font.getSize()*2)*value, font.getSize());
		full.setFill(fullColor);
		getChildren().add(full);
	}
}
