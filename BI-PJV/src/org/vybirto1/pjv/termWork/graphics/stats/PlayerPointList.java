package org.vybirto1.pjv.termWork.graphics.stats;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import org.vybirto1.pjv.termWork.data.entity.Player;


public class PlayerPointList extends Pane {
	private final Font font;
	public PlayerPointList(final Player player, final Font font){
		this.font = font;
		player.addOnMainStatChange(this::update);
		update(player);
	}
	private void update(final Player player){
		setPrefSize(198, 4*font.getSize());
		if (getChildren().size() > 0){
			getChildren().clear();
		}
		PercentageItem healthBar = new PercentageItem("HP:", player.getHealthPercent(), Color.GREEN, Color.RED, font);
		getChildren().add(healthBar);
		PercentageItem xpBar = new PercentageItem("XP:", player.getXPPercent(), Color.PURPLE, Color.BLACK, font);
		xpBar.relocate(healthBar.getLayoutX(), healthBar.getLayoutY() + healthBar.getPrefHeight());
		getChildren().add(xpBar);
	}
}
