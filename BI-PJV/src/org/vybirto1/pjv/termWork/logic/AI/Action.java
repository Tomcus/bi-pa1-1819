package org.vybirto1.pjv.termWork.logic.AI;

public abstract class Action {
	private int time;

	public Action(int time){
		this.time = time;
	}

	public int getTime() {
		return time;
	}
}
