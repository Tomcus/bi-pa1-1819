package org.vybirto1.pjv.termWork.logic.AI;

import org.vybirto1.pjv.termWork.data.entity.Enemy;

public class EntityAction extends Action {
	private Enemy entity;

	public EntityAction (Enemy entity, int time){
		super(time);
		this.entity = entity;
	}

	public EntityAction(EntityAction oldAction, int time){
		super(time);
		this.entity = oldAction.entity;
	}

	public Enemy getEntity() {
		return entity;
	}
}
