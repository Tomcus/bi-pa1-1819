package org.vybirto1.pjv.termWork.logic;

public interface IEvaluator<E> {
	boolean evaluate(E t);
}
