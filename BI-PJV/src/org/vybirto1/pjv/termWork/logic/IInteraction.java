package org.vybirto1.pjv.termWork.logic;

import org.vybirto1.pjv.termWork.data.entity.MovableEntity;

public interface IInteraction {
	void interact(MovableEntity interactor);
}
