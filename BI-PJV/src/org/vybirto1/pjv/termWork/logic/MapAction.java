package org.vybirto1.pjv.termWork.logic;

import org.vybirto1.pjv.termWork.data.level.Tile;

public interface MapAction {
	void update(Tile t);
}
