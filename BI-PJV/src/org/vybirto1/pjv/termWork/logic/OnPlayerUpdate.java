package org.vybirto1.pjv.termWork.logic;

import org.vybirto1.pjv.termWork.data.entity.Player;

public interface OnPlayerUpdate {
	void onUpdate(Player player);
}
