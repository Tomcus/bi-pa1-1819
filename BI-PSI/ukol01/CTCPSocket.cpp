#include "./CTCPSocket.h"

const size_t CTCPSocket::BUFFER_SIZE = 1024;

CTCPSocket::CTCPSocket(int sock):sock(sock), _timeout(false), _overflow(false), message(), messageEnding("\n") { }

CTCPSocket::CTCPSocket(const CTCPSocket &a):CTCPSocket(a.sock) { }

CTCPSocket& CTCPSocket::operator= (const CTCPSocket& a){
  sock = a.sock;
  return *this;
}

void CTCPSocket::closeSocket(){
  close(sock);
}

void CTCPSocket::sendMessage(const string& message) const {
  // cout << "Sending: >" << message << "<" << endl;
  if(send(sock, message.c_str(), message.size(), 0) < 0){
    throw "CTCPSocket: Unnable to send message";
  }
}

bool CTCPSocket::timeout() const {
  return _timeout;
}

bool CTCPSocket::overflow() const {
  return _overflow;
}

void CTCPSocket::reset(){
  _timeout = false;
  _overflow = false;
}

void CTCPSocket::setMessageEnding(const string& newEnding){
  messageEnding = newEnding;
}

string CTCPSocket::recieve(const int TIMEOUT, const int maxMessageSize){
  char buffer[BUFFER_SIZE];
  while (true){
    struct timeval timeout;
    timeout.tv_sec = TIMEOUT;
    timeout.tv_usec = 0;
    fd_set sockets;
    size_t pos = message.find(messageEnding);
    if (pos != string::npos){
      string res = message.substr(0, pos + 2);
      message = message.substr(pos + 2, message.size() - pos - 2);
      //cout << "fail" << endl;
      return res;
    }
    if (message.size() >= (size_t)maxMessageSize){
      _overflow = true;
      //cout << "fail2" << endl;
      return "";
    }
    FD_ZERO(&sockets);
    FD_SET(sock, &sockets);
    if (select(sock + 1, &sockets, NULL, NULL, &timeout) < 0){
      //cout << "fail3" << endl;
      throw "CTCPSocket: Failed at select()";
    }
    if(!FD_ISSET(sock, &sockets)){
      //cout << "fail4" << endl;
      _timeout = true;
      return "";
    }
    int charsRead = recv(sock, buffer, BUFFER_SIZE, 0);
    if (charsRead <= 0){
      //cout << "fail5" << endl;
      throw "CTCPSocket: Failed reading from socket";
    }
    message += string(buffer, charsRead);
  }
}
