#ifndef CTCPSocket_H
#define CTCPSocket_H

#include <unistd.h> // close(), read(), write()
#include <netdb.h>
#include <string>
#include <vector>
#include <climits>
#include <iostream>

using namespace std;

class CTCPSocket{
public:
  CTCPSocket(int sock);
  CTCPSocket(const CTCPSocket& a);
  CTCPSocket& operator= (const CTCPSocket& a);
  void closeSocket();
  void sendMessage(const string & message) const;
  bool timeout() const;
  bool overflow() const;
  void reset();
  void setMessageEnding(const string& newEnding);
  string recieve(const int TIMEOUT = INT_MAX, const int maxMessageSize = INT_MAX);
  const static size_t BUFFER_SIZE;
protected:
  int sock;
  bool _timeout, _overflow;
  string message;
  string messageEnding;
};

#endif//CTCPSocket_H
