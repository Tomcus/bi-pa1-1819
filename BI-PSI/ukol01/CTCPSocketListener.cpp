#include "./CTCPSocketListener.h"

CTCPSocketListener::CTCPSocketListener(const int port):isOpen(false){
  sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0)
    throw "CTCPSocketListener: Unable to connect to any device";
  isOpen = true;
  bzero(&address, sizeof(address));
  address.sin_family = AF_INET;
  address.sin_port = htons(port);
  address.sin_addr.s_addr = htonl(INADDR_ANY);

  if (bind(sock, (struct sockaddr *) &address, sizeof(address)) < 0) {
    close(sock);
    isOpen = false;
    throw "CTCPSocketListener: Unable to bind device with socket";
  }

  if (listen(sock, 10) < 0){
    close(sock);
    isOpen = false;
    throw "CTCPSocketListener: Unable to set socket to listen";
  }
}

CTCPSocketListener::~CTCPSocketListener(){
  /*if (isOpen)
    close(sock);*/
}

void CTCPSocketListener::closeSocket(){
  if (isOpen){
    isOpen = false;
    close(sock);
  }
}
CTCPSocket CTCPSocketListener::listenAndWait() const {
  struct sockaddr_in newAddress;
	socklen_t size = 0;
  int newSock = accept(sock, (struct sockaddr *)&newAddress, &size);
  return CTCPSocket(newSock);
}
