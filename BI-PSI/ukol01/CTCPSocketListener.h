#ifndef CTCPSocketListener_H
#define CTCPSocketListener_H

#include <sys/socket.h> // socket(), bind(), connect(), listen()
#include <unistd.h> // close(), read(), write()
#include <netinet/in.h> // struct sockaddr_in
#include <arpa/inet.h> // htons(), htonl()
#include <strings.h> // bzero()
#include "./CTCPSocket.h"

using namespace std;

class CTCPSocketListener{
public:
  CTCPSocketListener(const int port);
  ~CTCPSocketListener();
  void closeSocket();
  CTCPSocket listenAndWait() const;
  //CTCPSocketListener(const CTCPSocketListener& a);
  //CTCPSocketListener& operator=(const CTCPSocketListener& a);
protected:
  struct sockaddr_in address;
  int sock;
  bool isOpen;
};

#endif//CTCPSocketListener_H
