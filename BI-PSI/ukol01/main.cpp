#include <iostream>
#include <sstream>
#include <string>
#include <climits>
#include "./CTCPSocket.h"
#include "./CTCPSocketListener.h"

using namespace std;

const int MAX_MESSAGE_LENGTH = 100;
const int DEFAULT_TIMEOUT = 1;
const int RECHARCHING_TIMEOUT = 5;
const int PORT = 3999;

const string SERVER_USER = "100 LOGIN\r\n";
const string SERVER_PASSWORD = "101 PASSWORD\r\n";
const string SERVER_MOVE = "102 MOVE\r\n";
const string SERVER_TURN_LEFT = "103 TURN LEFT\r\n";
const string SERVER_TURN_RIGHT = "104 TURN RIGHT\r\n";
const string SERVER_PICK_UP = "105 GET MESSAGE\r\n";
const string SERVER_OK = "200 OK\r\n";
const string SERVER_LOGIN_FAILED = "300 LOGIN FAILED\r\n";
const string SERVER_SYNTAX_ERROR = "301 SYNTAX ERROR\r\n";
const string SERVER_LOGIC_ERROR = "302 LOGIC ERROR\r\n";

const string MESSAGE_ENDING = "\r\n";

struct Position{
public:
  static Position DEFAULT, MIDDLE;
  int x, y;
  Position(int x, int y):x(x), y(y) { }
  Position():Position(INT_MAX, INT_MAX) { }
  Position(const Position &a):x(a.x), y(a.y) { }
  Position& operator= (const Position &a) {
    x = a.x;
    y = a.y;
    return *this;
  }
  bool operator==(const Position &a){
    return (x == a.x && y == a.y);
  }
  bool operator!=(const Position &a){
    return (a.x != x || a.y != y);
  }
  bool isValid(){
    if (x == INT_MAX && y == INT_MAX)
      return false;
    return true;
  }
};

Position Position::DEFAULT = Position(INT_MAX, INT_MAX);
Position Position::MIDDLE = Position(0, 0);


string question(CTCPSocket& socket, const string & message, int size){
  socket.sendMessage(message);
  //cout << message << endl;
  string answer = socket.recieve(DEFAULT_TIMEOUT, size);
  //cout << answer << endl;
  if (socket.overflow()){
    socket.sendMessage(SERVER_SYNTAX_ERROR);
    return "";
  }
  if (socket.timeout()){
    return answer;
  }
  //cout << "Obdrzeno: >" << answer << "<" << endl;
  while(answer == "RECHARGING\r\n"){
    answer = socket.recieve(RECHARCHING_TIMEOUT, 12);
    if (socket.timeout()){
      return "";
    }
    if (answer != "FULL POWER\r\n"){
      socket.sendMessage(SERVER_LOGIC_ERROR);
      return "";
    }
    answer = socket.recieve(DEFAULT_TIMEOUT);
    if (socket.timeout()){
      socket.reset();
      socket.sendMessage(message);
      answer = socket.recieve(DEFAULT_TIMEOUT);
    }
  }
  if ((int)answer.size() > size){
    socket.sendMessage(SERVER_SYNTAX_ERROR);
    return "";
  }
  return answer;
}

bool login(CTCPSocket& socket){
  string userName = question(socket, SERVER_USER, 100);
  if (userName == ""){
    return false;
  }
  int pass = 0;
  for (int i = 0; i < (int)userName.size() - 2; i++){
    pass += (unsigned char)userName[i];
  }
  //cout << pass << endl;
  string password = question(socket, SERVER_PASSWORD, 7);
  if (password == ""){
    return false;
  }
  int pass2;
  char c1, c2;
  if (sscanf(password.c_str(), "%d%c%c", &pass2, &c1, &c2) != 3 || c1 != '\r' || c2 != '\n'){
    socket.sendMessage(SERVER_SYNTAX_ERROR);
    return false;
  }
  if (pass != pass2){
    socket.sendMessage(SERVER_LOGIN_FAILED);
    return false;
  }
  socket.sendMessage(SERVER_OK);
  return true;
}

Position parseToPosition(CTCPSocket& socket, string & txt){
  int x, y;
  char c1, c2;
  if (sscanf(txt.c_str(), "OK %d %d%c%c", &x, &y, &c1, &c2) !=4 || c1 != '\r' || c2 != '\n'){
    socket.sendMessage(SERVER_SYNTAX_ERROR);
    return Position::DEFAULT;
  }
  return Position(x,y);
}

Position moveForward(CTCPSocket& socket){
  string answer = question(socket, SERVER_MOVE, 12);
  if (answer == ""){
    return Position::DEFAULT;
  }
  return parseToPosition(socket, answer);
}

Position turnLeft(CTCPSocket& socket){
  string answer = question(socket, SERVER_TURN_LEFT, 12);
  if (answer == ""){
    return Position::DEFAULT;
  }
  return parseToPosition(socket, answer);
}

Position turnRight(CTCPSocket& socket){
  string answer = question(socket, SERVER_TURN_RIGHT, 12);
  if (answer == ""){
    return Position::DEFAULT;
  }
  return parseToPosition(socket, answer);
}

bool isMiddle(CTCPSocket& socket, Position& pos){
  if (pos == Position::MIDDLE){
    string finalMessage = question(socket, SERVER_PICK_UP, 100);
    if (finalMessage != "")
      socket.sendMessage(SERVER_OK);
    return true;
  }
  return false;
}

Position getRot(Position& actual, Position& previous){
  return Position(actual.x - previous.x, actual.y - previous.y);
}

void comWork(CTCPSocket& socket){
  Position actual, previous;
  if (!login(socket))
    return;
  actual = moveForward(socket);
  if (actual == Position::DEFAULT)
    return;
  if (isMiddle(socket, actual))
    return;
  previous = actual;
  actual = moveForward(socket);
  if (actual == Position::DEFAULT)
    return;
  while (!isMiddle(socket, actual)){
    Position rot = getRot(actual, previous);
    if (rot.x != 0){
      if (actual.x == 0){
        if (actual.y > 0){
          turnLeft(socket);
        } else {
          turnRight(socket);
        }
      }
      if (actual.x > 0 && rot.x > 0){
        turnLeft(socket);
        turnLeft(socket);
      }
      if (actual.x < 0 && rot.x < 0){
        turnLeft(socket);
        turnLeft(socket);
      }
    } else {
      if (actual.y == 0){
        if (actual.x > 0){
          turnLeft(socket);
        } else {
          turnRight(socket);
        }
      }
      if (actual.y > 0 && rot.y > 0){
        turnLeft(socket);
        turnLeft(socket);
      }
      if (actual.y < 0 && rot.y < 0){
        turnLeft(socket);
        turnLeft(socket);
      }
    }
    previous = actual;
    actual = moveForward(socket);
    if (actual == Position::DEFAULT)
      return;
  }
}

int main(void) {
  try {
    CTCPSocketListener listener(PORT);
    while (true) {
      CTCPSocket comSocket = listener.listenAndWait();
      pid_t pid = fork();
      if (pid == 0){
        listener.closeSocket();
        comSocket.setMessageEnding(MESSAGE_ENDING);
        comWork(comSocket);
        comSocket.closeSocket();
        break;
      }
      comSocket.closeSocket();
    }
    listener.closeSocket();
  } catch (const char * err){
    cout << err << endl;
    return 1;
  }
  return 0;
}
