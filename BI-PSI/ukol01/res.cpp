#ifndef CTCPSocket_H
#define CTCPSocket_H

#include <unistd.h> // close(), read(), write()
#include <netdb.h>
#include <string>
#include <vector>
#include <climits>
#include <iostream>

using namespace std;

class CTCPSocket{
public:
  CTCPSocket(int sock);
  CTCPSocket(const CTCPSocket& a);
  CTCPSocket& operator= (const CTCPSocket& a);
  void closeSocket();
  void sendMessage(const string & message) const;
  bool timeout() const;
  bool overflow() const;
  void reset();
  void setMessageEnding(const string& newEnding);
  string recieve(const int TIMEOUT = INT_MAX, const int maxMessageSize = INT_MAX);
  const static size_t BUFFER_SIZE;
protected:
  int sock;
  bool _timeout, _overflow;
  string message;
  string messageEnding;
};

#endif//CTCPSocket_H
#ifndef CTCPSocketListener_H
#define CTCPSocketListener_H

#include <sys/socket.h> // socket(), bind(), connect(), listen()
#include <unistd.h> // close(), read(), write()
#include <netinet/in.h> // struct sockaddr_in
#include <arpa/inet.h> // htons(), htonl()
#include <strings.h> // bzero()
#include "./CTCPSocket.h"

using namespace std;

class CTCPSocketListener{
public:
  CTCPSocketListener(const int port);
  ~CTCPSocketListener();
  void closeSocket();
  CTCPSocket listenAndWait() const;
  //CTCPSocketListener(const CTCPSocketListener& a);
  //CTCPSocketListener& operator=(const CTCPSocketListener& a);
protected:
  struct sockaddr_in address;
  int sock;
  bool isOpen;
};

#endif//CTCPSocketListener_H
#include <iostream>
#include <sstream>
#include <string>
#include <climits>
#include "./CTCPSocket.h"
#include "./CTCPSocketListener.h"

using namespace std;

const int MAX_MESSAGE_LENGTH = 100;
const int DEFAULT_TIMEOUT = 1;
const int RECHARCHING_TIMEOUT = 5;
const int PORT = 3999;

const string SERVER_USER = "100 LOGIN\r\n";
const string SERVER_PASSWORD = "101 PASSWORD\r\n";
const string SERVER_MOVE = "102 MOVE\r\n";
const string SERVER_TURN_LEFT = "103 TURN LEFT\r\n";
const string SERVER_TURN_RIGHT = "104 TURN RIGHT\r\n";
const string SERVER_PICK_UP = "105 GET MESSAGE\r\n";
const string SERVER_OK = "200 OK\r\n";
const string SERVER_LOGIN_FAILED = "300 LOGIN FAILED\r\n";
const string SERVER_SYNTAX_ERROR = "301 SYNTAX ERROR\r\n";
const string SERVER_LOGIC_ERROR = "302 LOGIC ERROR\r\n";

const string MESSAGE_ENDING = "\r\n";

struct Position{
public:
  static Position DEFAULT, MIDDLE;
  int x, y;
  Position(int x, int y):x(x), y(y) { }
  Position():Position(INT_MAX, INT_MAX) { }
  Position(const Position &a):x(a.x), y(a.y) { }
  Position& operator= (const Position &a) {
    x = a.x;
    y = a.y;
    return *this;
  }
  bool operator==(const Position &a){
    return (x == a.x && y == a.y);
  }
  bool operator!=(const Position &a){
    return (a.x != x || a.y != y);
  }
  bool isValid(){
    if (x == INT_MAX && y == INT_MAX)
      return false;
    return true;
  }
};

Position Position::DEFAULT = Position(INT_MAX, INT_MAX);
Position Position::MIDDLE = Position(0, 0);


string question(CTCPSocket& socket, const string & message, int size){
  socket.sendMessage(message);
  //cout << message << endl;
  string answer = socket.recieve(DEFAULT_TIMEOUT, size);
  //cout << answer << endl;
  if (socket.overflow()){
    socket.sendMessage(SERVER_SYNTAX_ERROR);
    return "";
  }
  if (socket.timeout()){
    return answer;
  }
  //cout << "Obdrzeno: >" << answer << "<" << endl;
  while(answer == "RECHARGING\r\n"){
    answer = socket.recieve(RECHARCHING_TIMEOUT, 12);
    if (socket.timeout()){
      return "";
    }
    if (answer != "FULL POWER\r\n"){
      socket.sendMessage(SERVER_LOGIC_ERROR);
      return "";
    }
    answer = socket.recieve(DEFAULT_TIMEOUT);
    if (socket.timeout()){
      socket.reset();
      socket.sendMessage(message);
      answer = socket.recieve(DEFAULT_TIMEOUT);
    }
  }
  if ((int)answer.size() > size){
    socket.sendMessage(SERVER_SYNTAX_ERROR);
    return "";
  }
  return answer;
}

bool login(CTCPSocket& socket){
  string userName = question(socket, SERVER_USER, 100);
  if (userName == ""){
    return false;
  }
  int pass = 0;
  for (int i = 0; i < (int)userName.size() - 2; i++){
    pass += (unsigned char)userName[i];
  }
  //cout << pass << endl;
  string password = question(socket, SERVER_PASSWORD, 7);
  if (password == ""){
    return false;
  }
  int pass2;
  char c1, c2;
  if (sscanf(password.c_str(), "%d%c%c", &pass2, &c1, &c2) != 3 || c1 != '\r' || c2 != '\n'){
    socket.sendMessage(SERVER_SYNTAX_ERROR);
    return false;
  }
  if (pass != pass2){
    socket.sendMessage(SERVER_LOGIN_FAILED);
    return false;
  }
  socket.sendMessage(SERVER_OK);
  return true;
}

Position parseToPosition(CTCPSocket& socket, string & txt){
  int x, y;
  char c1, c2;
  if (sscanf(txt.c_str(), "OK %d %d%c%c", &x, &y, &c1, &c2) !=4 || c1 != '\r' || c2 != '\n'){
    socket.sendMessage(SERVER_SYNTAX_ERROR);
    return Position::DEFAULT;
  }
  return Position(x,y);
}

Position moveForward(CTCPSocket& socket){
  string answer = question(socket, SERVER_MOVE, 12);
  if (answer == ""){
    return Position::DEFAULT;
  }
  return parseToPosition(socket, answer);
}

Position turnLeft(CTCPSocket& socket){
  string answer = question(socket, SERVER_TURN_LEFT, 12);
  if (answer == ""){
    return Position::DEFAULT;
  }
  return parseToPosition(socket, answer);
}

Position turnRight(CTCPSocket& socket){
  string answer = question(socket, SERVER_TURN_RIGHT, 12);
  if (answer == ""){
    return Position::DEFAULT;
  }
  return parseToPosition(socket, answer);
}

bool isMiddle(CTCPSocket& socket, Position& pos){
  if (pos == Position::MIDDLE){
    string finalMessage = question(socket, SERVER_PICK_UP, 100);
    if (finalMessage != "")
      socket.sendMessage(SERVER_OK);
    return true;
  }
  return false;
}

Position getRot(Position& actual, Position& previous){
  return Position(actual.x - previous.x, actual.y - previous.y);
}

void comWork(CTCPSocket& socket){
  Position actual, previous;
  if (!login(socket))
    return;
  actual = moveForward(socket);
  if (actual == Position::DEFAULT)
    return;
  if (isMiddle(socket, actual))
    return;
  previous = actual;
  actual = moveForward(socket);
  if (actual == Position::DEFAULT)
    return;
  while (!isMiddle(socket, actual)){
    Position rot = getRot(actual, previous);
    if (rot.x != 0){
      if (actual.x == 0){
        if (actual.y > 0){
          turnLeft(socket);
        } else {
          turnRight(socket);
        }
      }
      if (actual.x > 0 && rot.x > 0){
        turnLeft(socket);
        turnLeft(socket);
      }
      if (actual.x < 0 && rot.x < 0){
        turnLeft(socket);
        turnLeft(socket);
      }
    } else {
      if (actual.y == 0){
        if (actual.x > 0){
          turnLeft(socket);
        } else {
          turnRight(socket);
        }
      }
      if (actual.y > 0 && rot.y > 0){
        turnLeft(socket);
        turnLeft(socket);
      }
      if (actual.y < 0 && rot.y < 0){
        turnLeft(socket);
        turnLeft(socket);
      }
    }
    previous = actual;
    actual = moveForward(socket);
    if (actual == Position::DEFAULT)
      return;
  }
}

int main(void) {
  try {
    CTCPSocketListener listener(PORT);
    while (true) {
      CTCPSocket comSocket = listener.listenAndWait();
      pid_t pid = fork();
      if (pid == 0){
        listener.closeSocket();
        comSocket.setMessageEnding(MESSAGE_ENDING);
        comWork(comSocket);
        comSocket.closeSocket();
        break;
      }
      comSocket.closeSocket();
    }
    listener.closeSocket();
  } catch (const char * err){
    cout << err << endl;
    return 1;
  }
  return 0;
}
#include "./CTCPSocket.h"

const size_t CTCPSocket::BUFFER_SIZE = 1024;

CTCPSocket::CTCPSocket(int sock):sock(sock), _timeout(false), _overflow(false), message(), messageEnding("\n") { }

CTCPSocket::CTCPSocket(const CTCPSocket &a):CTCPSocket(a.sock) { }

CTCPSocket& CTCPSocket::operator= (const CTCPSocket& a){
  sock = a.sock;
  return *this;
}

void CTCPSocket::closeSocket(){
  close(sock);
}

void CTCPSocket::sendMessage(const string& message) const {
  //cout << "Sending: >" << message << "<" << endl;
  if(send(sock, message.c_str(), message.size(), 0) < 0){
    throw "CTCPSocket: Unnable to send message";
  }
}

bool CTCPSocket::timeout() const {
  return _timeout;
}

bool CTCPSocket::overflow() const {
  return _overflow;
}

void CTCPSocket::reset(){
  _timeout = false;
  _overflow = false;
}

void CTCPSocket::setMessageEnding(const string& newEnding){
  messageEnding = newEnding;
}

string CTCPSocket::recieve(const int TIMEOUT, const int maxMessageSize){
  char buffer[BUFFER_SIZE];
  while (true){
    struct timeval timeout;
    timeout.tv_sec = TIMEOUT;
    timeout.tv_usec = 0;
    fd_set sockets;
    size_t pos = message.find(messageEnding);
    if (pos != string::npos){
      string res = message.substr(0, pos + 2);
      message = message.substr(pos + 2, message.size() - pos - 2);
      //cout << "fail" << endl;
      return res;
    }
    if (message.size() >= (size_t)maxMessageSize){
      _overflow = true;
      //cout << "fail2" << endl;
      return "";
    }
    FD_ZERO(&sockets);
    FD_SET(sock, &sockets);
    if (select(sock + 1, &sockets, NULL, NULL, &timeout) < 0){
      //cout << "fail3" << endl;
      throw "CTCPSocket: Failed at select()";
    }
    if(!FD_ISSET(sock, &sockets)){
      //cout << "fail4" << endl;
      _timeout = true;
      return "";
    }
    int charsRead = recv(sock, buffer, BUFFER_SIZE, 0);
    if (charsRead <= 0){
      //cout << "fail5" << endl;
      throw "CTCPSocket: Failed reading from socket";
    }
    message += string(buffer, charsRead);
  }
}
#include "./CTCPSocketListener.h"

CTCPSocketListener::CTCPSocketListener(const int port):isOpen(false){
  sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0)
    throw "CTCPSocketListener: Unable to connect to any device";
  isOpen = true;
  bzero(&address, sizeof(address));
  address.sin_family = AF_INET;
  address.sin_port = htons(port);
  address.sin_addr.s_addr = htonl(INADDR_ANY);

  if (bind(sock, (struct sockaddr *) &address, sizeof(address)) < 0) {
    close(sock);
    isOpen = false;
    throw "CTCPSocketListener: Unable to bind device with socket";
  }

  if (listen(sock, 10) < 0){
    close(sock);
    isOpen = false;
    throw "CTCPSocketListener: Unable to set socket to listen";
  }
}

CTCPSocketListener::~CTCPSocketListener(){
  /*if (isOpen)
    close(sock);*/
}

void CTCPSocketListener::closeSocket(){
  if (isOpen){
    isOpen = false;
    close(sock);
  }
}
CTCPSocket CTCPSocketListener::listenAndWait() const {
  struct sockaddr_in newAddress;
	socklen_t size = 0;
  int newSock = accept(sock, (struct sockaddr *)&newAddress, &size);
  return CTCPSocket(newSock);
}
