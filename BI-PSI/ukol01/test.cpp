#include <iostream>
#include <string>

using namespace std;

const string MESSAGE_END = "\r\n";

int main(void){
  string str("\n\n\n\r\r\r\\\r\n");
  cout << str.find(MESSAGE_END) << endl;
  return 0;
}
