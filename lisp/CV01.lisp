(defun Fac (n)
	(if (= n 0)
		1
		(* n (Fac (-n 1)))))
(defun moc (x n)
	(if (= x 0)
		0
		(if (= n 0)
			1
			(* x (moc x (- n 1))))))
(defun fib (n)
	(if (= n 0)
		0
		(if (= n 1)
			1
			(+ (fib (- n 1)) (fib (- n 2))))))
(defun binmoc (x n)
	(if (= x 0)
		0
		(if (= n 0)
			1
			(if (evenp n)
				(* (binmoc x (/ n 2)) (binmoc x (/ n 2)))
				(* x (* (binmoc x (/ (- n 1) 2)) (binmoc x (/ (- n 1) 2))))))))
(defun binmoc2 (x n)
	(if (= x 0)
		0
		(if (= n 0)
			1
			(if (evenp n)
				((lambda (x) (* x x)) (binmoc2 x (/ n 2)))
				(let ((a (binmoc2 x (/ (- n 1) 2)))) (* a a))
				)
			)
		)
	)

;(let ((par hodnota) (par2 hodnota2) ) telo) - jiny zapis pro lambda fci
;	-vraci hodnotu posledniho vyrazu v tele !!!
;	(let (x (binmoc a (/ (- n 1) 2)))(* x x))

;KONCOVA REKURZE
;===============
;!nevyuzivat vysledek rekurze v dalsim vypoctu
(defun FacAcc (n)
	(FacAccu n 1));1 - aby kdyz n = 0 => 0! = 1
(defun FacAccu (n Acc)
	(if (= n 0)
		Acc
	(FacAccu (- n 1) (* n Acc))
	)
)
(defun Fib2 (n)
	(if (= n 0)
		0
		(FibAcc n 1 0 1)))
(defun FibAcc (n i F0 F1)
	(if (= n i)
		F1
		(FibAcc n (+ i 1) F1 (+ F0 F1))))

;SEZNAMY
;=============
;pr.: (1 2 3)
;prazdny seznam - (), nil
;(car s) - prvni prvek seznamu (hlava)
;(cdr s) - zbytek seznamu (telo, ocas) bez hlavy
;(list 1 2 3) - vytvori seznam (1 2 3)
;take lze vytorit '(1 2 3) - (1 2 3) nelze

;Pr.: nalezi prvek x seznamu S
;test.: (nalezip 4 '(1 2 3))
;	>nil

(defun nalezip (x s)
	(if (equalp s ())
		nil
		(if (equalp x (car s))
			t
			(nalezip x (cdr s)))))
(defun NalPodp (x s)
	(if (equalp s ())
		nil
		(if (atom (car s))
			(if (equalp x (car s))
				t
				(NalPodp x (cdr s)))
			(if (NalPodp x (car s))
				t
				(NalPodp x (cdr s))))))
