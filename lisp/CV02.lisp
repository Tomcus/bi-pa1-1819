(defun delka (s)
	(if (equalp s nil)
		0
		(+ 1 (delka (cdr s)))))
(defun delkaPod (s)
	(if (equalp s nil)
		0
		(if (listp (car s))
			(+(delkaPod (cdr s)) (delkaPod (car S)))
			(+ 1 (delkaPod (cdr s))))))
(defun prostredni (s)
	(pros s s))
(defun pros (s1 s2)
	(if (equalp s2 nil)
		(car s1)
		(if (equalp (cdr s2) nil) ; kontrola zda s2 je jednoprvkovy
			(car s1)
			(pros (cdr s1) (cdr (cdr s2))))))
;du - vraci levy prostredni nebo oba
;du - vratit prvek na 1/3 v seznamu
; (cdr (cdr (cdr s2))) => (cdddr s2)
; pro 4 (maximum) (cddddr s2)
; stejne pro car (car (cdr (cdr s2))) => (caddr s2)
(defun arPrum (s)
	(if (equalp s nil)
		0
	(prum s 0 0)))
(defun prum (s n soucet)
	(if (equalp s nil)
		(/ soucet n)
		(prum (cdr s) (+ n 1) (+ soucet (car s)))))
(defun arPrum2 (s)
	(if (equalp s nil)
		0
		(/ (soucet s) (delka s))))
(defun soucet (s)
	(if (equalp s nil)
		0
		(+ (car s) (soucet (cdr s)))))
;(cons 1 (cons 2 (cons (cons 3 (cons 4 nil)) (cons 5 nil)))) => (1 2 (3 4) 5)
(defun naZac (x S)
	(cons x S))
(defun naKonec (x S)
	(if (equalp S nil)
		'(x)
		(cons (car S) (naKonec x (cdr S)))))
(defun otoc (s)
	(otocAcc s nil))
(defun otocAcc (s Acc)
	(if (equalp s nil)
		Acc
		(otocAcc (cdr s) (cons (car s) Acc))))
;du - (1 (2 3) 4 5)-> (5 4 (3 2) 1) - pridat otaceni podseznamu
(defun bubbleSort (s)
	(BubblS s s))
(defun BubblS (s1 s2)
	(if (equalp s2 nil)
		;konci
		(BubblS (Bublej s1) (cdr s2))))
(defun Bublej (s)
	(if (equalp s nil)
		nil
		(if (> (car s) (car (cdr s)))
			(cons (car (cdr s)) (Bublej (cons (car s) (cddr s))))
			(cons (car s) (Bublej (cdr S))))))