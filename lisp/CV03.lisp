(defun AzaB (x y s)
	(if (equalp s nil)
		nil
		(if (equalp (car s) x)
			(cons y (AzaB x y (cdr s)))
			(if (listp (car s))
				(cons (AzaB x y (car s)) (AzaB x y (cdr s)))
				(cons (car s) (AzaB x y (cdr s)))))))
(defun Slij (x y)
	(if (equalp x nil)
		y
		(cons (car x) (Slij (cdr x) y))))
(defun Rozbal (s)
	(if (equalp s nil)
		nil
		(if (atom (car s))
			(cons (car s) (Rozbal (cdr s)))
			(Slij (Rozbal (car s)) (Rozbal (cdr s))))))
(defun prvniSloupec (s)
	(if (equalp s nil)
		nil
		(cons (car (car s)) (prvniSloupec (cdr s)))))
;(defun HDiag (m))
(defun delka (s)
	(if (equalp s nil)
		0
		(+ 1 (delka (cdr s)))))
(defun Rozmer (m)
	(cons (delka m) (cons (delka (car m)) nil)))
(defun sectiListy (s1 s2)
	(if (equalp s1 nil)
		nil
		(cons (+ (car s1) (car s2)) (sectiListy (cdr s1) (cdr s2)))))
(defun SumM (m1 m2)
	(if (equalp m1 nil)
		nil
		(cons (sectiListy (car m1) (car m2)) (SumM (cdr m1) (cdr m2)))))
(defun jTyPrvekSeznamu (n s)
	(if (equalp n 1)
		(car s)
		(jTyPrvekSeznamu (- n 1) (cdr s))))
(defun jTySloupecMatice (n m)
	(if (equalp m nil)
		nil
		(cons (jTyPrvekSeznamu n (car m)) (jTySloupecMatice n (cdr m)))))
(defun iTyRadekMatice (n m)
	(if (equalp n 1)
		car (m)
		(iTyRadekMatice (- n 1) (cdr m))))
(defun Usekni (m)
	(if (equalp m nil)
		nil
		(cons (cdr (car m)) (Usekni (cdr m)))))
(defun HDiag (m)
	(if (null m)
		nil
		(cons (car(car m)) (HDiag (Usekni (cdr m))))))
(defun InOrderPrintBVS (st)
	(if (null st)
		nil
		(Slij (InOrderPrintBVS(car (cdr st))) (cons (car st) (InOrderPrintBVS (car (cdr (cdr st))))))))
;FUNKCE JAKO PARAMETR
;(Vykonej f x y) - nebude fungovat bude chtit f vyhodnotit
;(Vykonej #'f x y) - pujde kdyz ve fci zavolam (Funcall f x y)
(defun Vykonej (f x y)
	(Funcall f x y))
