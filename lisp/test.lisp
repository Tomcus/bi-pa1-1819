(defun modulo(a b)
	(if (< a b)
		a
		(modulo (- a b) b)))