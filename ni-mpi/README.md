# Reseni ukolu na MPI

Co je potreba:
* G++ (testovano s verzi 10.2)
* GNU Make (testovano na verzi 4.3)
* strip

## Kompilace

```
make
```

## Spusteni

```
make run
```

## K reseni

Zdrojove kody se nachazeji ve slozce `src`. Hlavnim souborem je `main.cpp` ten obasuje spusteni vypoctu reseni zadanych ukolem. V hlavni funkci main se spousti 3* funkce test_jacobi pro moznosti a-c a pak se spousti test_gauss_seidel take pro moznosti a-c.

Funkce `test_jacobi` a `test_gause_seidel` nejdrive vytvori matici A a vektor B podle zadani a nasledne spusti vypocet dle danne metody a vytiskne na standartni vstup vysledek (pocet iteraci a reseni).

Samotne implementace datovych struktur (matice, vektor) a jejich operaci (nasobeni matic, scitani a odcitani vektoru, ...) se nachazeji v samostatnych zdrojovych souborech. Jelikoz jsem se rozhodl pouzit pri reseni moznosti templatovanych trid v C++, tak jsem musel umistit implementaci funkci do hlavickoveho souboru, a tak puvodni plan, tedy vytvoril jednoduchou a samostatnou matematickou knihovnu, zanikl. Nakonec je v techto zdrojovych souborech par funkci navic, ktere jsem nakonec nepouzil. Veskere funkce a struktury se nachazeji v namespacu `mpi`.

K datovym strukturam. Zakladnimi prvky jsou `view` typy, tedy `matrix_view` a `vector_view`. Tyto prvky jsou abstraktnimy objekty a meli by byt pouze pouzivany jako interface promenne pro argumenty funkci. Zaroven definuji virtualni funkce `get` a `set` pro nastavovani prvnku v matici/vektoru. Pokud clovek potrebuje do struktury zapisovat, tak mu k tomu poslouzi datove struktury `matrix` a `vector`. Obe jsou jsou silne templatovane a to takto:

```c++
mpi::matrix<datovy_typ_matic, sirka, vyska> mat; // Vim ze toto by melo byt spravne obracene, ale me osobne vyhovuje vice tento programatorsky zpusob.

mpi::vector<datovy_typ_vectoru, velikost> vec;
```

Pokud uzivatel potrebuje vytvorit matici dle nejakeho predpisu (ctete funkce), jako napriklad jednotkovou matici E nebo matice ze zadani ulohy, tak je lze vytvorim pomoci struktur zacinajici na `templated_*`. Tedy `templated_vector` a `templated_matrix`, tem je mozne do konstruktoru predat funcki, ktera vezme souradnice a vrati danne cislo. Pro metodu `set` je v tomto pripade vyhoze `std::runtime_error`.

Pro matice jsem jeste pridal specialni struktury pro ctvercove matice. Existuji tedy i struktury `sq_matrix_view`, `sq_matrix` a `sq_templated_matrix`. Tyto struktury potrebuji do templatu dodat jenom velikost jedne strany. Definice potom muze vypadat nejak tatko:

```c++
mpi::sq_matrix<double, 20> mat;
```

Kazda struktura je definovana v souboru po ni pojemonvanem, tedy `matrix.hpp` a `vector.hpp`. Tyto soubory take obsahuji funkce a operace nad jednotlivymi strukturami. Ne vsechny operace jsou definovany, ale nakonec jsem se uchylil k tomu implementovat jenom co je potreba (zbyvalo malo casu). Ale rad bych se k teto knihovne v budoucnosti vratil a chybejice operace dodelal, jelikoz me prace na teto casti velice bavila.

Operace mezi jednotlivymi strukturami (tj. nasobeni matice a vektoru) a implementace reseni Jacobiho a Gaussovy-Seidelovy metody se nachazeji v `mpi_math.hpp`. K implementacim obou metod jsem pouzil ruzne vzorecky (halvne z duvodu limitace implementace a stavu knihovny). Pro Jacobiho metodu jsem pouzil vzorecek s maticemi a pouzil jsem i mnou implementovane operace. Pro Gausoovu-Seidelovu metodu jsem nemel nektere operace implemntovany a ze strachu ze nstihnu ukol dodelat cely, tak jsem pouzil vzorecek, ktery pocita vysledky po prvku (samotny vzorecek je uveden i v komentarich zdrojoveho kodu).

## K vysledkum

Prvni 2 zadani pro obe metody dobehly uspesne. Zatimco u moznosti c nelze vyuzit Jacobiho metodu protoze diagonala matice A neni dominantnejsi nez zbytek matice. A u Gausovy-Seidelovy metody take neni vysledek, protoze matice neni diagonalne dominantni ani pozitivne definitni (je symetricka).

Aktualne Jacobiho metoda spadne (hodi vyjimku), ale Gausova-Seidelova metoda skonci, ale se spravnym vysledkem. Jelikoz Gaussova-Seidelova metoda zarucuje, ale ne nutne, vysledky pro diagonalne dominantni matcie a nebo diagonalni a pozitivne definitni, tak tyto vlastnosti nejsou kontrolovany, ale funkce vrati hodne vysoke hodnoty, hodnoty nekonecna nebo `NaN`, takze uzivatel vi ze je neco spatne (Aktualne toto reseni je zakomentovane aby se ve vysledku neobjevovalo, pokud chcete, tak si jej muzete odkomentovat v `main.cpp`).

### Porovnani metod

Jacobiho metoda je obecne vice iterativne narocnejsi nez metoda GS. Ovsem vyhodou Jacobiho metody je ze lze pouzit vypocet jednotlivych mezivysledku paralerizovat. Pokud bych mel vytvorit nejakou obecnou metodu solve, tak bych se nechal inspirovat `std::sort` z C++. Teda ze pro mensi matice bych pouzil GS metodu a pro vetsi matice (chtelo by to vyzkouset, ale napriklad pro n > 50 by mohla byt dobra podminka) Jacobiho metodu.
