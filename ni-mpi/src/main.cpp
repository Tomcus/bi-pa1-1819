#include <iostream>
#include <functional>
#include <tuple>
#include <stdexcept>

#include "mpi_math.hpp"

const double MIN_ERROR = 1e-6;

// Tests Jacobi method as specified in task
void test_jacobi(double gamma) {
    mpi::sq_templated_matrix<double, 20> a(
        [=](size_t x, size_t y){
            if (x == y) {
                return gamma;
            } else if (x - y <= 1 || y - x <= 1) {
                return -1.0;
            } else {
                return 0.0;
            }
        }
    );
    
    mpi::templated_vector<double, 20> b(
        [=](size_t i) {
            if (i == 0 || i == 19)
                return gamma - 1;
            return gamma - 2;
        }
    );
    try {
        auto [res_vec, iter] = mpi::jacobi_solve(a, b, MIN_ERROR);
        std::cout << "Jacobi compute finished for gamma=" << gamma << " in " << iter << " iterations.\nSolution:\n";
        res_vec.print();
    } catch (const std::exception& e) {
        std::cout << "Error Jacobi computation for gamma=" << gamma << " failed because:\n" << e.what() << std::endl;
    }
}

// Tests Jacobi method as specified in task
void test_gauss_seidel(double gamma) {
    // Matrix A
    mpi::sq_templated_matrix<double, 20> a(
        [=](size_t x, size_t y){
            if (x == y) {
                return gamma;
            } else if (x - y <= 1 || y - x <= 1) {
                return -1.0;
            } else {
                return 0.0;
            }
        }
    );
    // Vector b
    mpi::templated_vector<double, 20> b(
        [=](size_t i) {
            if (i == 0 || i == 19)
                return gamma - 1;
            return gamma - 2;
        }
    );
    // Get solution with number of iterations
    try {
        auto [res_vec, iter] = mpi::gauss_seidel_solve(a, b, MIN_ERROR);
        std::cout << "Gauss-Seidel compute finished for gamma=" << gamma << " in " << iter << " iterations.\nSolution:\n";
        res_vec.print();
    } catch (std::exception& e) {
        std::cout << "Error Gauss-Seidel computation for gamma=" << gamma << " failed because:\n" << e.what() << std::endl;
    }
}

int main() {
    test_jacobi(3);
    test_jacobi(2);
    test_jacobi(1);
    test_gauss_seidel(3);
    test_gauss_seidel(2);
    // test_gauss_seidel(1);
}