#ifndef MPI_MATH_MATRIX_HPP
#define MPI_MATH_MATRIX_HPP

#include <iostream>
#include <array>
#include <stdexcept>
#include <functional>
#include <utility>
#include <cmath>

namespace mpi {

    // Abstract matrix class - all matrix implementations should inherit from this.
    template<typename T, size_t width, size_t height>
    class matrix_view {
    public:
        virtual T get(size_t i, size_t j) const = 0;
        virtual void set(size_t i, size_t j, const T& new_val) = 0;

        // Prints matrix in current state
        void print() const {
            for (size_t j = 0; j < height; ++j) {
                for (size_t i = 0; i < width; ++i) {
                    std::cout << get(i, j) << " ";
                }
                std::cout << "\n";
            }
            std::cout << std::flush;
        }
    };

    // Matrix that is defined by function - simple matricies like Identity Matrix
    template<typename T, size_t width, size_t height>
    class templated_matrix: public matrix_view<T, width, height> {
    public:
        using template_function = std::function<T(size_t, size_t)>;
        explicit templated_matrix(const template_function& t_func): temp_func(t_func) { }
        // Gets value from saved function
        T get(size_t i, size_t j) const override {
            if (i >= width || j >= height)
                throw std::runtime_error("Index out of range to get value from matrix");
            return temp_func(i, j);
        }
        // Throws an error if you try to write some data into this structure - create copy for it
        void set(size_t , size_t , const T& ) override {
            throw std::runtime_error("Can't set values of templated matrix");
        }
    protected:
        template_function temp_func;
    };

    // Main matrix implementation - data are stored in an array.
    template<typename T, size_t width, size_t height>
    class matrix: public matrix_view<T, width, height> {
    public:
        matrix(): data() { }
        T get(size_t i, size_t j) const override {
            if (i >= width || j >= height)
                throw std::runtime_error("Index out of range to get value from matrix");
            return data[i + j*width];
        }
        void set(size_t i, size_t j, const T& new_val) override {
            if (i >= width || j >= height)
                throw std::runtime_error("Index out of range to set value in matrix");
            data[i + j*width] = new_val;
        }
        void operator=(const matrix_view<T, width, height>& other) {
            for (size_t i = 0; i < width; ++i) {
                for (size_t j = 0; j < height; ++j) {
                    set(i, j, other.get(i, j));
                }
            }
        }
    protected:
        std::array<T, width*height> data;
    };

    // Definition of square matricies
    template <typename T, size_t size>
    using sq_matrix_view = matrix_view<T, size, size>;
    template <typename T, size_t size>
    using sq_matrix = matrix<T, size, size>;
    template <typename T, size_t size>
    using sq_templated_matrix = templated_matrix<T, size, size>;

    // Definition of matrix multiplication
    template<typename T, size_t a_height, size_t a_width_b_height, size_t b_width>
    matrix<T, b_width, a_height> multiply(const matrix_view<T, a_width_b_height, a_height>& a,
                                          const matrix_view<T, b_width, a_width_b_height>& b) {
        matrix<T, b_width, a_height> res;
        for (size_t i = 0; i < b_width; ++i) {
            for (size_t j = 0; j < a_height; ++j) {
                T new_val = 0;
                for (size_t k = 0; k < a_width_b_height; ++k) {
                    new_val += a.get(k, j) * b.get(i, k);
                }
                res.set(i, j, new_val);
            }
        }
        return res;
    }

    // Implemenatation of matrix addition
    template<typename T, size_t width, size_t height>
    matrix<T, width, height> add(const matrix_view<T, width, height>& a,
                                 const matrix_view<T, width, height>& b) {
        matrix<T, width, height> res;
        for (size_t i = 0; i < width; ++i) {
            for (size_t j = 0; j < height; ++j) {
                res.set(i, j, a.get(i, j) + b.get(i, j));
            }
        }
        return res;
    }

    // Implemenatation of matrix subtraction
    template<typename T, size_t width, size_t height>
    matrix<T, width, height> subtract(const matrix_view<T, width, height>& a,
                                      const matrix_view<T, width, height>& b) {
        matrix<T, width, height> res;
        for (size_t i = 0; i < width; ++i) {
            for (size_t j = 0; j < height; ++j) {
                res.set(i, j, a.get(i, j) - b.get(i, j));
            }
        }
        return res;
    }

    // Implementation of matrix multiplication by scalar value.
    template<typename T, size_t width, size_t height>
    matrix<T, width, height> multiply_by_scalar(const matrix_view<T, width, height>& mat,
                                                const T& scalar) {
        matrix<T, width, height> res;
        for (size_t i = 0; i < width; ++i) {
            for (size_t j = 0; j < height; ++j) {
                res.set(i, j, mat.get(i, j) * scalar);
            }
        }
        return res;
    }

    // Function creates new matrix from preexisting one by extracting the diagonal values
    template<typename T, size_t size>
    sq_matrix<T, size> extract_diagonal(const sq_matrix_view<T, size>& mat) {
        sq_matrix<T, size> res;
        for (size_t i = 0; i < size; ++i) {
            for (size_t j = 0; j < size; ++j) {
                if (i == j) {
                    res.set(i, j, mat.get(i, j));
                } else {
                    res.set(i, j, 0);
                }
            }
        }
        return res;
    }

    // Function creates new matrix from preexisting one by extracting values under the diagonal
    template<typename T, size_t size>
    sq_matrix<T, size> extract_under_diagonal(const sq_matrix_view<T, size>& mat) {
        sq_matrix<T, size> res;
        for (size_t i = 0; i < size; ++i) {
            for (size_t j = 0; j < size; ++j) {
                if (i < j) {
                    res.set(i, j, mat.get(i, j));
                } else {
                    res.set(i, j, 0);
                }
            }
        }
        return res;
    }

    // Function creates new matrix from preexisting one by extracting values over the diagonal
    template<typename T, size_t size>
    sq_matrix<T, size> extract_over_diagonal(const sq_matrix_view<T, size>& mat) {
        sq_matrix<T, size> res;
        for (size_t i = 0; i < size; ++i) {
            for (size_t j = 0; j < size; ++j) {
                if (i > j) {
                    res.set(i, j, mat.get(i, j));
                } else {
                    res.set(i, j, 0);
                }
            }
        }
        return res;
    }

    // Checks if matrix is diagonally dominant
    template<typename T, size_t size>
    bool is_diagonally_dominant(const sq_matrix_view<T, size>& mat) {
        for (size_t row = 0; row < size; ++ row) {
            T diag = 0;
            T sum_of_others = 0;
            for (size_t i = 0; i < size; ++i) {
                if (i == row) {
                    diag = abs(mat.get(i, row));
                } else {
                    sum_of_others += abs(mat.get(i, row));
                }
            }
            if (sum_of_others > diag)
                return false;
        }
        return true;
    }

    template <typename T, size_t size>
    bool is_symmetric(const sq_matrix_view<T, size>& mat) {
        for (size_t row = 0; row < size; ++row) {
            for (size_t i = 0; i < row; ++i) {
                if (mat.get(i,row) != mat.get(row, i)) {
                    return false;
                }
            }
        }
        return true;
    }
}

// Definitions of all the operators using the methods above
template<typename T, size_t a_height, size_t a_width_b_height, size_t b_width>
mpi::matrix<T, b_width, a_height> operator*(const mpi::matrix_view<T, a_width_b_height, a_height>& a,
                                            const mpi::matrix_view<T, b_width, a_width_b_height>& b) {
    return std::move(mpi::multiply(a, b));
}

template<typename T, size_t width, size_t height>
mpi::matrix<T, width, height> operator+(const mpi::matrix_view<T, width, height>& a,
                                        const mpi::matrix_view<T, width, width>& b) {
    return std::move(mpi::add(a, b));
}

template<typename T, size_t width, size_t height>
mpi::matrix<T, width, height> operator-(const mpi::matrix_view<T, width, height>& a,
                                        const mpi::matrix_view<T, width, width>& b) {
    return std::move(mpi::subtract(a, b));
}

template<typename T, size_t width, size_t height>
mpi::matrix<T, width, height> operator*(const mpi::matrix_view<T, width, height>& a, const T& scalar) {
    return std::move(mpi::multiply_by_scalar(a, scalar));
}

template<typename T, size_t width, size_t height>
mpi::matrix<T, width, height> operator*(const T& scalar, const mpi::matrix_view<T, width, height>& a) {
    return std::move(mpi::multiply_by_scalar(a, scalar));
}

#endif//MPI_MATH_MATRIX_HPP