#ifndef MPI_MATH_HPP
#define MPI_MATH_HPP

#include <tuple>

#include "matrix.hpp"
#include "vector.hpp"

namespace mpi {

    // Implemenation of vector * matrix multiplication
    template<typename T, size_t width, size_t height>
    vector<T, height> multiply(const matrix_view<T, width, height>& mat, const vector_view<T, width>& vec) {
        vector<T, height> res;
        for (size_t j = 0; j < height; ++j) {
            T new_val = 0;
            for (size_t i = 0; i < width; ++i) {
                new_val += mat.get(i, j) * vec.get(i);
            }
            res.set(j, new_val);
        }
        return res;
    }

    // Implementation of Jacobi method for solving Ax=b
    template<typename T, size_t size>
    std::tuple<vector<T, size>, size_t> jacobi_solve(const sq_matrix_view<T, size>& a, const vector_view<T, size>& b,
                                                     const T& minimal_error) {
        if (!is_diagonally_dominant(a)) {
            throw std::runtime_error("Can't use jacobi method to solve Ax=b, because A is not diagonally dominant.");
        }
        auto diag_matrix = extract_diagonal(a);
        auto other_vals = a - diag_matrix;
        size_t iterations = 0;
        vector<T, size> res;
        vector<T, size> error_vec;
        for (size_t i = 0; i < size; ++i) {
            // Setting result vector to all zeros
            res.set(i, 0);
            // Setting error vector to all zeros
            error_vec.set(i, 0);
            // Inverting diagonal matrix (what is needed in the algorithm)
            diag_matrix.set(i, i, 1/diag_matrix.get(i, i));
        }
        double error = 1000;
        while (error > minimal_error) {
            iterations ++;
            // Calculate solution
            res = multiply(diag_matrix, b - multiply(other_vals, res));
            // Calculate residulum
            error_vec = multiply(a, res) - b;
            // recalculate error
            error = error_vec.magnitude() / b.magnitude();
        }
        return std::make_tuple(res, iterations);
    }

    // Implementation of Gauss-Seidel method for solving Ax=b
    template<typename T, size_t size>
    std::tuple<vector<T, size>, size_t> gauss_seidel_solve(const sq_matrix_view<T, size>& a, const vector_view<T, size>& b,
                                                           const T& minimal_error) {
        vector<T, size> res;
        vector<T, size> error_vec;
        size_t iterations = 0;
        for (size_t i = 0; i < size; ++i) {
            // Setting result vector to all zeros
            res.set(i, 0);
            // Setting error_vec to all zeros (to be initialized)
        }
        double error = 1000;
        while (error > minimal_error) {
            iterations ++;
            for (size_t i = 0; i < size; ++i) {
                T sum_a = 0;
                for (size_t j = 0; j < i; ++j) {
                    sum_a += a.get(i, j) * res.get(j);
                }
                T sum_b = 0;
                for (size_t j = i + 1; j < size; ++j) {
                    sum_b += a.get(i, j) * res.get(j);
                }
                res.set(i, 1/a.get(i,i) * (b.get(i) - sum_a - sum_b));
            }
            // Calculate residulum
            error_vec = multiply(a, res) - b;
            // recalculate error
            error = error_vec.magnitude() / b.magnitude();
        }
        return std::make_tuple(res, iterations);
    }
}

#endif//MPI_MATH_HPP