#ifndef MPI_MATH_VECTOR_HPP
#define MPI_MATH_VECTOR_HPP

#include <iostream>
#include <cmath>
#include <array>
#include <functional>
#include <stdexcept>

namespace mpi {

    // Abstract vector class, every implementation of vector should inherit from this.
    template <typename T, size_t size>
    class vector_view {
    public:
        virtual T get(size_t i) const = 0;
        virtual void set(size_t i, const T& new_val) = 0;
        void print() const {
            for (size_t i = 0; i < size; ++i) {
                std::cout << get(i) << " ";
            }
            std::cout << std::endl;
        }
        T magnitude() const {
            T res = 0;
            for (size_t i = 0; i < size; ++i) {
                res += get(i)*get(i);
            }
            return sqrt(res);
        }
    };

    // Templated vector - defined by simple function
    template <typename T, size_t size>
    class templated_vector: public vector_view<T, size> {
    public:
        using template_function = std::function<T(size_t)>;
        explicit templated_vector(const template_function& t_func): temp_func(t_func) { }
        T get(size_t i) const override {
            if (i >= size) {
                throw std::runtime_error("Index out of range for getting value from vector.");
            }
            return temp_func(i);
        }
        void set(size_t , const T& ) override {
            throw std::runtime_error("Can't set values to templated vector");
        }
    protected:
        template_function temp_func;
    };

    // Real implementation of vector, where data are stored in array
    template <typename T, size_t size>
    class vector: public vector_view<T, size> {
    public:
        vector(): data() { }
        T get(size_t i) const override {
            if (i >= size) {
                throw std::runtime_error("Index out of range for getting value from vector.");
            }
            return data[i];
        }
        void set(size_t i, const T& new_val) override {
            if (i >= size) {
                throw std::runtime_error("Index out of range for setting value in vector.");
            }
            data[i] = new_val;
        }
        void operator=(const vector_view<T, size>& other) {
            for (size_t i = 0; i < size; ++i) {
                data[i] = other.get(i);
            }
        }
    protected:
        std::array<T, size> data;
    };

    // Definition of vector addition
    template <typename T, size_t size>
    vector<T, size> add(const vector_view<T, size>& a, const vector_view<T, size>&b) {
        vector<T, size> res;
        for (size_t i = 0; i < size; ++i) {
            res.set(i, a.get(i) + b.get(i));
        }
        return res;
    }

    // Implementation of vector subtraction
    template <typename T, size_t size>
    vector<T, size> subtract(const vector_view<T, size>& a, const vector_view<T, size>&b) {
        vector<T, size> res;
        for (size_t i = 0; i < size; ++i) {
            res.set(i, a.get(i) - b.get(i));
        }
        return res;
    }
}

// Definitions of operators using function defined above.
template <typename T, size_t size>
mpi::vector<T, size> operator+(const mpi::vector_view<T, size>& a, const mpi::vector_view<T, size>&b) {
    return std::move(mpi::add(a, b));
}

template <typename T, size_t size>
mpi::vector<T, size> operator-(const mpi::vector_view<T, size>& a, const mpi::vector_view<T, size>&b) {
    return std::move(mpi::subtract(a, b));
}

#endif//MPI_MATH_VECTOR_HPP