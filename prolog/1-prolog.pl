% ======= Jak nacist tento soubor ========

% ?-   %interpreter ocekava zadani dotazu.

% ?- consult('1-prolog.pl').             
% % 1-prolog.pl compiled 0.00 sec, 28 clauses
% true.

% consult('1-prolog.pl').             % "kompiluj" program 1-prolog.pl
% ['program.pl',program2].            % "kompiluj" program.pl , program2.pl v aktualnim adresari
% listing.                            % vypis programove predikty
% trace, rodic(X,david).              % trasuj volne predikaty
% notrace.                            % zrus rezim trasovani
% halt.                               % ukonci interpreter


% ======= Databaze faktu ========


muz(bart).
muz(homer).
muz(abraham).
muz(clancy).

zena(mona).
zena(marge).
zena(liza).
zena(meggie).
zena(selma).
zena(patty).
zena(jacqueline).

rodic(homer,bart).
rodic(homer,liza).
rodic(homer,meggie).
rodic(abraham,homer).
rodic(marge,bart).
rodic(marge,liza).
rodic(marge,meggie).
rodic(mona,homer).
rodic(jacqueline,marge).
rodic(jacqueline,patty).
rodic(jacqueline,selma).


manzel(homer,marge).
manzel(abraham,mona).
manzel(clancy,jacqueline).

% ======= Prvni priklady na procviceni ========

vdana(Z) :- manzel(_,Z). %_ anonimni promenna - neni treba zjistovat jeji hodnotu! - nezajima me jeji hodnota
zenaty(M) :- manzel(M,_).

dite(Dite, Rodic) :- rodic(Rodic, Dite).

%matka(?M,?Dite) je M matkou ditete Dite
matka(M,D) :- zena(M), rodic(M,D).

%otec(?O,?Dite) je O otcem ditete Dite
otec(O,D) :- muz(O), rodic(O,D).

rodice(R1, R2, D) :- otec(R1, D), matka(R2, D).
%nebo
rodice(R1, R2, D) :- otec(R2, D), matka(R1, D).

%syn(?S,?R) je S syn rodice R

syn(Syn, Rodic) :- muz(Syn), rodic(Rodic, Syn).

%dcera(?D,?R) je D dcerou rodice R

dcera(Dcera, Rodic) :- zena(Dcera), rodic(Rodic, Dcera).

%sourozenci(?X, ?Y) Jsou X a Y sourozenci. Vlastni/nevlastni

%sourozenci(X,Y) :- rodice(Matka,Otec,X), rodice(Matka,Otec,Y).
sourozenci(X,Y) :- rodic(R,X), rodic(R,Y), X \= Y. %X se nerovna Y!

%sestra(?S, ?X) je S sestrou clovek X

%sestra(Sestra, X) :- zena(Sestra), sourozenci(Sestra, X).
sestra(S,X) :- rodic(R,S), rodic(R,X), zena(S).
%bratr(?B, ?X) je B bratrem cloveka X

%bratr(Bratr, X) :- muz(Bratr), sourozenci(Bratr, X).
bratr(B, X) :- rodic(R,B), rodic(R,X), muz(B).
%prarodic(?P,?Vnuk) je P prarodic vnuka Vnuk

prarodic(P,V) :- rodic(P,X), rodic(X,V).

%dedecek(?D,?Vnuk) 
%babicka(B,Vnuk)

%teta(?T,?Dite) je T tetou ditete D

%stryc(?S,?Dite) je S strycem ditete D

%neter(?N,?X) je N neteri clovek X
%synovec

%bratranec
%sestrenice

%tchan(?T,?X) je T tchanem cloveka X

%tchyne(?T,?X) je T tchyni cloveka X

tchyne(T,X):-matka(T,D), manzel(X,D).
tchyne(T,X):-matka(T,D), manzel(D,X).

%predek(-P,+X)
predek(P, X):-rodic(P,X). %ukoncovaci podminka
predek(P, X):-rodic(R, X), predek(P, R).

%faktorial(+N,-V) V = N!
faktorial(0,1).
faktorial(N,V):- N1 is N-1, faktorial(N1,V1), V is N * V1.
%is - prirazeni (jako = v C nebo := v Pascalu)
%= - unifikace, ztotozneni dvou promennych - dotaz zda jsou stejna - prace s pointery

