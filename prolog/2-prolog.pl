%SEZNAMY
%[a, b, c, d]... 4prvkovy SEZNAMY
%[]...prazdny SEZNAMY
%[X]...libovolny(X je promenna) 1 prvkovy seznam
%[a, [b , c], d, e] - seznam s podseznamem
%[Hlava|Telo]... car a cdr
%[a,b|Telo]

%patri(+X,+S)... x je v seznamu s
patri(X,[X,_]).%kdyz je x v hlave seznamu
patri(X,[Y,Telo]):- X\=Y, patri(X,Telo).

%nepatri(+X,+S)
nepatri(_,[]).
nepatri(X,[H,T]):- X\=H, nepatri(X,T).

%delka(+S,-D)
delka([],0).
delka([_|T],D):- delka(T,D1), D is D1 + 1.

%delkaPod(+S,-D)
delkaPod([],0).
delkaPod([X|T],D):- atomic(X), delkaPod(T,D1), D is D1 + 1.
delkaPod([X|T],D):- delkaPod(X,D1), delkaPod(T,D2), D is D1 + D2.

%cetnost(+X,+S,-C)...cetnost prvku v seznamu
cetnost(_,[],0).
cetnost(X,[X|Telo],C):- cetnost(X,Telo,C1), C is C1 + 1.
cetnost(X,[Y|Telo],C):- X \= Y, cetnost(X,Telo,C).

%pridejZ(+X,+S,-O)
pridejZ(X,S,[X|S]).

%pridejK(+X,+S,-O)
pridejK(X,[],[X]).
pridejK(X,[H|T],[H|O1]):-pridejK(X,T,O1).

%otoc(+S,-S2)...[1, 2, 3] -> [3, 2, 1]
otoc([],[]).
otoc([H|T],S2):- otoc(T, S3), pridejK(H,S3,S2).

otoc2(S1,S2):-otocAk(S1,S2,[]).

otocAk([],Ak,Ak).
otocAk([H|T], S2, Ak):-otocAk(T,S2,[H|Ak]).

%slij(+S1,+S2,-S)...[1,2],[3,4]->[1,2,3,4]
slij([],S2,S2).
slij([H|T],S2,[H|S]):-slij(T,S2,S).

%rozbal(+S,-O)...[1,2,[3,4,5],6]->[1,2,3,4,5,6]
rozbal([],[]).
rozbal([H|T],[H|S2]):-atomic(H),rozbal(T,S2).
rozbal([H|T],S):-is_list(H),rozbal(T,S2),rozbal(H,S3),slij(S3,S2,S).

%fak(+N,-S)...seznam hodnot fakt 1,...,n
fakt(0,[1]).
fakt(N,[F,F1|T2]):-N>0,N1 is N-1, fakt(N1,[F1,T2]), F is N*F1.

%bublesort(+S,-S2)...[3,1,2,1]->[1,1,2,3]
bublesort(S,S2):-bubleS(S,S,S2).
bubleS(S,[],S).
bubleS(S,[_,T],S2):-bublej(S,S3),bubleS(S3,T,S2).
%dodelat bublej