bublesort(S,S2):-bubleS(S,S,S2).
bubleS(S,[],S).
bubleS(S,[_,T],S2):-bublej(S,S3),bubleS(S3,T,S2).
bublej([],[]).
bublej([X],[X]).
bublej([X,Y|S],[Y|Sout]):-X>Y, bublej([X|S],Sout).
bublej([X,Y|S],[X|Sout]):-X=<Y, bublej([Y|S],Sout).
%matice
%[[1,2],[3,4]]
%delka(+S,-D)
delka([],0).
delka([_|T],D):- delka(T,D1), D is D1 + 1.
%rozmer(+Matice,-M,-N)
rozmer([],0,0).
rozmer([X|Matice],M,N):- delka([X|Matice],M), delka(X,N).
%diagonala(+Matice,-Diag)
diagonala([],[]).
diagonala([[X|_]|Matice],[X|Diag]):-usekniSloupec(Matice,Matice2), diagonala(Matice2,Diag).
usekniSloupec([],[]).
usekniSloupec([[_|Radek]|Matice],[Radek|M2]):-usekniSloupec(Matice,M2).

%graf
graf(V,E).
dfs(graf(V,E),W,[W]):-nenisoused(W,_,E).
dfs(graf(V,E),W,[W|Uzavrene]):-soused(W,X,E),smaz(W,V,V1),dfs(graf(V,E),X,Uzavrene).
soused(W,X,[[W,X]|_]).
soused(W,X,[[A,B]|E]):-(W\=A;X\=B),soused(W,X,E).

%rez
%! - operator rezu -tj. zakazuje backtrack